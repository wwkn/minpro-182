package com.xsis.boot182.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Biodata;
import com.xsis.boot182.model.Test;
import com.xsis.boot182.model.Trainer;
import com.xsis.boot182.service.TestService;

@Controller
public class TestController extends BaseController{
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private TestService testService;

	@RequestMapping("/test.html")
	public String test(Model model) {
		List<Test> list = new ArrayList<>();
		list = this.testService.getList();
		//tempat menampung data dari model ke view
		model.addAttribute("testList", list);
		return "test/test";
	}
	
	@RequestMapping("/test/list.html")
	public String list(Model model) {
		List<Test> list = new ArrayList<>();
		list = this.testService.getList();
		//untuk mengisi 
		model.addAttribute("testList", list);
		return "test/list";
	}
	
	@RequestMapping("/test/add.html")
	public String add(Model model) {
		return "test/add";
	}
	
	@RequestMapping("/test/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Test test = this.testService.getData(id);
		model.addAttribute("test", test);
		return "test/edit";
	}
	
	@RequestMapping("/test/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Test test = this.testService.getData(id);
		model.addAttribute("test", test);
		return "test/delete";
	}
	
	@RequestMapping("/test/search.html")
	public String search(Model model, HttpServletRequest request) {
		String search = request.getParameter("search");
		List<Test> list = new ArrayList<>();
		list = this.testService.search(search);
		model.addAttribute("searchList", list);
		return "test/search";
	}

	@RequestMapping(value="/api/test/post/", method=RequestMethod.POST)
	public ResponseEntity<Test> save(@RequestBody Test test) {
		ResponseEntity<Test> result = null;
		try {
			//get date now
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			test.setCreatedOn(date);
			test.setCreatedBy(this.getUserId());
			test.setIsDelete(0);
			this.testService.insert(test);
			result = new ResponseEntity<Test>(test, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Test>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@RequestMapping(value="/api/test/update/", method=RequestMethod.PUT)
	public ResponseEntity<Test> update(@RequestBody Test test) {
		ResponseEntity<Test> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			test.setModifiedOn(date);
			test.setModifiedBy(this.getUserId());
			this.testService.update(test);
			result = new ResponseEntity<Test>(test, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/test/delete/", method=RequestMethod.PUT)
	public ResponseEntity<Test> delete(@RequestBody Test test) {
		ResponseEntity<Test> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			test.setDeletedOn(date);
			test.setDeletedBy(this.getUserId());
			test.setIsDelete(1);
			this.testService.update(test);
			result = new ResponseEntity<Test>(test, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}
