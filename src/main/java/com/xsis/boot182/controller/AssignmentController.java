package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Assignment;
import com.xsis.boot182.model.Biodata;
import com.xsis.boot182.service.AssignmentService;
import com.xsis.boot182.service.BiodataService;

@Controller
public class AssignmentController extends BaseController{
	private Log log = LogFactory.getLog(getClass());
	
	//menambahkan service assignment untuk mengakses list assign
	@Autowired
	private AssignmentService assignmentService;
	//menambahkan service biodata untuk mengakses list biodata
	@Autowired
	private BiodataService biodataService;
	
	//mapping untuk url assignment
	@RequestMapping("/assignment.html")
	public String assignment(Model model) {
		List<Assignment> list = new ArrayList<>();
		list = this.assignmentService.getList();
		//tempat menampung data dari model ke view
		model.addAttribute("assignList", list);
		return "assignment/assignment";
	}
	
	//mapping untuk url list assignment
	@RequestMapping("/assignment/list.html")
	public String list(Model model) {
		List<Assignment>list = new ArrayList<>();
		list = this.assignmentService.getList();
		model.addAttribute("assignList", list);
		return "assignment/list";
	}
	
	//mapping untuk url add
	@RequestMapping("/assignment/add.html")
	public String add(Model model) {
		List<Biodata> listBiodata = new ArrayList<>();
		listBiodata = this.biodataService.getList();
		
		model.addAttribute("bioList", listBiodata);
		return "assignment/add";
	}
	
	//mapping untuk url edit
	@RequestMapping("/assignment/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id= Long.parseLong(request.getParameter("id"));
		Assignment assignment = this.assignmentService.getData(id);
		List<Biodata> listBiodata = new ArrayList<>();
		listBiodata = this.biodataService.getList();
		
		model.addAttribute("bioList", listBiodata);
		
		model.addAttribute("assignment", assignment);
		return "assignment/edit";
	}
	
	//mapping untuk url delete 
	@RequestMapping("/assignment/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Assignment assignment = this.assignmentService.getData(id);
		model.addAttribute("assignment", assignment);
		return "assignment/delete";
	}
	
	//mapping untuk url mark as done
	@RequestMapping("/assignment/markDone.html")
	public String mDone(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Assignment assignment = this.assignmentService.getData(id);
		model.addAttribute("assignment", assignment);
		return "assignment/mark";
	}
	
	//mapping untuk url hold
	@RequestMapping("/assignment/hold.html")
	public String hold(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Assignment assignment = this.assignmentService.getData(id);
		model.addAttribute("assignment", assignment);
		return "assignment/hold";
	}
	
	//mapping search
	@RequestMapping("/assignment/search.html")
	public String search(Model model, HttpServletRequest request) {
		String search = request.getParameter("cari");
		List<Assignment> list = new ArrayList<>();
		list = this.assignmentService.search(search);
		model.addAttribute("searchList", list);
		
		return "assignment/search";
	}

	//untuk metode simpan/insert ke database
	@RequestMapping(value="/api/assignment/post/", method=RequestMethod.POST)
	public ResponseEntity<Assignment> save(@RequestBody Assignment assignment) {
		ResponseEntity<Assignment> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			assignment.setCreatedBy(this.getUserId());
			assignment.setCreatedOn(date);
			assignment.setIsDelete(0);
			assignment.setIsHold(0);
			this.assignmentService.insert(assignment);
			result = new ResponseEntity<Assignment>(assignment, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Assignment>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//untuk metode mark as done
	@RequestMapping(value="/api/assignment/m/", method=RequestMethod.PUT)
	public ResponseEntity<Assignment> mDone(@RequestBody Assignment assignment) {
		ResponseEntity<Assignment> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			assignment.setModifiedBy(this.getUserId());
			assignment.setModifiedOn(date);
			assignment.setIsDone(1);
			this.assignmentService.update(assignment);
			result = new ResponseEntity<Assignment>(assignment, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//untuk metode edit
	@RequestMapping(value="/api/assignment/e/", method=RequestMethod.PUT)
	public ResponseEntity<Assignment> edit(@RequestBody Assignment assignment) {
		ResponseEntity<Assignment> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			assignment.setModifiedBy(this.getUserId());
			assignment.setModifiedOn(date);
			this.assignmentService.update(assignment);
			result = new ResponseEntity<Assignment>(assignment, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//untuk metode delete temp
	@RequestMapping(value="/api/assignment/d/", method=RequestMethod.PUT)
	public ResponseEntity<Assignment> delete(@RequestBody Assignment assignment) {
		ResponseEntity<Assignment> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			assignment.setModifiedBy(this.getUserId());
			assignment.setModifiedOn(date);
			assignment.setDeletedBy(this.getUserId());
			assignment.setDeletedOn(date);
			assignment.setIsDelete(1);
			this.assignmentService.update(assignment);
			result = new ResponseEntity<Assignment>(assignment, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//untuk metode hold
	@RequestMapping(value="/api/assignment/h/", method=RequestMethod.PUT)
	public ResponseEntity<Assignment> holds(@RequestBody Assignment assignment) {
		ResponseEntity<Assignment> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			assignment.setModifiedBy(this.getUserId());
			assignment.setModifiedOn(date);
			assignment.setIsHold(1);
			this.assignmentService.update(assignment);
			result = new ResponseEntity<Assignment>(assignment, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
}