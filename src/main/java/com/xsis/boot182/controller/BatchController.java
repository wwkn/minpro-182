package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Batch;
import com.xsis.boot182.model.Biodata;
import com.xsis.boot182.model.BootcampType;
import com.xsis.boot182.model.Room;
import com.xsis.boot182.model.Technology;
import com.xsis.boot182.model.Test;
import com.xsis.boot182.model.Trainer;
import com.xsis.boot182.service.BatchService;
import com.xsis.boot182.service.BiodataService;
import com.xsis.boot182.service.BootcampTypeService;
import com.xsis.boot182.service.ClazzService;
import com.xsis.boot182.service.RoomService;
import com.xsis.boot182.service.TechnologyService;
import com.xsis.boot182.service.TestService;
import com.xsis.boot182.service.TrainerService;

@Controller
public class BatchController extends BaseController{
	
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private BatchService batchService;
	
	@Autowired
	private TechnologyService technoService;
	
	@Autowired
	private TrainerService trainerService;
	
	@Autowired
	private RoomService roomService;
	
	@Autowired
	private BootcampTypeService bootcampTypeService;
	
	@Autowired
	private BiodataService biodataService;
	
	@Autowired
	private ClazzService clazzService;
	
	@Autowired
	private TestService testService;
	
	@RequestMapping("/batch.html")
	public String batch(Model model) {
		List<Batch> list = new ArrayList<>();
		list = this.batchService.getList();
		//tempat menampung data dari model ke view
		model.addAttribute("batchList", list);
		return "batch/batch";
	}
	
	@RequestMapping("/batch/list.html")
	public String list(Model model) {
		List<Batch> list = new ArrayList<>();
		list = this.batchService.getList();
		//untuk mengisi 
		model.addAttribute("batchList", list);
		return "batch/list";
	}
	
	@RequestMapping("/batch/add.html")
	public String add(Model model) {
		List<Technology> listTech = new ArrayList<>();
		listTech = this.technoService.getList();
		
		List<Trainer> listTrainer = new ArrayList<>();
		listTrainer = this.trainerService.getList();
		
		List<Room> listRoom = new ArrayList<>();
		listRoom = this.roomService.getList();
		
		List<BootcampType> listBoot = new ArrayList<>();
		listBoot = this.bootcampTypeService.getList();
		
		model.addAttribute("techList", listTech);
		model.addAttribute("trainerList", listTrainer);
		model.addAttribute("roomList", listRoom);
		model.addAttribute("bootList", listBoot);
		return "batch/add";
	}
	
	@RequestMapping("/batch/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Batch batch = this.batchService.getData(id);
		
		List<Technology> tech = new ArrayList<>();
		tech = this.technoService.getList();
		
		List<Trainer> trainer = new ArrayList<>();
		trainer = this.trainerService.getList();
		
		List<Room> room = new ArrayList<>();
		room = this.roomService.getList();
		
		List<BootcampType> boot = new ArrayList<>();
		boot = this.bootcampTypeService.getList();
		
		model.addAttribute("batch", batch);
		model.addAttribute("techList", tech);
		model.addAttribute("trainerList", trainer);
		model.addAttribute("roomList", room);
		model.addAttribute("bootList", boot);
		return "batch/edit";
	}
	
	@RequestMapping("/batch/search.html")
	public String search(Model model, HttpServletRequest request) {
		String search = request.getParameter("search");
		List<Batch> list = new ArrayList<>();
		list = this.batchService.search(search);
		model.addAttribute("searchList", list);
		return "batch/search";
	}
	
	@RequestMapping("/batch/add-participant.html")
	public String addPart(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Batch batch = this.batchService.getData(id);
		
		List<Biodata> list = new ArrayList<>();
		list = this.biodataService.getList();
		
		model.addAttribute("batch", batch);
		model.addAttribute("biodataList", list);
		return "batch/add-participant";
	}
	
	@RequestMapping("/batch/setup-test.html")
	public String setupTest(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Batch batch = this.batchService.getData(id);
		
		List<Test> list = new ArrayList<>();
		list = this.testService.filterList();
		
		model.addAttribute("batch", batch);
		model.addAttribute("testList", list);
		return "batch/setup-test";
	}
	
	@RequestMapping(value="/api/batch/post/", method=RequestMethod.POST)
	public ResponseEntity<Batch> save(@RequestBody Batch batch) {
		ResponseEntity<Batch> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			batch.setCreatedOn(date);
			batch.setCreatedBy(this.getUserId());
			batch.setIsDelete(0);
			this.batchService.insert(batch);
			result = new ResponseEntity<Batch>(batch, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Batch>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/batch/update/", method=RequestMethod.PUT)
	public ResponseEntity<Batch> update(@RequestBody Batch batch) {
		ResponseEntity<Batch> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			batch.setModifiedOn(date);
			batch.setModifiedBy(this.getUserId());
			this.batchService.update(batch);
			result = new ResponseEntity<Batch>(batch, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Batch>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}
