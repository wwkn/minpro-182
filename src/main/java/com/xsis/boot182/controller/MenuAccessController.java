package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Menu;
import com.xsis.boot182.model.MenuAccess;
import com.xsis.boot182.model.Role;
import com.xsis.boot182.service.MenuAccessService;
import com.xsis.boot182.service.MenuService;
import com.xsis.boot182.service.RoleService;

@Controller
public class MenuAccessController extends BaseController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private MenuAccessService menuAccessService;

	@Autowired
	private MenuService menuService;

	@Autowired
	private RoleService roleService;

	@RequestMapping("/menu-access.html")
	public String index(Model model) {
		List<MenuAccess> listMA = new ArrayList<>();
		listMA = this.menuAccessService.getList();

		List<Role> listR = new ArrayList<>();
		listR = this.roleService.getList();

		List<Menu> listM = new ArrayList<>();
		listM = this.menuService.getList();

		model.addAttribute("mAList", listMA);
		model.addAttribute("rList", listR);
		return "menuAccess/index";
	}

	@RequestMapping("/menu-access/list.html")
	public String list(Model model) {
		List<MenuAccess> listMA = new ArrayList<>();
		listMA = this.menuAccessService.getList();
		model.addAttribute("list", listMA);
		return "menuAccess/list";
	}

	@RequestMapping("/menu-access/add.html")
	public String add(Model model) {
		List<MenuAccess> listMA = new ArrayList<>();
		listMA = this.menuAccessService.getList();

		List<Role> listR = new ArrayList<>();
		listR = this.roleService.getList();

		List<Menu> listM = new ArrayList<>();
		listM = this.menuService.getList();

		model.addAttribute("mAList", listMA);
		model.addAttribute("rList", listR);
		model.addAttribute("mList", listM);
		return "menuAccess/add";
	}
	
	@RequestMapping("/menu-access/search.html")
	public String search(Model model, HttpServletRequest request) {
		Long search = Long.parseLong(request.getParameter("cari"));
		List<MenuAccess> listMA = new ArrayList<>();
		listMA = this.menuAccessService.search(search);
		model.addAttribute("searchList", listMA);
		return "menuAccess/search";
	}

	@RequestMapping("/menu-access/delete.html")
	public String getdelete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		MenuAccess menuAccess = this.menuAccessService.getData(id);

		model.addAttribute("menuAccess", menuAccess);
		return "menuAccess/delete";
	}

	@RequestMapping(value = "/api/menu-access/add/", method = RequestMethod.POST)
	public ResponseEntity<MenuAccess> save(@RequestBody MenuAccess menuAccess) {
		ResponseEntity<MenuAccess> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			menuAccess.setCreatedOn(date);
			menuAccess.setCreatedBy(this.getUserId());
			this.menuAccessService.insert(menuAccess);
			result = new ResponseEntity<MenuAccess>(menuAccess, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<MenuAccess>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//data yang diambil berdasar acuan id dari menu access akan di delete
	@RequestMapping(value = "/api/menu-access/delete/{id}", method = RequestMethod.DELETE)
		public ResponseEntity<MenuAccess> delete(@PathVariable(name = "id") Long id) {
		try {
			MenuAccess menuAccess = this.menuAccessService.getData(id);
			this.menuAccessService.delete(menuAccess);
			return new ResponseEntity<MenuAccess>(HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
