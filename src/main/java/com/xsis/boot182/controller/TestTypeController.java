package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.TestType;
import com.xsis.boot182.service.TestTypeService;

@Controller
public class TestTypeController extends BaseController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private TestTypeService testTypeService;

	@RequestMapping("/test-type.html")
	public String testType(Model model) {
		return "testType/list";
	}

	@RequestMapping("/test-type/add.html")
	public String add(Model model) {
		return "testType/add";
	}

	@RequestMapping("/test-type/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		TestType testType = this.testTypeService.getData(id);
		model.addAttribute("testType", testType);
		return "testType/edit";
	}
	@RequestMapping("/test-type/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		TestType testType = this.testTypeService.getData(id);
		model.addAttribute("testType", testType);
		return "testType/delete";
	}

	@RequestMapping(value = "/api/test-type/", method = RequestMethod.GET)
	public ResponseEntity<List<TestType>> getAll() {
		ResponseEntity<List<TestType>> result = null;
		try {
			List<TestType> list = this.testTypeService.getList();
			result = new ResponseEntity<List<TestType>>(list, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@RequestMapping(value = "/api/test-type/", method = RequestMethod.POST)
	public ResponseEntity<TestType> save(@RequestBody TestType testType) {

		ResponseEntity<TestType> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			testType.setCreatedOn(date);
			testType.setDeletedOn(date);
			testType.setCreatedBy(this.getUserId());
			testType.setIsDelete(0);
			testType.setTypeOfAnswer(0);
			this.testTypeService.insert(testType);
			result = new ResponseEntity<TestType>(testType, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<TestType>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@RequestMapping(value = "/api/test-type/", method = RequestMethod.PUT)
	public ResponseEntity<TestType> get(@RequestBody TestType testType) {
		ResponseEntity<TestType> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			testType.setModifiedOn(date);		
			testType.setModifiedBy(this.getUserId());
			this.testTypeService.update(testType);
			result = new ResponseEntity<TestType>(testType, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@RequestMapping(value = "/api/test-type/delete", method = RequestMethod.PUT)
	public ResponseEntity<TestType> delete(@RequestBody TestType testType) {
		ResponseEntity<TestType> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			testType.setDeletedOn(date);
			testType.setDeletedBy(this.getUserId());
			testType.setIsDelete(1);
			this.testTypeService.update(testType);
			result = new ResponseEntity<TestType>(testType, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@RequestMapping(value = "/api/test-type/{idnyatestType}", method = RequestMethod.GET)
	public ResponseEntity<TestType> getEdit(@PathVariable(name = "idnyatestType") Long id) {
		ResponseEntity<TestType> result = null;
		try {
			TestType testType = this.testTypeService.getData(id);
			result = new ResponseEntity<TestType>(testType, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}
