package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Biodata;
import com.xsis.boot182.model.BootcampTestType;
import com.xsis.boot182.service.BiodataService;
import com.xsis.boot182.service.BootcampTestTypeService;

@Controller
public class BiodataController extends BaseController {
	
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private BiodataService biodataService;
	
	@Autowired
	private BootcampTestTypeService bootcampTestTypeService;
	
	@RequestMapping("/biodata.html")
	public String index(Model model) {
		List<Biodata> listBiodata = new ArrayList<>();
		listBiodata = this.biodataService.getList();
		//untuk mengisi 
		model.addAttribute("list", listBiodata);
		return "biodata/index";
	}
	
	@RequestMapping("/biodata/list.html")
	public String list(Model model) {
		List<Biodata> listBiodata = new ArrayList<>();
		listBiodata = this.biodataService.getList();
		//untuk mengisi 
		model.addAttribute("list", listBiodata);
		return "biodata/list";
	}
	
	@RequestMapping("/biodata/add.html")
	public String add(Model model) {
		return "biodata/add";
	}
	
	@RequestMapping("/biodata/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id= Long.parseLong(request.getParameter("id"));
		Biodata biodata = this.biodataService.getData(id);
		
		List<BootcampTestType> btt = new ArrayList<>();
		btt = this.bootcampTestTypeService.getList();
		
		model.addAttribute("bttList", btt);
		model.addAttribute("biodata", biodata);
		return "biodata/edit";
	}
	
	@RequestMapping("/biodata/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id= Long.parseLong(request.getParameter("id"));
		Biodata biodata = this.biodataService.getData(id);
		model.addAttribute("biodata", biodata);
		return "biodata/delete";
	}
	
	@RequestMapping("/biodata/search.html")
	public String search(Model model, HttpServletRequest request) {
		String search = request.getParameter("cari");
		List<Biodata> list = new ArrayList<>();
		list = this.biodataService.search(search);
		model.addAttribute("searchList", list);
		return "biodata/search";
	}
	
	@RequestMapping(value="/api/biodata/", method=RequestMethod.POST)
	public ResponseEntity<Biodata> save(@RequestBody Biodata biodata) {
		ResponseEntity<Biodata> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			biodata.setCreatedOn(date);
			biodata.setCreatedBy(this.getUserId());
			biodata.setIsDelete(0);
			Long valBtt = Long.parseLong("1");
			biodata.setBootcampTestType(valBtt);
			this.biodataService.insert(biodata);
			result = new ResponseEntity<Biodata>(biodata, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Biodata>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/biodata/update/", method=RequestMethod.PUT)
	public ResponseEntity<Biodata> update(@RequestBody Biodata biodata) {
		ResponseEntity<Biodata> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			biodata.setModifiedOn(date);
			biodata.setModifiedBy(this.getUserId());
			this.biodataService.update(biodata);
			result = new ResponseEntity<Biodata>(biodata, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Biodata>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/biodata/del/", method=RequestMethod.PUT)
	public ResponseEntity<Biodata> delete(@RequestBody Biodata biodata){
		ResponseEntity<Biodata> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			biodata.setDeletedBy(this.getUserId());
			biodata.setDeletedOn(date);
			biodata.setIsDelete(1);
			this.biodataService.update(biodata);
			result = new ResponseEntity<Biodata>(biodata, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
}
