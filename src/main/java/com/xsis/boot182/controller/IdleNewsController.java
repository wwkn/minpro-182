package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Id;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.service.CategoryService;
import com.xsis.boot182.service.IdleNewsService;
import com.xsis.boot182.model.Category;
import com.xsis.boot182.model.IdleNews;

@Controller
public class IdleNewsController extends BaseController {

	private Log log = LogFactory.getLog(getClass());
	@Autowired
	private IdleNewsService idleNewsService;

	@Autowired
	private CategoryService categoryService;

	//tampilan awal dipanggil saat url terisi /idle-news.html
	@RequestMapping("/idle-news.html")
	public String index(Model model) {
		List<IdleNews> listIN = new ArrayList<>();
		listIN = this.idleNewsService.getList();
		model.addAttribute("list", listIN);
		return "idleNews/index";
	}

	//dipanggil oleh loadData untuk mengisi list
	@RequestMapping("/idle-news/list.html")
	public String list(Model model) {
		List<IdleNews> listIN = new ArrayList<>();
		listIN = this.idleNewsService.getList();
		model.addAttribute("list", listIN);
		return "idleNews/list";
	}
	
	//dipanggil untuk mengeluarkan isi modal add
	//bersama dengan pilihan dari table category
	@RequestMapping("/idle-news/add.html")
	public String add(Model model) {
		List<IdleNews> listIN = new ArrayList<>();
		listIN = this.idleNewsService.getList();

		List<Category> listC = new ArrayList<>();
		listC = this.categoryService.getList();

		model.addAttribute("inList", listIN);
		model.addAttribute("cList", listC);
		return "idleNews/add";
	}
	
	
	//dipakai untuk mengisi form dari edit.jsp dengan data
	//dari row yang memiliki id yang dipanggil dari get param
	@RequestMapping("/idle-news/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		IdleNews idleNews = this.idleNewsService.getData(id);

		List<Category> listC = new ArrayList<>();
		listC = this.categoryService.getList();

		model.addAttribute("idleNews", idleNews);
		model.addAttribute("cList", listC);
		return "idleNews/edit";
	}
	//dipanggil untuk modal(dialog box) saat tombol publish di klik
	@RequestMapping("/idle-news/publish.html")
	public String publish(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		IdleNews idleNews = this.idleNewsService.getData(id);

		model.addAttribute("idleNews", idleNews);
		return "idleNews/publish";
	}
	@RequestMapping("/idle-news/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		IdleNews idleNews = this.idleNewsService.getData(id);

		model.addAttribute("idleNews", idleNews);
		return "idleNews/delete";
	}
	
	@RequestMapping("/idle-news/search.html")
	public String search(Model model, HttpServletRequest request) {
		String search=request.getParameter("searchWord");
		List<IdleNews> listIN = new ArrayList<>();
		listIN = this.idleNewsService.search(search);
		model.addAttribute("searchList", listIN);
		return "idleNews/search";
	}
	
	//menambahkan data dari form kedalam model
	@RequestMapping(value = "/api/idle-news/post/", method = RequestMethod.POST)
	public ResponseEntity<IdleNews> save(@RequestBody IdleNews idleNews) {
		ResponseEntity<IdleNews> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			idleNews.setCreatedOn(date);
			idleNews.setcreatedBy(this.getUserId());
			idleNews.setIsDelete(0);
			idleNews.setIsPublish(1);
			this.idleNewsService.insert(idleNews);
			result = new ResponseEntity<IdleNews>(idleNews, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<IdleNews>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	//mengupdate data dr row dengan id yang terpilih dr table
	@RequestMapping(value = "/api/idle-news/edit/", method = RequestMethod.PUT)
	public ResponseEntity<IdleNews> update(@RequestBody IdleNews idleNews) {
		ResponseEntity<IdleNews> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			idleNews.setModifiedOn(date);
			idleNews.setModifiedBy(this.getUserId());
			this.idleNewsService.update(idleNews);
			result = new ResponseEntity<IdleNews>(idleNews, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<IdleNews>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//mengganti is publish dari 1(unpublished/false) menjadi 0(published/true)
	@RequestMapping(value = "/api/idle-news/publish/", method = RequestMethod.PUT)
	public ResponseEntity<IdleNews> publish(@RequestBody IdleNews idleNews) {
		ResponseEntity<IdleNews> result = null;
		try {
			SimpleDateFormat bentukTanggal = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = bentukTanggal.format(currentDate);
			idleNews.setIsPublish(0);
			this.idleNewsService.update(idleNews);
			result = new ResponseEntity<IdleNews>(idleNews, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(),e);
			result= new ResponseEntity<IdleNews>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

//mengubah is delete menjadi 1
@RequestMapping(value = "/api/idle-news/delete/", method = RequestMethod.PUT)
public ResponseEntity<IdleNews> delete(@RequestBody IdleNews idleNews) {
	ResponseEntity<IdleNews> result = null;
	try {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = Calendar.getInstance().getTime();
		String date = format.format(currentDate);
		idleNews.setDeletedOn(date);
		idleNews.setDeletedBy(this.getUserId());
		idleNews.setIsDelete(1);
		this.idleNewsService.update(idleNews);
		result = new ResponseEntity<IdleNews>(idleNews, HttpStatus.CREATED);
	} catch (Exception e) {
		log.debug(e.getMessage(),e);
		result= new ResponseEntity<IdleNews>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	return result;
}
}