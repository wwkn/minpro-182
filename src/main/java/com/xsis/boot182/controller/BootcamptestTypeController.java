package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.BootcampTestType;
import com.xsis.boot182.service.BootcampTestTypeService;

@Controller
public class BootcamptestTypeController extends BaseController {
	
private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private BootcampTestTypeService bttService;
	
	@RequestMapping("/bootcampTestType.html")
	public String btt(Model model) {
		return "bootcamptesttype/bootcamptesttype";
	}
	
	//add data
	@RequestMapping("/bootcamptesttype/add.html")
	public String add(Model model) {
		return "bootcamptesttype/add";
	}
	
	//edit data
	@RequestMapping("/bootcamptesttype/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		BootcampTestType bootcamptesttype = this.bttService.getData(id);
		model.addAttribute("btt", bootcamptesttype);
		return "bootcamptesttype/edit";
	}
	
	//delete data
	@RequestMapping("/bootcamptesttype/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		BootcampTestType bootcamptesttype = this.bttService.getData(id);
		model.addAttribute("btt", bootcamptesttype);
		return "bootcamptesttype/delete";
	}
	
	//untuk mengambil data dari database
	@RequestMapping(value="/api/btt/", method=RequestMethod.GET)
	public ResponseEntity<List<BootcampTestType>> getAll(){
		ResponseEntity<List<BootcampTestType>> result = null;
		try {
			List<BootcampTestType> list = this.bttService.getList();
			result = new ResponseEntity<List<BootcampTestType>>(list, HttpStatus.OK);
		}catch(Exception e) {
			log.debug(e.getMessage(), e);
		 	result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//untuk memanggil data berdasarkan id
			@RequestMapping(value="/api/btt/{id}", method=RequestMethod.GET)
			public ResponseEntity<BootcampTestType> get(@PathVariable(name="id")Long id) {
				ResponseEntity<BootcampTestType> result = null;
				try {
					BootcampTestType btt = this.bttService.getData(id);
					result = new ResponseEntity<BootcampTestType>(btt, HttpStatus.OK);
				} catch (Exception e) {
					log.debug(e.getMessage(), e);
					result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
				}
				return result;
			}
	
	//untuk form add dimanan createdOn diambil dari date sekarang dan createdBy dari user login
	@RequestMapping(value="/api/btt/", method=RequestMethod.POST)
	public ResponseEntity<BootcampTestType> save(@RequestBody BootcampTestType btt){
		ResponseEntity<BootcampTestType> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			btt.setCreatedOn(date);
			btt.setCreatedBy(this.getUserId());
			btt.setIsDelete(0);
			this.bttService.insert(btt);
			result = new ResponseEntity<BootcampTestType>(btt, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<BootcampTestType>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//untuk form edit
	@RequestMapping(value="/api/btt/", method=RequestMethod.PUT)
	public ResponseEntity<BootcampTestType> update(@RequestBody BootcampTestType btt) {
		ResponseEntity<BootcampTestType> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			btt.setModifiedOn(date);
			btt.setModifiedBy(this.getUserId());
			this.bttService.update(btt);
			result = new ResponseEntity<BootcampTestType>(btt, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	//untuk form delete
	@RequestMapping(value="/api/btt/d", method=RequestMethod.PUT)
	public ResponseEntity<BootcampTestType> delete(@RequestBody BootcampTestType btt) {
		ResponseEntity<BootcampTestType> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			btt.setDeletedOn(date);
			btt.setDeletedBy(this.getUserId());
			btt.setIsDelete(1);
			this.bttService.update(btt);
			result = new ResponseEntity<BootcampTestType>(btt, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}