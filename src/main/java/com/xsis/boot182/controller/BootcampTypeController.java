package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.BootcampType;
import com.xsis.boot182.service.BootcampTypeService;

@Controller
public class BootcampTypeController extends BaseController {
	
	
	private Log log=LogFactory.getLog(getClass());
	
	@Autowired
	private BootcampTypeService bts;
	
	//mengarahkan request bootcamptype.html ke bootcamptype/index
	//model=penampung variabel sebelum dikirim ke view
	@RequestMapping("/bootcamp-type.html")
	public String index(Model model) {
		List<BootcampType> listBtc= new ArrayList<>();
		listBtc = this.bts.getList(); 	
		model.addAttribute("list",listBtc);
		return "bootcampType/index";
	}
	@RequestMapping("/bootcamp-type/list.html")
	public String list(Model model) {
		List<BootcampType> listBtc= new ArrayList<>();
		listBtc = this.bts.getList(); 	
		model.addAttribute("list",listBtc);
		return "bootcampType/list";
	}
	
	
	//controller add bikin setelah disambung dari fungsi di jsp
	@RequestMapping("/bootcamp-type/add.html")
	public String add(Model model) {
		return "bootcampType/add";
	}
	
	@RequestMapping("/bootcamp-type/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id=Long.parseLong(request.getParameter("id"));
		BootcampType Bct = this.bts.getData(id);
		model.addAttribute("bootcampType",Bct);
		return "bootcampType/edit";
	}
	
	@RequestMapping("/bootcamp-type/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id=Long.parseLong(request.getParameter("id"));
		BootcampType Bct = this.bts.getData(id);
		model.addAttribute("bootcampType",Bct);
		return "bootcampType/delete";
	}
	
	@RequestMapping("/bootcamp-type/search.html")
	public String search(Model model, HttpServletRequest request) {
		String search=request.getParameter("cari");
		List<BootcampType> listBct= new ArrayList<>();
		listBct = this.bts.search(search);
		model.addAttribute("searchList",listBct);
		return "bootcampType/search";
	}
	
	@RequestMapping(value="/api/bootcamp-type/", method=RequestMethod.POST)
	public ResponseEntity<BootcampType> save(@RequestBody BootcampType bootcampType){
		ResponseEntity<BootcampType> result=null;
		try {
			SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date=format.format(currentDate);
			bootcampType.setCreatedOn(date);
			bootcampType.setCreatedBy(this.getUserId());
			bootcampType.setIsDelete(0);
			
			this.bts.insert(bootcampType);
			result=new ResponseEntity<BootcampType>(bootcampType,HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result=new ResponseEntity<BootcampType>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	@RequestMapping(value="/api/bootcamp-type/",method=RequestMethod.PUT)
	public ResponseEntity<BootcampType> get(@RequestBody BootcampType bootcampType){
		ResponseEntity<BootcampType> result=null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate=Calendar.getInstance().getTime();
			String date=format.format(currentDate);
			bootcampType.setModifiedOn(date);
			bootcampType.setModifiedBy(this.getUserId());
			this.bts.update(bootcampType);
			result=new ResponseEntity<BootcampType>(bootcampType, HttpStatus.OK);
		}catch (Exception e) {
			log.error(e.getMessage(),e);
			result=new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/bootcamp-type/delete",method=RequestMethod.PUT)
	public ResponseEntity<BootcampType> delete(@RequestBody BootcampType bootcampType){
		ResponseEntity<BootcampType> result=null;
		try {
			SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate=Calendar.getInstance().getTime();
			String date= format.format(currentDate);
			bootcampType.setDeletedOn(date);
			bootcampType.setDeletedBy(this.getUserId());
			bootcampType.setIsDelete(1);
			this.bts.update(bootcampType);
			result= new ResponseEntity<BootcampType>(bootcampType, HttpStatus.OK);
		}catch (Exception e) {
			log.error(e.getMessage(),e);
			result=new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/bootcamp-type/{idRow}",method=RequestMethod.GET)
	public ResponseEntity<BootcampType> getEdit(@PathVariable(name="idRow") Long id){
		ResponseEntity<BootcampType> result=null;
		try {
			BootcampType Btc=this.bts.getData(id);
			result=new ResponseEntity<BootcampType>(Btc, HttpStatus.OK);
		}catch (Exception e) {
			log.error(e.getMessage(),e);;
			result=new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
}
