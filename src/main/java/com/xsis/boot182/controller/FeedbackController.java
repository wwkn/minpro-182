package com.xsis.boot182.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.boot182.model.Test;
import com.xsis.boot182.service.FeedbackService;
import com.xsis.boot182.service.TestService;

@Controller
public class FeedbackController extends BaseController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private FeedbackService feedbackService;

	@Autowired
	private TestService testService;

	@RequestMapping("/feedback.html")
	public String feedback(Model model) {
		List<Test> list = new ArrayList<>();
		list = this.testService.getList();
		// tempat menampung data dari model ke view
		model.addAttribute("testList", list);
		return "feedback/feedback";
	}
}
