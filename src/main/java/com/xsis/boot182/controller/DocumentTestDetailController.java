package com.xsis.boot182.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.xsis.boot182.model.DocumentTestDetail;
import com.xsis.boot182.model.Question;
import com.xsis.boot182.service.DocumentTestDetailService;
import com.xsis.boot182.service.QuestionService;

@Controller
public class DocumentTestDetailController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private DocumentTestDetailService documentDetailService;
	
	@Autowired
	private QuestionService questionService;
	
	
	@RequestMapping("/document-test/add-question.html")
	public String add(Model model) {
		
		List<Question> listQuestion = new ArrayList<>();
		listQuestion = this.questionService.getList();
		model.addAttribute("questionList", listQuestion);

		return "document-test/add-question";
	}
	
	@RequestMapping(value = "/api/document-test-detail/{idDocTest}", method = RequestMethod.GET)
	public ResponseEntity<List<DocumentTestDetail>> get(@PathVariable(name = "idDocTest") Long id) {
		ResponseEntity<List<DocumentTestDetail>> result = null;
		try {
			List<DocumentTestDetail> docTestDetail = this.documentDetailService.listDocTestDetailByDocTest(id);
			result = new ResponseEntity<List<DocumentTestDetail>>(docTestDetail, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value = "/api/document-test-detail/", method = RequestMethod.POST)
	public ResponseEntity<DocumentTestDetail> save(@RequestBody DocumentTestDetail documentTestDetail) {
		ResponseEntity<DocumentTestDetail> result = null;
		try {
			this.documentDetailService.insert(documentTestDetail);
			result = new ResponseEntity<DocumentTestDetail>(documentTestDetail, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<DocumentTestDetail>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value = "/api/document-test-detail/", method = RequestMethod.DELETE)
	public ResponseEntity<DocumentTestDetail> delete(@RequestBody DocumentTestDetail documentTestDetail) {
		ResponseEntity<DocumentTestDetail> result = null;
		try {
			this.documentDetailService.delete(documentTestDetail);
			result = new ResponseEntity<DocumentTestDetail>(documentTestDetail, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = new ResponseEntity<DocumentTestDetail>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	

}
