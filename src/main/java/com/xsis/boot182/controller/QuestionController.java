package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Question;
import com.xsis.boot182.service.QuestionService;

@Controller
public class QuestionController extends BaseController {

	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private QuestionService questionService;

	@RequestMapping("/question.html")
	public String question(Model model) {
		model.addAttribute("judul", "QUESTION");
		return "question/question";
	}

	@RequestMapping("/question/add.html")
	public String add(Model model) {
		return "question/add";
	}

	@RequestMapping("/question/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Question questions = this.questionService.getData(id);
		model.addAttribute("question", questions);
		return "question/edit";
	}
	
	@RequestMapping("/question/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Question questions = this.questionService.getData(id);
		model.addAttribute("question",questions);
		return "question/delete";
	}

	@RequestMapping(value = "/api/question/", method = RequestMethod.GET)
	public ResponseEntity<List<Question>> getAll() {
		ResponseEntity<List<Question>> result = null;
		try {
			List<Question> list = this.questionService.getList();
			result = new ResponseEntity<List<Question>>(list, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@RequestMapping(value = "/api/question/", method = RequestMethod.POST)
	public ResponseEntity<Question> save(@RequestBody Question question) {
		ResponseEntity<Question> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			question.setCreatedOn(date);
			question.setCreatedBy(this.getUserId());
			question.setIsDelete(0);
			this.questionService.insert(question);
			result = new ResponseEntity<Question>(question, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Question>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@RequestMapping(value = "/api/choice/", method = RequestMethod.PUT)
	public ResponseEntity<Question> update(@RequestBody Question question) {
		ResponseEntity<Question> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			question.setModifiedOn(date);
			question.setModifiedBy(this.getUserId());
			this.questionService.update(question);
			result = new ResponseEntity<Question>(question, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@RequestMapping(value = "/api/question/{id}", method = RequestMethod.GET)
	public ResponseEntity<Question> get(@PathVariable(name = "id") Long id) {
		ResponseEntity<Question> result = null;
		try {
			Question question = this.questionService.getData(id);
			result = new ResponseEntity<Question>(question, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@RequestMapping(value="/api/question/", method=RequestMethod.PUT)
	public ResponseEntity<Question> delete(@RequestBody Question question) {
		ResponseEntity<Question> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			question.setDeletedOn(date);
			question.setDeletedBy(this.getUserId());
			question.setIsDelete(1);
			this.questionService.update(question);
			result = new ResponseEntity<Question>(question, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}
