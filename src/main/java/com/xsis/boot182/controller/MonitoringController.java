package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Biodata;
import com.xsis.boot182.model.Monitoring;
import com.xsis.boot182.service.BiodataService;
import com.xsis.boot182.service.MonitoringService;

@Controller
public class MonitoringController extends BaseController {

	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private MonitoringService monitoringService;
	
	@Autowired
	private BiodataService biodataService;
	
	@RequestMapping("/monitoring.html")
	public String index(Model model) {
		List<Monitoring> listMonitoring = new ArrayList<>();
		listMonitoring = this.monitoringService.getList();
		model.addAttribute("listMonitoring", listMonitoring);
		return "monitoring/index";
	}
	
	@RequestMapping("/monitoring/list.html")
	public String list(Model model) {
		List<Monitoring> listMonitoring = new ArrayList<>();
		listMonitoring = this.monitoringService.getList();
		model.addAttribute("listMonitoring", listMonitoring);
		return "monitoring/list";
	}
	
	@RequestMapping("/monitoring/add.html")
	public String add(Model model) {
		List<Biodata> listBiodata = new ArrayList<>();
		listBiodata = this.biodataService.getList();
		model.addAttribute("listBiodata", listBiodata);
		return "monitoring/add";
	}
	
	@RequestMapping("/monitoring/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Monitoring monitoring = this.monitoringService.getData(id);
		
		List<Biodata> biodata = new ArrayList<>();
		biodata = this.biodataService.getList();
		
		model.addAttribute("biodataList", biodata);
		model.addAttribute("monitoring", monitoring);
		return "monitoring/edit";
	}
	
	@RequestMapping("/monitoring/placement.html")
	public String placement(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Monitoring monitoring = this.monitoringService.getData(id);
		
		List<Biodata> biodata = new ArrayList<>();
		biodata = this.biodataService.getList();
		
		model.addAttribute("biodataList", biodata);
		model.addAttribute("monitoring", monitoring);
		return "monitoring/placement";
	}
	
	@RequestMapping("/monitoring/delete.html")
	public String placementDel(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Monitoring monitoring = this.monitoringService.getData(id);
		model.addAttribute("monitoring", monitoring);
		return "monitoring/delete";
	}
	
	@RequestMapping("/monitoring/search.html")
	public String search(Model model, HttpServletRequest request) {
		String search = request.getParameter("cari");
		List<Monitoring> list = new ArrayList<>();
		list = this.monitoringService.search(search);
		model.addAttribute("searchList", list);
		return "monitoring/search";
	}
	
	@RequestMapping(value="/api/monitoring/post/", method=RequestMethod.POST)
	public ResponseEntity<Monitoring> save(@RequestBody Monitoring monitoring) {
		ResponseEntity<Monitoring> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			monitoring.setCreatedOn(date);
			monitoring.setCreatedBy(this.getUserId());
			monitoring.setIsDelete(0);
			this.monitoringService.insert(monitoring);
			result = new ResponseEntity<Monitoring>(monitoring, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Monitoring>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/monitoring/update/", method=RequestMethod.PUT)
	public ResponseEntity<Monitoring> update(@RequestBody Monitoring monitoring) {
		ResponseEntity<Monitoring> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			monitoring.setModifiedOn(date);
			monitoring.setModifiedBy(this.getUserId());
			this.monitoringService.update(monitoring);
			result = new ResponseEntity<Monitoring>(monitoring, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Monitoring>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/monitoring/placement/", method=RequestMethod.PUT)
	public ResponseEntity<Monitoring> placement(@RequestBody Monitoring monitoring) {
		ResponseEntity<Monitoring> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			monitoring.setModifiedOn(date);
			monitoring.setModifiedBy(this.getUserId());
			this.monitoringService.update(monitoring);
			result = new ResponseEntity<Monitoring>(monitoring, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Monitoring>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/monitoring/delete/", method=RequestMethod.PUT)
	public ResponseEntity<Monitoring> delete(@RequestBody Monitoring monitoring) {
		ResponseEntity<Monitoring> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			monitoring.setDeletedOn(date);
			monitoring.setDeletedBy(this.getUserId());
			monitoring.setIsDelete(1);
			this.monitoringService.update(monitoring);
			result = new ResponseEntity<Monitoring>(monitoring, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Monitoring>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}