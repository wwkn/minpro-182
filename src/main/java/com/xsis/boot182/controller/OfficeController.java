package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Office;
import com.xsis.boot182.model.OfficeViewModel;
import com.xsis.boot182.service.OfficeService;
import com.xsis.boot182.service.RoomService;

@Controller
public class OfficeController extends BaseController{
	
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private OfficeService offS;
	
	@RequestMapping("/office.html")
	public String office(Model model) {
		List<Office> list = new ArrayList<>();
		list = this.offS.getList();
		model.addAttribute("list", list);
		return "office/office";
	}
	
	@RequestMapping("/office/add.html")
	public String add(Model model) {
		return "office/add";
	}
	
	@RequestMapping("/office/edit.html")
	public String edit(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Office office = this.offS.getData(id);
		model.addAttribute("office", office);
		return "office/edit";
	}
	
	@RequestMapping("/office/delete.html")
	public String delete(Model model, HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		Office office = this.offS.getData(id);
		model.addAttribute("office", office);
		return "office/delete";
	}
	
	@RequestMapping("/office/radd.html")
	public String addR (Model model) {
		return "office/room/add";
	}
	
	@RequestMapping(value="/api/office/", method=RequestMethod.POST)
	public ResponseEntity<OfficeViewModel> save(@RequestBody OfficeViewModel officeV){
		ResponseEntity<OfficeViewModel> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			officeV.setCreatedOn(date);
			officeV.setCreatedBy(this.getUserId());
			officeV.setIsDelete(0);
			this.offS.insert(officeV);
			result = new ResponseEntity<OfficeViewModel>(officeV, HttpStatus.CREATED);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<OfficeViewModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/office/", method=RequestMethod.PUT)
	public ResponseEntity<Office> update(@RequestBody Office office){
		ResponseEntity<Office> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			office.setModifiedBy(this.getUserId());
			office.setModifiedOn(date);
			this.offS.update(office);
			result = new ResponseEntity<Office>(office, HttpStatus.OK);
		}catch(Exception e) {
			log.debug(e.getMessage(),e);
			result = new ResponseEntity<Office>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/office/d", method=RequestMethod.PUT)
	public ResponseEntity<Office> delete(@RequestBody Office office){
		ResponseEntity<Office> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-DD");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			office.setDeletedBy(this.getUserId());
			office.setDeletedOn(date);
			office.setIsDelete(1);
			this.offS.update(office);
			result = new ResponseEntity<Office>(office, HttpStatus.OK);
		}catch(Exception e) {
			log.debug(e.getMessage(),e);
			result = new ResponseEntity<Office>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}