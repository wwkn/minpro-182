package com.xsis.boot182.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.boot182.model.Menu;
import com.xsis.boot182.service.MenuService;

@Controller
public class MenuController extends BaseController{
	private Log log = LogFactory.getLog(getClass());
	//Untuk mengakses menu service
	@Autowired
	private MenuService menuService;
	
	//mapping url menu
	@RequestMapping("/menu.html")
	public String tampil(Model model) {
		List<Menu> list = new ArrayList<>();
		list = this.menuService.getList();
		model.addAttribute("MnuList", list);
		return "menu/menu";
	}
	
	//mapping url menu untuk load ulang
		@RequestMapping("/menu/list.html")
		public String cTampil(Model model) {
			List<Menu> list = new ArrayList<>();
			list = this.menuService.getList();
			model.addAttribute("MnuList", list);
			return "menu/list";
		}

	//mapping untuk url add
		@RequestMapping("/menu/add.html")
		public String tambah(Model model) {
			List<Menu> listMn = new ArrayList<>();
			listMn = this.menuService.getList();
			model.addAttribute("mnuList", listMn);
			return "menu/add";
		}
		
	//mapping edit
		@RequestMapping("/menu/edit.html")
		public String edit(Model model, HttpServletRequest request) {
			Long id = Long.parseLong(request.getParameter("id"));
			Menu menu = this.menuService.getData(id);
			model.addAttribute("menu", menu);
			return "menu/edit";
		}
	
	//mapping delete 
		@RequestMapping("/menu/delete.html")
		public String delete(Model model, HttpServletRequest request) {
			Long id = Long.parseLong(request.getParameter("id"));
			Menu menu = this.menuService.getData(id);
			model.addAttribute("menu", menu);
			return "menu/delete";
		}
		
	//mapping search
		@RequestMapping("/menu/search.html")
		public String search(Model model, HttpServletRequest request) {
			String search = request.getParameter("cari");
			List<Menu> list = new ArrayList<>();
			list = this.menuService.search(search);
			model.addAttribute("searchList", list);
			return "menu/search";
		}
		
		
	//Membuat method request post dengan api
		@RequestMapping(value="/api/menu/tambah/", method=RequestMethod.POST)
	public ResponseEntity<Menu> simpan(@RequestBody Menu menu) {
		ResponseEntity<Menu> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date curentDate = Calendar.getInstance().getTime();
			String date = format.format(curentDate);
			menu.setCreatedBy(this.getUserId());
			menu.setCreatedOn(date);
			menu.setIsDelete(0);
			this.menuService.insert(menu);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			result = new ResponseEntity<Menu>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
		}
		
	//untuk metode update(edit)
	@RequestMapping(value="/api/menu/edit/", method=RequestMethod.PUT)
	public ResponseEntity<Menu> update(@RequestBody Menu menu) {
		ResponseEntity<Menu> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			menu.setModifiedOn(date);
			menu.setModifiedBy(this.getUserId());
			this.menuService.update(menu);
			result = new ResponseEntity<Menu>(menu, HttpStatus.OK);
			} catch (Exception e) {
				result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
		}
	
	//untuk metode update(delete)
	@RequestMapping(value="/api/menu/d", method=RequestMethod.PUT)
	public ResponseEntity<Menu> delete(@RequestBody Menu menu) {
		ResponseEntity<Menu> result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentDate = Calendar.getInstance().getTime();
			String date = format.format(currentDate);
			menu.setModifiedOn(date);
			menu.setModifiedBy(this.getUserId());
			menu.setDeletedOn(date);
			menu.setDeletedBy(this.getUserId());
			menu.setIsDelete(1);
			this.menuService.update(menu);
			result = new ResponseEntity<Menu>(menu, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
}