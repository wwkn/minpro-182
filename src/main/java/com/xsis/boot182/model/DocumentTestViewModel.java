package com.xsis.boot182.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DocumentTestViewModel {
	private DocumentTest documentTest;
	private List<DocumentTestDetail> documentTestDetail;
	private Long createdBy;
	private Date createdOn;

	public DocumentTest getDocumentTest() {
		return documentTest;
	}

	public void setDocumentTest(DocumentTest documentTest) {
		this.documentTest = documentTest;
	}

	public List<DocumentTestDetail> getDocumentTestDetail() {
		return documentTestDetail;
	}

	public void setDocumentTestDetail(List<DocumentTestDetail> documentTestDetail) {
		this.documentTestDetail = documentTestDetail;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date created = null;
		try {
			created = format.parse(createdOn);
		} catch (Exception e) {
			created = null;
		}
		this.createdOn = created;
	}
	

}
