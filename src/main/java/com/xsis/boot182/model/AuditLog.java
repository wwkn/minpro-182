package com.xsis.boot182.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="T_AUDIT_LOG")
public class AuditLog {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID", length=11, nullable=false)
	private Long id;
	
	@Column(name="TYPE", length=10, nullable=false)
	private String type;
	
	@Column(name="JSON_INSERT", length=255)
	private String jsonInsert;
	
	@Column(name="JSON_BEFORE", length=255)
	private String jsonBefore;
	
	@Column(name="JSON_AFTER", length=255)
	private String jsonAfter;
	
	@Column(name="JSON_DELETE", length=255)
	private String jsonDelete;
	
	@Column(name="CREATED_BY", length=11, nullable=false)
	private Long createdBy;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_ON", nullable=false)
	private Date createdOn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getJsonInsert() {
		return jsonInsert;
	}

	public void setJsonInsert(String jsonInsert) {
		this.jsonInsert = jsonInsert;
	}

	public String getJsonBefore() {
		return jsonBefore;
	}

	public void setJsonBefore(String jsonBefore) {
		this.jsonBefore = jsonBefore;
	}

	public String getJsonAfter() {
		return jsonAfter;
	}

	public void setJsonAfter(String jsonAfter) {
		this.jsonAfter = jsonAfter;
	}

	public String getJsonDelete() {
		return jsonDelete;
	}

	public void setJsonDelete(String jsonDelete) {
		this.jsonDelete = jsonDelete;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
