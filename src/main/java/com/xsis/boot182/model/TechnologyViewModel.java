package com.xsis.boot182.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TechnologyViewModel {

	//parent
	private Technology technology;

	private List<Trainer> train;
	
	//child
	private List<TechnologyTrainer> ttr;

	private Long createdBy;
	
	private Date createdOn;

	private Integer isDelete;

	public Technology getTechnology() {
		return technology;
	}

	public void setTechnology(Technology technology) {
		this.technology = technology;
	}

	public List<Trainer> getTrain() {
		return train;
	}

	public void setTrain(List<Trainer> train) {
		this.train = train;
	}

	public List<TechnologyTrainer> getTtr() {
		return ttr;
	}

	public void setTtr(List<TechnologyTrainer> ttr) {
		this.ttr = ttr;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date created = null;
		try {
			created = format.parse(createdOn);
		} catch (Exception e) {
			created = null;
		}
		this.createdOn = created;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

}
