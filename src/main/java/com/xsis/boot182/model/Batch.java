package com.xsis.boot182.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="T_BATCH")
public class Batch {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID", length=11, nullable=false)
	private Long id;
	
	@Column(name="TECHNOLOGY_ID", length=11, nullable=false)
	private Long technologyId;
	
	@Column(name="TRAINER_ID", length=11, nullable=false)
	private Long trainerId;
	
	@Column(name="NAME", length=255, nullable=false)
	private String name;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="PERIOD_FROM")
	private Date periodFrom;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="PERIOD_TO")
	private Date periodTo;
	
	@Column(name="ROOM_ID", length=11)
	private Long roomId;
	
	@Column(name="BOOTCAMP_TYPE_ID", length=11)
	private Long bootcampTypeId;
	
	@Column(name="NOTES", length=255)
	private String notes;
	
	@Column(name="CREATED_BY", length=11, nullable=false)
	private Long createdBy;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_ON", nullable=false)
	private Date createdOn;
	
	@Column(name="MODIFIED_BY", length=11)
	private Long modifiedBy;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="MODIFIED_ON")
	private Date modifiedOn;
	
	@Column(name="DELETED_BY", length=11)
	private Long deletedBy;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="DELETED_ON")
	private Date deletedOn;
	
	@Column(name="IS_DELETE")
	private Integer isDelete;
	
	@ManyToOne
	@JoinColumn(name="TECHNOLOGY_ID", insertable=false, updatable=false)
	private Technology technology;
	
	@ManyToOne
	@JoinColumn(name="TRAINER_ID", insertable=false, updatable=false)
	private Trainer trainer;
	
	@ManyToOne
	@JoinColumn(name="ROOM_ID", insertable=false, updatable=false)
	private Room room;
	
	@ManyToOne
	@JoinColumn(name="BOOTCAMP_TYPE_ID", insertable=false, updatable=false)
	private BootcampType bootcampType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTechnologyId() {
		return technologyId;
	}

	public void setTechnologyId(Long technologyId) {
		this.technologyId = technologyId;
	}

	public Long getTrainerId() {
		return trainerId;
	}

	public void setTrainerId(Long trainerId) {
		this.trainerId = trainerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getPeriodFrom() {
		return periodFrom;
	}

	public void setPeriodFrom(String periodFrom) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date pFrom = null;
		try {
			pFrom = format.parse(periodFrom);
		} catch (Exception e) {
			pFrom = null;
		}
		this.periodFrom = pFrom;
	}

	public Date getPeriodTo() {
		return periodTo;
	}

	public void setPeriodTo(String periodTo) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date pTo = null;
		try {
			pTo = format.parse(periodTo);
		} catch (Exception e) {
			pTo = null;
		}
		this.periodTo = pTo;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public Long getBootcampTypeId() {
		return bootcampTypeId;
	}

	public void setBootcampTypeId(Long bootcampTypeId) {
		this.bootcampTypeId = bootcampTypeId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date created = null;
		try {
			created = format.parse(createdOn);
		} catch (Exception e) {
			created = null;
		}
		this.createdOn = created;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date modified = null;
		try {
			modified = format.parse(modifiedOn);
		} catch (Exception e) {
			modified = null;
		}
		this.modifiedOn = modified;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(String deletedOn) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date deleted = null;
		try {
			deleted = format.parse(deletedOn);
		} catch (Exception e) {
			deleted = null;
		}
		this.deletedOn = deleted;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public Technology getTechnology() {
		return technology;
	}

	public void setTechnology(Technology technology) {
		this.technology = technology;
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public void setTrainer(Trainer trainer) {
		this.trainer = trainer;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public BootcampType getBootcampType() {
		return bootcampType;
	}

	public void setBootcampType(BootcampType bootcampType) {
		this.bootcampType = bootcampType;
	}
}
