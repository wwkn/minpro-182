package com.xsis.boot182.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
@Entity
@Table(name="t_room")
public class Room {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", length = 11, nullable=false)
	private Long id;
	
	@Column(name="code", length = 50, nullable=false)
	private String code;
	
	@Column(name="name",length=50,nullable=false)
	private String name;
	
	@Column(name="capacity",length=5,nullable=false)
	private Integer capacity;
	
	@Column(name="any_projector",length=1 ,nullable=false)
	private Integer any_projector;
	
	@Column(name="notes",length=500)
	private String notes;
	
	@Column(name="office_id",length=11,nullable=false)
	private Long officeId;
	
	@Column(name="created_by",length=11,nullable=false)
	private Long createdBy;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="created_on",nullable=false)
	private Date createdOn;
	
	@Column(name="modified_by",length=11)
	private Long modifiedBy;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="modified_on")
	private Date modifiedOn;
	
	@Column(name="deleted_by",length=11)
	private Long deletedBy;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="deleted_on")
	private Date deletedOn;
	
	@Column(name="is_delete", length=1)
	private Integer isDelete;
	
	@ManyToOne
	@JoinColumn(name="office_id",insertable=false, updatable=false)
	private Office officeNya;
	
	@ManyToOne
	@JoinColumn(name="created_by",insertable=false, updatable=false)
	private User user;
	
	public Office getOfficeNya() {
		return officeNya;
	}
	public void setOfficeNya(Office officeNya) {
		this.officeNya = officeNya;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getCapacity() {
		return capacity;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	
	public Integer getAny_projector() {
		return any_projector;
	}
	public void setAny_projector(Integer any_projector) {
		this.any_projector = any_projector;
	}
	
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	public Long getOfficeId() {
		return officeId;
	}
	public void setOfficeId(Long long1) {
		this.officeId = long1;
	}
	
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Long getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	 
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	public Long getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}
	
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	
	public Integer getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
	
	
	

}
