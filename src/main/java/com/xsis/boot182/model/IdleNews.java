package com.xsis.boot182.model;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "T_IDLE_NEWS")

public class IdleNews {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", length = 11, nullable = false)
	private Long id;
	
	@Column(name = "CATEGORY_ID", length = 11, nullable = false)
	private Long categoryId;
	
	@Column(name = "TITLE", length = 255, nullable = false)
	private String title;
	
	@Column(name = "CONTENT", length = 4000)
	private String content;
	
	@Column(name = "IS_PUBLISH", nullable = false)
	private Integer isPublish;
	
	@Column(name = "CREATED_BY", length = 11, nullable=false)
	private Long createdBy;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_ON", nullable=false)
	private Date createdOn;

	@Column(name = "MODIFIED_BY", length = 11)
	private Long modifiedBy;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_ON")
	private Date modifiedOn;

	@Column(name = "DELETED_BY", length = 11)
	private Long deletedBy;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "DELETED_ON")
	private Date deletedOn;

	@Column(name = "IS_DELETE", nullable=false)
	private Integer isDelete;

	@ManyToOne
	@JoinColumn(name="CREATED_BY", insertable=false, updatable=false)
	private User user;
	
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID", insertable=false, updatable=false)
	private Category category;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getIsPublish() {
		return isPublish;
	}

	public void setIsPublish(Integer isPublish) {
		this.isPublish = isPublish;
	}

	public Long getcreatedBy() {
		return createdBy;
	}

	public void setcreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		Date newTgl = null;
		try {
			newTgl = new SimpleDateFormat("yyyy-MM-dd").parse(createdOn);
		} catch (Exception e) {
			createdOn = null;
		}
		this.createdOn = newTgl;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		Date newTgl = null;
		try {
			newTgl = new SimpleDateFormat("yyyy-MM-dd").parse(modifiedOn);
		} catch (Exception e) {
			modifiedOn = null;
		}
		this.modifiedOn = newTgl;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(String deletedOn) {
		Date newTgl=null;
		try {
			newTgl = new SimpleDateFormat("yyyy-MM-dd").parse(deletedOn);
		} catch (Exception e) {
			deletedOn = null;
		}
		this.deletedOn = newTgl;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
	
	
	
	
}
