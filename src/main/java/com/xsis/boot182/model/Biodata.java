package com.xsis.boot182.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="T_BIODATA")
public class Biodata {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID", nullable=false, length=11)
	private Long id;
	
	@Column(name="NAME", nullable=false, length=255)
	private String name;
	
	@Column(name="GENDER", nullable=true, length=5)
	private String gender;
	
	@Column(name="LAST_EDUCATION", nullable=false, length=100)
	private String lastEducation;
	
	@Column(name="GRADUATION_YEAR", nullable=false, length=5)
	private String graduationYear;
	
	@Column(name="EDUCATIONAL_LEVEL", nullable=false, length=5)
	private String educationalLevel;
	
	@Column(name="MAJORS", nullable=false, length=100)
	private String majors;
	
	@Column(name="GPA", nullable=false, length=5)
	private String gpa;
	
	@Column(name="BOOTCAMP_TEST_TYPE", nullable=true, length=11)
	private Long bootcampTestType;
	
	@Column(name="IQ", nullable=true, length=4)
	private Integer iq;
	
	@Column(name="DU", nullable=true, length=10)
	private String du;
	
	@Column(name="ARITHMETIC", nullable=true, length=5)
	private Integer arithmetic;
	
	@Column(name="NESTED_LOGIC", nullable=true, length=5)
	private Integer nestedLogic;
	
	@Column(name="JOIN_TABLE", nullable=true, length=5)
	private Integer joinTable;
	
	@Column(name="TRO", nullable=true, length=50)
	private String tro;
	
	@Column(name="NOTES", nullable=true, length=100)
	private String notes;
	
	@Column(name="INTERVIEWER", nullable=true, length=100)
	private String interviewer;
	
	@Column(name="CREATED_BY", nullable=false, length=11)
	private Long createdBy;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_ON", nullable=false)
	private Date createdOn;
	
	@Column(name="MODIFIED_BY", nullable=true, length=11)
	private Long modifiedBy;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;
	
	@Column(name="DELETED_BY", nullable=true)
	private Long deletedBy;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name="DELETED_ON", nullable=true)
	private Date deletedOn;
	
	@Column(name="IS_DELETE", nullable=false)
	private Integer isDelete;
	
	@ManyToOne
	@JoinColumn(name="BOOTCAMP_TEST_TYPE", insertable=false, updatable=false)
	private BootcampTestType btt;
	
	public BootcampTestType getBtt() {
		return btt;
	}
	public void setBtt(BootcampTestType btt) {
		this.btt = btt;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getLastEducation() {
		return lastEducation;
	}
	public void setLastEducation(String lastEducation) {
		this.lastEducation = lastEducation;
	}
	public String getGraduationYear() {
		return graduationYear;
	}
	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}
	public String getEducationalLevel() {
		return educationalLevel;
	}
	public void setEducationalLevel(String educationalLevel) {
		this.educationalLevel = educationalLevel;
	}
	public String getMajors() {
		return majors;
	}
	public void setMajors(String majors) {
		this.majors = majors;
	}
	public String getGpa() {
		return gpa;
	}
	public void setGpa(String gpa) {
		this.gpa = gpa;
	}
	public Long getBootcampTestType() {
		return bootcampTestType;
	}
	public void setBootcampTestType(Long bootcampTestType) {
		this.bootcampTestType = bootcampTestType;
	}
	public Integer getIq() {
		return iq;
	}
	public void setIq(Integer iq) {
		this.iq = iq;
	}
	public String getDu() {
		return du;
	}
	public void setDu(String du) {
		this.du = du;
	}
	public Integer getArithmetic() {
		return arithmetic;
	}
	public void setArithmetic(Integer arithmetic) {
		this.arithmetic = arithmetic;
	}
	public Integer getNestedLogic() {
		return nestedLogic;
	}
	public void setNestedLogic(Integer nestedLogic) {
		this.nestedLogic = nestedLogic;
	}
	public Integer getJoinTable() {
		return joinTable;
	}
	public void setJoinTable(Integer joinTable) {
		this.joinTable = joinTable;
	}
	public String getTro() {
		return tro;
	}
	public void setTro(String tro) {
		this.tro = tro;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getInterviewer() {
		return interviewer;
	}
	public void setInterviewer(String interviewer) {
		this.interviewer = interviewer;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date created = null;
		try {
			created = format.parse(createdOn);
		} catch (Exception e) {
			created = null;
		}
		this.createdOn = created;
	}
	public Long getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(String modifiedOn) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date modified = null;
		try {
			modified = format.parse(modifiedOn);
		} catch (Exception e) {
			modified = null;
		}
		this.modifiedOn = modified;
	}
	public Long getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(String deletedOn) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date deleted = null;
		try {
			deleted = format.parse(deletedOn);
		} catch (Exception e) {
			deleted = null;
		}
		this.deletedOn = deleted;
	}
	public Integer getIsDelete() {
		return isDelete = isDelete;
	}
	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
	
}
