package com.xsis.boot182.dao;

import com.xsis.boot182.model.BatchTest;

public interface BatchTestDao {
	public BatchTest getBatch(Long batchId);
	public BatchTest getTest(Long testId);
	public void insert (BatchTest batchTest);
	public void delete (BatchTest batchTest);
}
