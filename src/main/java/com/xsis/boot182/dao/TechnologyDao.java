package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Technology;

public interface TechnologyDao {
	
	public Technology get(Long id);
	public void insert(Technology technology);
	public void delete(Technology technology);
	public void update(Technology technology);
	public List<Technology> getList();
	public List<Technology> search(String search);

}
