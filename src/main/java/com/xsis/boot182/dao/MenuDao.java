package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Menu;

public interface MenuDao {
	public Menu get(Long id);
	public void insert(Menu menu);
	public void delete(Menu menu);
	public void update(Menu menu);
	public List <Menu> getList();
	public List<Menu> search(String search);
	public String GeneratedCode();
	/*public String GeneratedCode();*/
}
