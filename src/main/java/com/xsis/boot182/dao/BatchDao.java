package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Batch;

public interface BatchDao {
	public Batch get(Long id);
	public void insert(Batch batch);
	public void delete(Batch batch);
	public void update(Batch batch);
	public List<Batch> search(String search);
	public List<Batch> getList();
}
