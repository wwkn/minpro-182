package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Category;

public interface CategoryDao {
	public Category get (Long id);
	public void insert (Category category);
	public void delete (Category category);
	public void update (Category category);
	public List<Category> geList();
}
