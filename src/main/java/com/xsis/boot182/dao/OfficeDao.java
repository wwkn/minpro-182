package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Office;

public interface OfficeDao {
	public Office get(Long id);
	public void insert(Office office);
	public void delete(Office Office);
	public void update(Office Office);
	public List<Office> getList();
	
}