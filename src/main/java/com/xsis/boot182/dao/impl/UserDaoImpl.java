package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.UserDao;
import com.xsis.boot182.model.User;

@Repository
public class UserDaoImpl implements UserDao{
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public User get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(User.class, id);
	}
	
	@Override
	public void insert(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.save(user);
	}

	@Override
	public void update(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.update(user);
	}
	
	@Override
	public void delete(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(user);
	}

	@Override
	public List<User> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql="select u from User u join Role r on r.id = u.createdBy where u.isDelete=0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

	@Override
	public User getByUsername(String username) {
		Session session = sessionFactory.getCurrentSession();
		String hql="select u from User u join Role r on r.id = u.createdBy where u.username=:username";
		Query query = session.createQuery(hql);
		query.setParameter("username", username);
		User user = (User)query.getSingleResult();
		return user;
	}

	@Override
	public List<User> search(String search) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select u from User u where u.isDelete=0 and u.username like :abc";
		Query query = session.createQuery(hql);
		query.setParameter("abc", "%"+search+"%");
		return query.getResultList();
	}
}
