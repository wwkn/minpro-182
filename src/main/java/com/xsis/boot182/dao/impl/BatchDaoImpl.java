package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.BatchDao;
import com.xsis.boot182.model.Batch;

@Repository
public class BatchDaoImpl implements BatchDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Batch get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Batch.class, id);
	}

	@Override
	public void insert(Batch batch) {
		Session session = sessionFactory.getCurrentSession();
		session.save(batch);
	}

	@Override
	public void delete(Batch batch) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(batch);
	}

	@Override
	public void update(Batch batch) {
		Session session = sessionFactory.getCurrentSession();
		session.update(batch);
	}

	@Override
	public List<Batch> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select b from Batch b join Technology tc on tc.id=b.technologyId join Trainer tr on tr.id=b.trainerId join Room r on r.id=b.roomId join BootcampType bt on bt.id=b.bootcampTypeId where b.isDelete=0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

	@Override
	public List<Batch> search(String search) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select b from Batch b join Technology tc on tc.id=b.technologyId join Trainer tr on tr.id=b.trainerId join Room r on r.id=b.roomId join BootcampType bt on bt.id=b.bootcampTypeId where b.name like :search and b.isDelete=0 or b.technology.name like :search and b.isDelete=0";
		Query query = session.createQuery(hql);
		query.setParameter("search", "%"+search+"%");
		return query.getResultList();
	}

}
