package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.MenuAccessDao;
import com.xsis.boot182.model.MenuAccess;

@Repository
public class MenuAccessDaoImpl implements MenuAccessDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public MenuAccess get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(MenuAccess.class, id);
	}

	@Override
	public List<MenuAccess> getList() {
		Session session =sessionFactory.getCurrentSession();
		String hql="select ma from MenuAccess ma join Role r on r.id=ma.roleId join Menu m on m.id=ma.menuId";
		Query query=session.createQuery(hql);
		return query.getResultList();
	}

	@Override
	public void insert(MenuAccess menuAccess) {
		Session session =sessionFactory.getCurrentSession();
		session.save(menuAccess);

	}

	@Override
	public void delete(MenuAccess menuAccess) {
		Session session =sessionFactory.getCurrentSession();
		session.delete(menuAccess);

	}
	
	@Override
	public List<MenuAccess> search(Long search) {
		Session session =sessionFactory.getCurrentSession();
		String hql="select ma from MenuAccess ma join Role r on r.id=ma.roleId join Menu m on m.id=ma.menuId where ma.roleId like :searchId";
		Query query=session.createQuery(hql);
		query.setParameter("searchId", search);
		return query.getResultList();
	}


}
