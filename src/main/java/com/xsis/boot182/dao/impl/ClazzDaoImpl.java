package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.ClazzDao;
import com.xsis.boot182.model.Clazz;

@Repository
public class ClazzDaoImpl implements ClazzDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Clazz get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Clazz.class, id);
	}

	@Override
	public void insert(Clazz clazz) {
		Session session = sessionFactory.getCurrentSession();
		session.save(clazz);
	}

	@Override
	public void update(Clazz clazz) {
		Session session = sessionFactory.getCurrentSession();
		session.update(clazz);
		
	}

	@Override
	public void delete(Clazz clazz) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(clazz);
		
	}

	@Override
	public List<Clazz> search(String search) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select c from Clazz c join Batch b on b.id=c.batchId join Biodata bi on bi.id=c.biodataId where c.batch.name like :search";
		Query query = session.createQuery(hql);
		query.setParameter("search", "%"+search+"%");
		return query.getResultList();
	}

	@Override
	public List<Clazz> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select c from Clazz c join Batch b on b.id=c.batchId join Biodata bi on bi.id=c.biodataId";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

}
