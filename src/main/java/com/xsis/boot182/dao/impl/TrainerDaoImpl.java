package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.TrainerDao;
import com.xsis.boot182.model.Trainer;

@Repository
public class TrainerDaoImpl implements TrainerDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Trainer get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Trainer.class, id);
	}

	@Override
	public void insert(Trainer trainer) {
		Session session = sessionFactory.getCurrentSession();
		session.save(trainer);
		
	}

	@Override
	public void delete(Trainer trainer) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(trainer);
		
	}

	@Override
	public void update(Trainer trainer) {
		Session session = sessionFactory.getCurrentSession();
		session.update(trainer);
		
	}

	@Override
	public List<Trainer> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select t from Trainer t join User u on u.id = t.createdBy where t.isDelete=0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

	@Override
	public List<Trainer> search(String search) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select t from Trainer t where t.isDelete=0 and t.name like :abc";
		Query query = session.createQuery(hql);
		query.setParameter("abc", "%"+search+"%");
		
		return query.getResultList();
	}
	

}
