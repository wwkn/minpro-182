package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.AssignmentDao;
import com.xsis.boot182.model.Assignment;

@Repository
public class AssignmentDaoImpl implements AssignmentDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	//untuk get session data id assignment
	@Override
	public Assignment get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Assignment.class, id);
	}

	//untuk insert data berdasarkan session yang login
	@Override
	public void insert(Assignment assignment) {
		Session session = sessionFactory.getCurrentSession();
		session.save(assignment);
	}

	//untuk update data berdasarkan session yang login
	@Override
	public void update(Assignment assignment) {
		Session session = sessionFactory.getCurrentSession();
		session.update(assignment);
	}

	//untuk delete data berdasarkan session yang login
	@Override
	public void delete(Assignment assignment) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(assignment);
	}

	@Override
	public List<Assignment> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select a from Assignment a join User u on u.id = a.createdBy join  Biodata b on b.id = a.testId where a.isDelete=0 and a.isHold=0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}
	

	
	@Override
	public List<Assignment> search(String search) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select a from Assignment a join Biodata b on b.id = a.testId where a.isDelete=0 and b.name like :abc";
		Query query = session.createQuery(hql);
		query.setParameter("abc", "%"+search+"%");
		return query.getResultList();
	}

}
