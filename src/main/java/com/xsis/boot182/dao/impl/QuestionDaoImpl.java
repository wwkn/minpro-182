package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.QuestionDao;
import com.xsis.boot182.model.Question;

@Repository
public class QuestionDaoImpl  implements QuestionDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Question get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Question.class, id);
	}

	@Override
	public void insert(Question question) {
		Session session = sessionFactory.getCurrentSession();
		session.save(question);
	}

	@Override
	public void update(Question question) {
		Session session = sessionFactory.getCurrentSession();
		session.update(question);
	}

	@Override
	public void delete(Question question) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(question);
	}

	@Override
	public List<Question> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "SELECT q FROM Question q WHERE isDelete = 0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

}
