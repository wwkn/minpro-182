package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.BootcampTestTypeDao;
import com.xsis.boot182.model.BootcampTestType;

@Repository
public class BootcampTestTypeDaoImpl implements BootcampTestTypeDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public BootcampTestType get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(BootcampTestType.class, id);
	}

	@Override
	public void insert(BootcampTestType btt) {
		Session session = sessionFactory.getCurrentSession();
		session.save(btt);
	}

	@Override
	public void delete(BootcampTestType btt) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(btt);
	}

	@Override
	public void update(BootcampTestType btt) {
		Session session = sessionFactory.getCurrentSession();
		session.update(btt);
	}

	@Override
	public List<BootcampTestType> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select b from BootcampTestType b join User u on u.id = b.createdBy where b.isDelete = 0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

}
