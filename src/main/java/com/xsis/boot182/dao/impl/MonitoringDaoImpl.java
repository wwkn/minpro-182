package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.MonitoringDao;
import com.xsis.boot182.model.Monitoring;

@Repository
public class MonitoringDaoImpl implements MonitoringDao {

	@Autowired
	private SessionFactory SessionFactory;
	
	@Override
	public Monitoring get(Long id) {
		Session session = SessionFactory.getCurrentSession();
		return session.get(Monitoring.class, id);
	}

	@Override
	public void insert(Monitoring monitoring) {
		Session session = SessionFactory.getCurrentSession();
		session.save(monitoring);
	}

	@Override
	public void delete(Monitoring monitoring) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Monitoring monitoring) {
		Session session = SessionFactory.getCurrentSession();
		session.update(monitoring);
	}

	@Override
	public List<Monitoring> search(String search) {
		Session session = SessionFactory.getCurrentSession();
		String hql = "select m from Monitoring m join Biodata b on b.id = m.biodataId where m.isDelete=0 and b.name like :abc";
		Query query = session.createQuery(hql);
		query.setParameter("abc", "%"+search+"%");
		return query.getResultList();
	}

	@Override
	public List<Monitoring> getList() {
		Session session = SessionFactory.getCurrentSession();
		String hql = "select m from Monitoring m join Biodata b on b.id = m.biodataId where m.isDelete=0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

}
