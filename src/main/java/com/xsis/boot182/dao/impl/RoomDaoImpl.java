package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.RoomDao;
import com.xsis.boot182.model.Role;
import com.xsis.boot182.model.Room;
@Repository
public class RoomDaoImpl implements RoomDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Room get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Room.class, id);
	}
	
	@Override
	public void insert(Room room) {
		Session session = sessionFactory.getCurrentSession();
		session.save(room);
	}
	
	@Override
	public void delete(Room room) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(room);
	}
	
	@Override
	public void update(Room room) {
		Session session = sessionFactory.getCurrentSession();
		session.update(room);
	}

	@Override
	public List<Room> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select r from Room r join User u on u.id = r.createdBy where r.isDelete=0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

	@Override
	public String generateCode() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select r from Room where r.isDelete=0 and "; 
		return null;
	}

	@Override
	public List<Room> listRoomByOffice(Long officeId) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select r from Room r where r.officeId = :idOffice";
		Query query = session.createQuery(hql);
		query.setParameter("idOffice", officeId);
		return null;
	}

}