package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.TestDao;
import com.xsis.boot182.model.Test;

@Repository
public class TestDaoImpl implements TestDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Test get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Test.class, id);
	}

	@Override
	public void insert(Test test) {
		Session session = sessionFactory.getCurrentSession();
		session.save(test);
	}

	@Override
	public void delete(Test test) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(test);
	}

	@Override
	public void update(Test test) {
		Session session = sessionFactory.getCurrentSession();
		session.update(test);
	}

	@Override
	public List<Test> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select t from Test t join User u on u.id = t.createdBy where t.isDelete=0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

	@Override
	public List<Test> search(String search) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select t from Test t join User u on u.id = t.createdBy where t.name like :search and t.isDelete=0";
		Query query = session.createQuery(hql);
		query.setParameter("search", "%"+search+"%");
		return query.getResultList();
	}

	@Override
	public List<Test> filterList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select t from Test t where t.isBootcampTest = 1 and t.isDelete = 0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}
}
