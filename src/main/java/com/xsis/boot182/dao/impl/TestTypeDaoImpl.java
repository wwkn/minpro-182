package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.TestTypeDao;
import com.xsis.boot182.model.TestType;

@Repository
public class TestTypeDaoImpl implements TestTypeDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public TestType get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(TestType.class, id);
	}

	@Override
	public void insert(TestType testType) {
		Session session = sessionFactory.getCurrentSession();
		session.save(testType);
	}
	@Override
	public void delete(TestType testType) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(testType);
	}
	@Override
	public void update(TestType testType) {
		Session session = sessionFactory.getCurrentSession();
		session.update(testType);
	}
	@Override
	public List<TestType> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select ty from TestType ty join User u on u.id = ty.createdBy where ty.isDelete=0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}
}