package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.TestimonyDao;
import com.xsis.boot182.model.Testimony;

@Repository
public class TestimonyDaoImpl implements TestimonyDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Testimony get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Testimony.class, id);
	}

	@Override
	public void insert(Testimony testimony) {
		Session session = sessionFactory.getCurrentSession();
		session.save(testimony);

	}

	@Override
	public void delete(Testimony testimony) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(testimony);

	}

	@Override
	public void update(Testimony testimony) {
		Session session = sessionFactory.getCurrentSession();
		session.update(testimony);

	}

	@Override
	public List<Testimony> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select ts from Testimony ts join User u on u.id = ts.createdBy where ts.isDelete = 0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

	@Override
	public List<Testimony> search(String search) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select ts from Testimony ts where ts.isDelete=0 and ts.title like :abc";
		Query query = session.createQuery(hql);
		query.setParameter("abc", "%"+search+"%");
		
		return query.getResultList();
	}

}
