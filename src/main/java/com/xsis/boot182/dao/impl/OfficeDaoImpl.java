package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.OfficeDao;
import com.xsis.boot182.model.Office;

@Repository
public class OfficeDaoImpl implements OfficeDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Office get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Office.class, id);
	}

	@Override
	public void insert(Office office) {
		Session session = sessionFactory.getCurrentSession();
		session.save(office);
	}

	@Override
	public void delete(Office office) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(office);
	}

	@Override
	public void update(Office office) {
		Session session = sessionFactory.getCurrentSession();
		session.update(office);
	}

	@Override
	public List<Office> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select o from Office o join User u on u.id = o.createdBy where o.isDelete = 0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

}