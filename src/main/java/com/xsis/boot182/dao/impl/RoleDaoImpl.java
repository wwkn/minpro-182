package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.RoleDao;
import com.xsis.boot182.model.Role;

@Repository
public class RoleDaoImpl implements RoleDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Role get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Role.class, id);
	}
	
	@Override
	public void insert(Role role) {
		Session session = sessionFactory.getCurrentSession();
		session.save(role);
	}
	
	@Override
	public void delete(Role role) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(role);
	}
	
	@Override
	public void update(Role role) {
		Session session = sessionFactory.getCurrentSession();
		session.update(role);
	}

	@Override
	public List<Role> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select r from Role r join User u on u.id = r.createdBy where r.isDelete=0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

	@Override
	public String generateCode() {
		Session session = sessionFactory.getCurrentSession();
		//mengambil field code di table role YANG PALING BESAR
		String hql = "select r from Role r where r.isDelete=0 and r.code=(select max(code) from Role)";
		Query query = session.createQuery(hql);
		//(Role) untuk casting
		Role role = (Role) query.getSingleResult();
		String newCode="";
		if(role==null) {
			newCode = "RO0001";
		} else {
			String oldCode = role.getCode().split("O")[1];
			newCode = "RO"+(Integer.parseInt(oldCode)+1);
		}
		return newCode;
	}

	@Override
	public Boolean checkName(String name) {
		Session session = sessionFactory.getCurrentSession();
		// :xyz --> parameter xyz 
		String hql = "select r.name from Role r where r.isDelete=0 and r.name= :xyz";
		Query query = session.createQuery(hql);
		query.setParameter("xyz", name);
		List<Role> listQ = query.getResultList();
		if (listQ!=null) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public List<Role> search(String search){
		Session session = sessionFactory.getCurrentSession();
		String hql = "select r from Role r where r.isDelete=0 and r.name like :name";
		Query query = session.createQuery(hql);
		query.setParameter("name", "%"+search+"%");
		return query.getResultList();
	}


}
