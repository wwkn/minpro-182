package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.DocumentTestDetailDao;

import com.xsis.boot182.model.DocumentTestDetail;

@Repository
public class DocumentTestDetailDaoImpl implements DocumentTestDetailDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public DocumentTestDetail get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(DocumentTestDetail.class, id);
	}

	@Override
	public void insert(DocumentTestDetail documentTestDetail) {
		Session session = sessionFactory.getCurrentSession();
		session.save(documentTestDetail);
	}

	@Override
	public void update(DocumentTestDetail documentTestDetail) {
		Session session = sessionFactory.getCurrentSession();
		session.update(documentTestDetail);
	}

	@Override
	public void delete(DocumentTestDetail documentTestDetail) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(documentTestDetail);
	}

	@Override
	public List<DocumentTestDetail> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "SELECT dt FROM DocumentTestDetail dt";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

	@Override
	public List<DocumentTestDetail> listDocDetailByDocTest(Long idDocumentTest) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "SELECT d FROM DocumentTestDetail d WHERE d.documentTestId = :idDocumentTest";
		Query query = session.createQuery(hql);
		query.setParameter("idDocumentTest", idDocumentTest);
		
		return query.getResultList();
	}
	


}
