package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.FeedbackDao;
import com.xsis.boot182.model.DocumentTestDetail;
import com.xsis.boot182.model.Feedback;

@Repository
public class FeedbackDaoImpl implements FeedbackDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Feedback get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Feedback.class, id);
	}

	@Override
	public void insert(Feedback feedback) {
		Session session = sessionFactory.getCurrentSession();
		session.save(feedback);
	}

	@Override
	public void update(Feedback feedback) {
		Session session = sessionFactory.getCurrentSession();
		session.update(feedback);
	}

	@Override
	public void delete(Feedback feedback) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(feedback);
	}

	@Override
	public List<Feedback> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "SELECT f FROM Feedback f";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

}
