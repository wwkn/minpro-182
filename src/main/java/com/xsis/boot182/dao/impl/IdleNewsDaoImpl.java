package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.IdleNewsDao;
import com.xsis.boot182.model.IdleNews;

@Repository
public class IdleNewsDaoImpl implements IdleNewsDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public IdleNews get(Long id) {
		Session session =sessionFactory.getCurrentSession();		
		return session.get(IdleNews.class, id);
	}

	@Override
	public List<IdleNews> getList() {
		Session session =sessionFactory.getCurrentSession();
		String hql="select idn from IdleNews idn join User u on u.id=idn.createdBy join Category c on c.id=idn.categoryId where idn.isDelete=0";
		Query query=session.createQuery(hql);
		return query.getResultList();
	}

	@Override
	public void insert(IdleNews idleNews) {
		Session session =sessionFactory.getCurrentSession();
		session.save(idleNews);
		
	}

	@Override
	public void update(IdleNews idleNews) {
		Session session =sessionFactory.getCurrentSession();
		session.update(idleNews);
		
	}

	@Override
	public void delete(IdleNews idleNews) {
		Session session =sessionFactory.getCurrentSession();
		session.update(idleNews);
		
	}

	@Override
	public List<IdleNews> search(String search) {
		Session session =sessionFactory.getCurrentSession();
		String hql="select idn from IdleNews idn join User u on u.id=idn.createdBy join Category c on c.id=idn.categoryId where idn.isDelete=0 and idn.title like :searchTitle";
		Query query=session.createQuery(hql);
		query.setParameter("searchTitle", "%"+search+"%");
		return query.getResultList();
	}

}
