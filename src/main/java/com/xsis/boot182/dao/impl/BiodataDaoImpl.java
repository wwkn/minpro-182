package com.xsis.boot182.dao.impl;

import java.util.List;

import org.hibernate.Criteria;

//import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.BiodataDao;
import com.xsis.boot182.model.Biodata;

@Repository
public class BiodataDaoImpl implements BiodataDao {

	@Autowired
	private SessionFactory SessionFactory;
	
	@Override
	public Biodata get(Long id) {
		Session session = SessionFactory.getCurrentSession();
		return session.get(Biodata.class, id);
	}

	@Override
	public void insert(Biodata biodata) {
		Session session = SessionFactory.getCurrentSession();
		session.save(biodata);
	}

	@Override
	public void delete(Biodata biodata) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Biodata biodata) {
		Session session = SessionFactory.getCurrentSession();
		session.update(biodata);

	}

	@Override
	public List<Biodata> getList() {
		Session session = SessionFactory.getCurrentSession();
		String hql = "select b from Biodata b join BootcampTestType btt on btt.id = b.bootcampTestType where b.isDelete=0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

	@Override
	public List<Biodata> search(String search) {
		Session session = SessionFactory.getCurrentSession();
		String hql = "select b from Biodata b where b.isDelete=0 and b.name like :abc";
		Query query = session.createQuery(hql);
		query.setParameter("abc", "%"+search+"%");
		/*List crit = session.createCriteria(Biodata.class).add(Restrictions.like("name", "%"+search+"%")).list();
		return crit;*/
		return query.getResultList();
	}

	private void add(SimpleExpression like) {
		// TODO Auto-generated method stub
		
	}

}
