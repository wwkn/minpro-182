package com.xsis.boot182.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.BatchTestDao;
import com.xsis.boot182.model.BatchTest;

@Repository
public class BatchTestDaoImpl implements BatchTestDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void insert(BatchTest batchTest) {
		Session session = sessionFactory.getCurrentSession();
		session.save(batchTest);
	}

	@Override
	public BatchTest getBatch(Long batchId) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(BatchTest.class, batchId);
	}

	@Override
	public BatchTest getTest(Long testId) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(BatchTest.class, testId);
	}

	@Override
	public void delete(BatchTest batchTest) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(batchTest);
	}
	
	
}
