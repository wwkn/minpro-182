package com.xsis.boot182.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.MenuDao;
import com.xsis.boot182.model.Menu;


@Repository
public class MenuDaoImpl implements MenuDao{
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Menu get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Menu.class, id);
	}

	@Override
	public void insert(Menu menu) {
		Session session = sessionFactory.getCurrentSession();
		session.save(menu);
	}

	@Override
	public void delete(Menu menu) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(menu);
	}

	@Override
	public void update(Menu menu) {
		Session session = sessionFactory.getCurrentSession();
		session.update(menu);
	}

	@Override
	public List<Menu> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select m from Menu m join User u on u.id = m.createdBy where m.isDelete=0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

	@Override
	public List<Menu> search(String search) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select m from Menu m where m.isDelete=0 and m.title like :abc";
		Query query = session.createQuery(hql);
		query.setParameter("abc", "%"+search+"%");
		return query.getResultList();
	}

	@Override
	public String GeneratedCode() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select m from Menu m where m.isDelete=0 and m.code=(select max(code) from Menu)";
		Query query = session.createQuery(hql);
		Menu menu = (Menu) query.getSingleResult();
		String newCode="";
		if(menu==null) {
			newCode = "M0001";
		} else {
			String oldCode = menu.getCode().split("M")[1];
			newCode = "M" +(Integer.parseInt(oldCode)+1);
		}
			return newCode;
		}	

	/*@Override
	public String getGenerateCode() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yy/MM");
        String tgl = dateFormat.format(Calendar.getInstance().getTime());
        String i;
        Menu menu = null;
		String id = menu.getId();
        if (id == null) {
            id = "000001";
        } else {
            id = String.valueOf(Integer.valueOf(id) + 1);
        }
        String comCod = "";
        switch (id.length()) {
            case 1:
                id = "00000" + id;
                break;
            case 2:
                id = "0000" + id;
                break;
            case 3:
                id = "000" + id;
                break;
            case 4:
                id = "00" + id;
                break;
            case 5:
                id = "0" + id;
                break;
        }
        comCod = "M" +  id;
        return comCod;
	}
	*/
	


}
