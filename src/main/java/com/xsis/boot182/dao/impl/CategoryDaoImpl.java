package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.CategoryDao;
import com.xsis.boot182.model.Category;

@Repository
public class CategoryDaoImpl implements CategoryDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Category get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Category.class, id);
	}

	@Override
	public void insert(Category category) {
		Session session = sessionFactory.getCurrentSession();
		session.save(category);
	}

	@Override
	public void delete(Category category) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(category);
	}

	@Override
	public void update(Category category) {
		Session session = sessionFactory.getCurrentSession();
		session.update(category);
	}

	@Override
	public List<Category> geList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select c from Category c";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

}
