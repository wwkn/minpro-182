package com.xsis.boot182.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.boot182.dao.TechnologyTrainerDao;
import com.xsis.boot182.model.Technology;
import com.xsis.boot182.model.TechnologyTrainer;

@Repository
public class TechnologyTrainerDaoImpl implements TechnologyTrainerDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public TechnologyTrainer get(Long id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(TechnologyTrainer.class, id);
	}

	@Override
	public void insert(TechnologyTrainer technologyTrainer) {
		Session session = sessionFactory.getCurrentSession();
		session.save(technologyTrainer);

	}

	@Override
	public void update(TechnologyTrainer technologyTrainer) {
		Session session = sessionFactory.getCurrentSession();
		session.update(technologyTrainer);

	}

	@Override
	public void delete(TechnologyTrainer technologyTrainer) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(technologyTrainer);

	}

	@Override
	public List<TechnologyTrainer> idTechnology(Long id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select ttr from TechnologyTrainer ttr where ttr.technologyId = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		return query.getResultList();
	}

	@Override
	public List<TechnologyTrainer> getList() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select t from Technology t join TechnologyTrainer ttr on ttr.technologyId = t.id join Trainer tr on tr.id = ttr.trainerId join User u on u.id = t.createdBy where t.isDelete=0";
		Query query = session.createQuery(hql);
		return query.getResultList();
	}

}
