package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.User;

public interface UserDao {
	public User get(Long id);
	public User getByUsername(String username);
	public void insert(User user);
	public void delete(User user);
	public void update(User user);
	public List<User> getList();
	public List<User> search(String search);
}
