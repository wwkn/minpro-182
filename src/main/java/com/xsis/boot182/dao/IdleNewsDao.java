package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.IdleNews;

public interface IdleNewsDao {
	public IdleNews get(Long id);
	public List<IdleNews> getList();
	public List<IdleNews> search(String search);
	public void insert(IdleNews idleNews);
	public void update(IdleNews idleNews);
	public void delete(IdleNews idleNews);
	
}
