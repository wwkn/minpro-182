package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.TechnologyTrainer;

public interface TechnologyTrainerDao {

	public TechnologyTrainer get(Long id);

	public void insert(TechnologyTrainer technologyTrainer);

	public void update(TechnologyTrainer technologyTrainer);

	public void delete(TechnologyTrainer technologyTrainer);

	public List<TechnologyTrainer> idTechnology(Long id);

	public List<TechnologyTrainer> getList();

}
