package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Clazz;

public interface ClazzDao {
	public Clazz get(Long id);
	public void insert(Clazz clazz);
	public void update(Clazz clazz);
	public void delete(Clazz clazz);
	public List<Clazz> search(String search);
	public List<Clazz> getList();
}
