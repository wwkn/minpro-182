package com.xsis.boot182.dao;

import java.util.List;

import com.xsis.boot182.model.Trainer;

public interface TrainerDao {

	public Trainer get(Long id);
	public void insert(Trainer trainer);
	public void delete(Trainer trainer);
	public void update(Trainer trainer);
	public List<Trainer> search(String search);
	public List<Trainer> getList();
	
}
