package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.OfficeDao;
import com.xsis.boot182.dao.RoomDao;
import com.xsis.boot182.model.Office;
import com.xsis.boot182.model.OfficeViewModel;
import com.xsis.boot182.model.Room;
@Service
@Transactional
public class OfficeServiceImpl implements OfficeService {

	@Autowired
	private OfficeDao officeDao;
	@Autowired
	private RoomDao roomDao;
	
	@Override
	public List<Office> getList() {
		return this.officeDao.getList();
	}

	@Override
	public Office getData(Long id) {
		return this.officeDao.get(id);
	}

	@Override
	public void insert(Office office) {
		this.officeDao.insert(office);
	}

	@Override
	public void delete(Office office) {
		this.officeDao.delete(office);
	}

	@Override
	public void update(Office office) {
		this.officeDao.update(office);
	}
}