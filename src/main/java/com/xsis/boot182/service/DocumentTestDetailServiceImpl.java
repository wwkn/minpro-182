package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.DocumentTestDetailDao;
import com.xsis.boot182.model.DocumentTestDetail;

@Service
@Transactional
public class DocumentTestDetailServiceImpl implements DocumentTestDetailService {

	@Autowired
	private DocumentTestDetailDao documentTestDetailDao;

	@Override
	public List<DocumentTestDetail> getList() {
		return this.documentTestDetailDao.getList();
	}

	@Override
	public DocumentTestDetail getData(Long id) {
		return this.documentTestDetailDao.get(id);
	}

	@Override
	public void insert(DocumentTestDetail documentTestDetail) {
		this.documentTestDetailDao.insert(documentTestDetail);
	}

	@Override
	public void update(DocumentTestDetail documentTestDetail) {
		this.documentTestDetailDao.update(documentTestDetail);
	}

	@Override
	public void delete(DocumentTestDetail documentTestDetail) {
		this.documentTestDetailDao.delete(documentTestDetail);
	}

	@Override
	public List<DocumentTestDetail> listDocTestDetailByDocTest(Long idDocumentTest) {
		return this.documentTestDetailDao.listDocDetailByDocTest(idDocumentTest);
	}

}
