package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Test;

public interface TestService {
	public List<Test> getList();
	public List<Test> search(String search);
	public List<Test> filterList();
	public Test getData(Long id);
	public void insert(Test test);
	public void delete(Test test);
	public void update(Test test);
}
