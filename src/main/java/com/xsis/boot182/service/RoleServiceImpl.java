package com.xsis.boot182.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.boot182.dao.RoleDao;
import com.xsis.boot182.model.Role;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;
	
	
	@Override
	public List<Role> getList() {
		return this.roleDao.getList();
	}

	@Override
	public Role getData(Long id) {
		return this.roleDao.get(id);
	}

	@Override
	public void insert(Role role) {
		this.roleDao.insert(role);
	}

	@Override
	public void delete(Role role) {
		this.roleDao.delete(role);
	}

	@Override
	public void update(Role role) {
		this.roleDao.update(role);
	}

	@Override
	public String generateCode() {
		return this.roleDao.generateCode();
	}

	@Override
	public Boolean checkName(String name) {
		return this.roleDao.checkName(name);
	}

	@Override
	public List<Role> search(String search) {
		return this.roleDao.search(search);
	}

}
