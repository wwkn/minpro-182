package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.TestimonyDao;
import com.xsis.boot182.model.Testimony;

@Service
@Transactional
public class TestimonyServiceImpl implements TestimonyService {

	@Autowired
	private TestimonyDao testimonyDao;
	
	@Override
	public List<Testimony> getList() {
		return this.testimonyDao.getList();
	}

	@Override
	public Testimony getData(Long id) {
		return this.testimonyDao.get(id);
	}

	@Override
	public void insert(Testimony testimony) {
		this.testimonyDao.insert(testimony);

	}

	@Override
	public void delete(Testimony testimony) {
		this.testimonyDao.delete(testimony);

	}

	@Override
	public void update(Testimony testimony) {
		this.testimonyDao.update(testimony);

	}

	@Override
	public List<Testimony> search(String search) {
		return this.testimonyDao.search(search);
	}

}
