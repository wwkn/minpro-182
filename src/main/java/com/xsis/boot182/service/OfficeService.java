package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Office;
import com.xsis.boot182.model.OfficeViewModel;

public interface OfficeService {
	public List<Office> getList();
	public Office getData(Long id);
	public void insert(Office office);
	public void delete(Office office);
	public void update(Office office);
}