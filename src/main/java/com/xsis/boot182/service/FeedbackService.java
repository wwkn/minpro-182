package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Feedback;

public interface FeedbackService {
	public List<Feedback> getList();

	public Feedback getData(Long id);

	public void insert(Feedback feedback);

	public void update(Feedback feedback);

	public void delete(Feedback feedback);
}
