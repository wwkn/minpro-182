package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Monitoring;

public interface MonitoringService {
	public List<Monitoring> getList();
	public List<Monitoring> search(String search);
	public Monitoring getData(Long id);
	public void insert(Monitoring monitoring);
	public void delete(Monitoring monitoring);
	public void update(Monitoring monitoring);
}
