package com.xsis.boot182.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.boot182.dao.MenuDao;
import com.xsis.boot182.model.Menu;

@Service
@Transactional
public class MenuServiceImpl implements MenuService {

	@Autowired
	private MenuDao menuDao;
	
	@Override
	public List<Menu> getList() {
		return this.menuDao.getList();
	}

	@Override
	public Menu getData(Long id) {
		return this.menuDao.get(id);
	}

	@Override
	public void insert(Menu menu) {
		this.menuDao.insert(menu);
	}

	@Override
	public void update(Menu menu) {
		this.menuDao.update(menu);
	}

	@Override
	public void delete(Menu menu) {
		this.menuDao.delete(menu);
	}

	@Override
	public List<Menu> search(String search) {
		return this.menuDao.search(search);
	}

	@Override
	public String GeneratedCode() {
		return this.menuDao.GeneratedCode();
	}

	
}
