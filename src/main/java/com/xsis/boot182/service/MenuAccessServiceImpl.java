package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.MenuAccessDao;
import com.xsis.boot182.model.MenuAccess;


@Service
@Transactional
public class MenuAccessServiceImpl implements MenuAccessService {
	
	@Autowired
	private MenuAccessDao menuAccessDao;
	
	@Override
	public List<MenuAccess> getList() {
		return this.menuAccessDao.getList();
	}

	@Override
	public MenuAccess getData(Long id) {
		return this.menuAccessDao.get(id);
	}

	@Override
	public void insert(MenuAccess menuAccess) {
		this.menuAccessDao.insert(menuAccess);
		
	}

	@Override
	public void delete(MenuAccess menuAccess) {
		this.menuAccessDao.delete(menuAccess);
		
	}

	@Override
	public List<MenuAccess> search(Long search) {
		return this.menuAccessDao.search(search);
	}
}
