package com.xsis.boot182.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.boot182.dao.AssignmentDao;
import com.xsis.boot182.model.Assignment;

@Service
@Transactional
public class AssignmentServiceImpl implements AssignmentService {

	@Autowired
	private AssignmentDao assignmentDao;
	
	@Override
	public List<Assignment> getList() {
		return this.assignmentDao.getList();
		}

	@Override
	public Assignment getData(Long id) {
		return this.assignmentDao.get(id);
	}

	@Override
	public void insert(Assignment assignment) {
		this.assignmentDao.insert(assignment);
	}

	@Override
	public void update(Assignment assignment) {
		this.assignmentDao.update(assignment);
	}

	@Override
	public void delete(Assignment assignment) {
		this.assignmentDao.delete(assignment);
	}

	@Override
	public List<Assignment> search(String search) {
		return this.assignmentDao.search(search);
	}
	
	
}
