package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.TechnologyTrainer;

public interface TechnologyTrainerService {
	
	public List<TechnologyTrainer> idTechnology(Long id);
	
	public List<TechnologyTrainer> getList();

	public TechnologyTrainer getData(Long id);

	public void insert(TechnologyTrainer technologyTrainer);

	public void delete(TechnologyTrainer technologyTrainer);

	public void update(TechnologyTrainer technologyTrainer);
}
