package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.User;

public interface UserService {
	public List<User>getList();
	public User getData(Long id);
	public User getByUsername(String username);
	public void insert(User user);
	public void update(User user);
	public void delete(User user);
	public List<User> search(String search);
	
}
