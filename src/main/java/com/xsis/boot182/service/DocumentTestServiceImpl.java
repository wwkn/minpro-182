package com.xsis.boot182.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.boot182.dao.DocumentTestDao;
import com.xsis.boot182.dao.DocumentTestDetailDao;

import com.xsis.boot182.model.DocumentTest;
import com.xsis.boot182.model.DocumentTestDetail;
import com.xsis.boot182.model.DocumentTestViewModel;

@Service
@Transactional
public class DocumentTestServiceImpl implements DocumentTestService {

	@Autowired
	private DocumentTestDao documentTestDao;

	@Autowired
	private DocumentTestDetailDao documentTestDetailDao;

	@Override
	public void insert(DocumentTest documentTest) {
		this.documentTestDao.insert(documentTest);
	}

	@Override
	public List<DocumentTest> getList() {
		return this.documentTestDao.getList();
	}

	@Override
	public DocumentTest getData(Long id) {
		return this.documentTestDao.get(id);
	}

	@Override
	public void delete(DocumentTest documentTest) {
		this.documentTestDao.delete(documentTest);
	}

	@Override
	public void update(DocumentTest documentTest) {
		this.documentTestDao.update(documentTest);
	}

	@Override
	public void update(DocumentTestViewModel documentTestViewModel) {
		this.update(documentTestViewModel.getDocumentTest());
		if (documentTestViewModel.getDocumentTestDetail() != null) {
			for (DocumentTestDetail d : documentTestViewModel.getDocumentTestDetail()) {
				d.setDocumentTestId(documentTestViewModel.getDocumentTest().getId());
				d.setCreatedBy(documentTestViewModel.getCreatedBy());
				d.setCreatedOn(documentTestViewModel.getCreatedOn());
				this.documentTestDetailDao.insert(d);
			}
		}
	}

}
