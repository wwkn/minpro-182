package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Room;

public interface RoomService {
	public List<Room> getList();
	public Room getData(Long id);
	public void insert(Room room);
	public void update(Room room);
	public void delete(Room room);
	public List<Room> listRoomByOffice(Long officeId);
}
