package com.xsis.boot182.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.boot182.dao.UserDao;
import com.xsis.boot182.model.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	@Override
	public List<User> getList() {
		return this.userDao.getList();
	}

	@Override
	public User getData(Long id) {
		return this.userDao.get(id);
	}

	@Override
	public void insert(User user) {
		this.userDao.insert(user);
	}

	@Override
	public void update(User user) {
		this.userDao.update(user);
	}

	@Override
	public void delete(User user) {
		this.userDao.delete(user);
	}

	@Override
	public User getByUsername(String username) {
		return this.userDao.getByUsername(username);
	}

	@Override
	public List<User> search(String search) {
		return this.userDao.search(search);
	}

}
