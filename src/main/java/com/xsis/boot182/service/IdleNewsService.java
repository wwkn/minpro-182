package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.IdleNews;

public interface IdleNewsService {
	public List<IdleNews> getList();
	public List<IdleNews> search(String search);
	public IdleNews getData(Long id);
	public void insert (IdleNews idleNews);
	public void delete (IdleNews idleNews);
	public void update (IdleNews idleNews);
}
