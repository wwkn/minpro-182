package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.DocumentTestDetail;

public interface DocumentTestDetailService {
	public List<DocumentTestDetail> getList();

	public DocumentTestDetail getData(Long id);

	public void insert(DocumentTestDetail documentTestDetail);

	public void update(DocumentTestDetail documentTestDetail);

	public void delete(DocumentTestDetail documentTestDetail);
	
	public List<DocumentTestDetail> listDocTestDetailByDocTest(Long idDocumentTest);
}
