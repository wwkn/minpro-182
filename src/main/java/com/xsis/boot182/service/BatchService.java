package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Batch;

public interface BatchService {
	public List<Batch> getList();
	public List<Batch> search(String search);
	public Batch getData(Long id);
	public void insert(Batch batch);
	public void delete(Batch batch);
	public void update(Batch batch);
}
