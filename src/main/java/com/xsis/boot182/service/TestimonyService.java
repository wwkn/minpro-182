package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Testimony;

public interface TestimonyService {

	public List<Testimony> getList();

	public Testimony getData(Long id);

	public void insert(Testimony testimony);

	public void delete(Testimony testimony);

	public void update(Testimony testimony);
	
	public List<Testimony> search(String search);
}
