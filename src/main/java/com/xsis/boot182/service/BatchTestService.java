package com.xsis.boot182.service;

import com.xsis.boot182.model.BatchTest;

public interface BatchTestService {
	public BatchTest getBatch(Long batchId);
	public BatchTest getTest(Long testId);
	public void insert (BatchTest batchTest);
	public void delete (BatchTest batchTest);
}
