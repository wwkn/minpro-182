package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Clazz;

public interface ClazzService {
	public List<Clazz> getList();
	public List<Clazz> search(String search);
	public Clazz getData(Long id);
	public void insert(Clazz clazz);
	public void update(Clazz clazz);
	public void delete(Clazz clazz);
}
