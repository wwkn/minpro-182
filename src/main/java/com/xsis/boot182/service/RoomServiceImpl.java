package com.xsis.boot182.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.boot182.dao.RoomDao;
import com.xsis.boot182.model.Room;

@Service
@Transactional
public class RoomServiceImpl implements RoomService {

	@Autowired
	private RoomDao roomDao;
	
	
	@Override
	public List<Room> getList() {
		return this.roomDao.getList();
	}

	@Override
	public Room getData(Long id) {
		return this.roomDao.get(id);
	}

	@Override
	public void insert(Room room) {
		this.roomDao.insert(room);
	}

	@Override
	public void delete(Room room) {
		this.roomDao.delete(room);
	}

	@Override
	public void update(Room room) {
		this.roomDao.update(room);
	}

	@Override
	public List<Room> listRoomByOffice(Long officeId) {
		return this.roomDao.listRoomByOffice(officeId);
	}
}
