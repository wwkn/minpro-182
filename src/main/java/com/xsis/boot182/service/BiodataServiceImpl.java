package com.xsis.boot182.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.boot182.dao.BiodataDao;
import com.xsis.boot182.model.Biodata;

@Service
@Transactional
public class BiodataServiceImpl implements BiodataService {

	@Autowired
	private BiodataDao biodataDao;
	
	@Override
	public List<Biodata> getList() {
		return this.biodataDao.getList();
	}

	@Override
	public Biodata getData(Long id) {
		return this.biodataDao.get(id);
	}

	@Override
	public void insert(Biodata biodata) {
		this.biodataDao.insert(biodata);

	}

	@Override
	public void delete(Biodata biodata) {
		this.biodataDao.delete(biodata);

	}

	@Override
	public void update(Biodata biodata) {
		this.biodataDao.update(biodata);
	}

	@Override
	public List<Biodata> search(String search) {
		return this.biodataDao.search(search);
	}

}
