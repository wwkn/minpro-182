package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.CategoryDao;
import com.xsis.boot182.model.Category;
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {
	
	@Autowired
	private CategoryDao categoryDao;
	
	@Override
	public List<Category> getList() {
		return this.categoryDao.geList();
	}

	@Override
	public Category getData(Long id) {
		return this.categoryDao.get(id);
	}

	@Override
	public void insert(Category category) {
		this.categoryDao.insert(category);
	}

	@Override
	public void delete(Category category) {
		this.categoryDao.delete(category);
	}

	@Override
	public void update(Category category) {
		this.categoryDao.update(category);
	}

}
