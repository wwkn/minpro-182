package com.xsis.boot182.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.BatchTestDao;
import com.xsis.boot182.model.BatchTest;

@Service
@Transactional
public class BatchTestServiceImpl implements BatchTestService{
	
	@Autowired
	private BatchTestDao batchTestDao;

	@Override
	public void insert(BatchTest batchTest) {
		this.batchTestDao.insert(batchTest);
	}

	@Override
	public BatchTest getBatch(Long batchId) {
		return this.batchTestDao.getBatch(batchId);
	}

	@Override
	public BatchTest getTest(Long testId) {
		return this.batchTestDao.getTest(testId);
	}

	@Override
	public void delete(BatchTest batchTest) {
		this.batchTestDao.delete(batchTest);
	}
	
}
