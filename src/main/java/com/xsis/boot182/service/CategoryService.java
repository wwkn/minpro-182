package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Category;

public interface CategoryService {
	public List<Category> getList();
	public Category getData(Long id);
	public void insert(Category category);
	public void delete(Category category);
	public void update(Category category);
}
