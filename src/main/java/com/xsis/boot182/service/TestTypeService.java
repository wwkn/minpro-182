package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.TestType;

public interface TestTypeService {
	public List<TestType> getList();
	public TestType getData(Long id);
	public void insert(TestType TestType);
	public void delete(TestType TestType);
	public void update(TestType TestType);
}
