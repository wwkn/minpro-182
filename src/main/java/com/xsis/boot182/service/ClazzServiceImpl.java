package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.ClazzDao;
import com.xsis.boot182.model.Clazz;

@Service
@Transactional
public class ClazzServiceImpl implements ClazzService{
	
	@Autowired
	private ClazzDao clazzDao;
	
	@Override
	public List<Clazz> getList() {
		return this.clazzDao.getList();
	}

	@Override
	public List<Clazz> search(String search) {
		return this.clazzDao.search(search);
	}

	@Override
	public Clazz getData(Long id) {
		return this.clazzDao.get(id);
	}

	@Override
	public void insert(Clazz clazz) {
		this.clazzDao.insert(clazz);
	}

	@Override
	public void update(Clazz clazz) {
		this.clazzDao.update(clazz);
	}

	@Override
	public void delete(Clazz clazz) {
		this.clazzDao.delete(clazz);
	}

}
