package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.BootcampTypeDao;
import com.xsis.boot182.model.BootcampType;

@Service
@Transactional
public class BootcampTypeServiceImpl implements BootcampTypeService {

	
	@Autowired
	private BootcampTypeDao BtcDao;
	
	
	@Override
	public List<BootcampType> getList() {
		return this.BtcDao.getList();
	}

	@Override
	public BootcampType getData(Long id) {
		return this.BtcDao.get(id);
		
	}

	@Override
	public void insert(BootcampType bootcampType) {
		this.BtcDao.insert(bootcampType);

	}

	@Override
	public void delete(BootcampType bootcampType) {
		this.BtcDao.delete(bootcampType);

	}

	@Override
	public void update(BootcampType bootcampType) {
		this.BtcDao.update(bootcampType);

	}

	@Override
	public List<BootcampType> search(String search) {
		return this.BtcDao.search(search);
	}

}
