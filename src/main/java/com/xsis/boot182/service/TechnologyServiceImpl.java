package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.TechnologyDao;
import com.xsis.boot182.dao.TechnologyTrainerDao;
import com.xsis.boot182.dao.TrainerDao;
import com.xsis.boot182.model.Technology;
import com.xsis.boot182.model.TechnologyTrainer;
import com.xsis.boot182.model.TechnologyViewModel;
import com.xsis.boot182.model.Trainer;

@Service
@Transactional
public class TechnologyServiceImpl implements TechnologyService {

	@Autowired
	private TrainerDao trainerDao;

	@Autowired
	private TechnologyDao technologyDao;
	
	@Autowired
	private TechnologyTrainerDao technologyTrainerDao;

	@Override
	public List<Technology> getList() {
		return this.technologyDao.getList();
	}

	@Override
	public Technology getData(Long id) {
		return this.technologyDao.get(id);
	}

	@Override
	public void insert(Technology technology) {
		this.technologyDao.insert(technology);

	}

	@Override
	public void delete(Technology technology) {
		this.technologyDao.update(technology);
		Technology tech = getData(technology.getId());
		List<TechnologyTrainer> ttrList = this.technologyTrainerDao.idTechnology(tech.getId());
		for (TechnologyTrainer ttr : ttrList) {
			this.technologyTrainerDao.delete(ttr);
		}
		

	}

	@Override
	public void update(Technology technology) {
		this.technologyDao.update(technology);

	}

	@Override
	public void insert(TechnologyViewModel technologyViewModel) {
		this.insert(technologyViewModel.getTechnology());
		if (technologyViewModel.getTtr()!=null) {
			for (TechnologyTrainer ttr : technologyViewModel.getTtr()) {
				ttr.setTechnologyId(technologyViewModel.getTechnology().getId());
				ttr.setCreatedBy(technologyViewModel.getCreatedBy());
				ttr.setCreatedOn(technologyViewModel.getCreatedOn());
				this.technologyTrainerDao.insert(ttr);
			}
		}
	}

	@Override
	public List<Technology> search(String search) {
		return this.technologyDao.search(search);
	}

	@Override
	public void update(TechnologyViewModel technologyViewModel) {
		this.update(technologyViewModel.getTechnology());
		if (technologyViewModel.getTtr()!=null) {
			for (TechnologyTrainer ttr : technologyViewModel.getTtr()) {
				ttr.setTechnologyId(technologyViewModel.getTechnology().getId());
				ttr.setCreatedBy(technologyViewModel.getCreatedBy());
				ttr.setCreatedOn(technologyViewModel.getCreatedOn());
				this.technologyTrainerDao.insert(ttr);
			}
		}
		
	}
}
