package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.BootcampTestType;

public interface BootcampTestTypeService {
	public List<BootcampTestType> getList();
	public BootcampTestType getData(Long id);
	public void insert(BootcampTestType btt);
	public void delete(BootcampTestType btt);
	public void update(BootcampTestType btt);
}
