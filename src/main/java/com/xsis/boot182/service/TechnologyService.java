package com.xsis.boot182.service;

import java.util.List;

import com.xsis.boot182.model.Technology;
import com.xsis.boot182.model.TechnologyViewModel;

public interface TechnologyService {

	public List<Technology> getList();

	public Technology getData(Long id);

	public void insert(Technology technology);

	public void delete(Technology technology);

	public void update(Technology technology);	
	
	public void update(TechnologyViewModel technologyViewModel);
	
	public void insert(TechnologyViewModel technologyViewModel);

	public List<Technology> search(String search);
}
