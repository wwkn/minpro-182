package com.xsis.boot182.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.boot182.dao.IdleNewsDao;
import com.xsis.boot182.model.IdleNews;

@Service
@Transactional
public class IdleNewsServiceImpl implements IdleNewsService {
	
	@Autowired
	private IdleNewsDao idleNewsDao;
	
	@Override
	public List<IdleNews> getList() {
		return this.idleNewsDao.getList();
	}

	@Override
	public IdleNews getData(Long id) {
		return this.idleNewsDao.get(id);
	}

	@Override
	public void insert(IdleNews idleNews) {
		this.idleNewsDao.insert(idleNews);

	}

	@Override
	public void delete(IdleNews idleNews) {
		this.idleNewsDao.delete(idleNews);

	}

	@Override
	public void update(IdleNews idleNews) {
		this.idleNewsDao.update(idleNews);

	}

	@Override
	public List<IdleNews> search(String search) {
		return this.idleNewsDao.search(search);
	}

}
