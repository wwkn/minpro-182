--------------------------------------------------------
--  File created - Senin-Desember-31-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table T_IDLE_NEWS
--------------------------------------------------------

  CREATE TABLE "MINPRO"."T_IDLE_NEWS" 
   (	"ID" NUMBER(11,0), 
	"CATEGORY_ID" NUMBER(11,0), 
	"TITLE" VARCHAR2(255 BYTE), 
	"CONTENT" VARCHAR2(4000 BYTE), 
	"IS_PUBLISH" NUMBER(1,0), 
	"CREATED_BY" NUMBER(11,0), 
	"CREATED_ON" DATE, 
	"MODIFIED_BY" NUMBER(11,0), 
	"MODIFIED_ON" DATE, 
	"DELETED_BY" NUMBER(11,0), 
	"DELETED_ON" DATE, 
	"IS_DELETE" NUMBER(1,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index T_IDLE_NEWS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MINPRO"."T_IDLE_NEWS_PK" ON "MINPRO"."T_IDLE_NEWS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table T_IDLE_NEWS
--------------------------------------------------------

  ALTER TABLE "MINPRO"."T_IDLE_NEWS" ADD CONSTRAINT "T_IDLE_NEWS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "MINPRO"."T_IDLE_NEWS" MODIFY ("IS_DELETE" NOT NULL ENABLE);
  ALTER TABLE "MINPRO"."T_IDLE_NEWS" MODIFY ("CREATED_ON" NOT NULL ENABLE);
  ALTER TABLE "MINPRO"."T_IDLE_NEWS" MODIFY ("CREATED_BY" NOT NULL ENABLE);
  ALTER TABLE "MINPRO"."T_IDLE_NEWS" MODIFY ("IS_PUBLISH" NOT NULL ENABLE);
  ALTER TABLE "MINPRO"."T_IDLE_NEWS" MODIFY ("TITLE" NOT NULL ENABLE);
  ALTER TABLE "MINPRO"."T_IDLE_NEWS" MODIFY ("CATEGORY_ID" NOT NULL ENABLE);
  ALTER TABLE "MINPRO"."T_IDLE_NEWS" MODIFY ("ID" NOT NULL ENABLE);
