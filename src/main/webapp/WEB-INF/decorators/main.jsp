
<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<sec:authorize access="isAuthenticated()">
	<sec:authentication property="principal.username" var="username" />
</sec:authorize>
<sec:authorize access="!isAuthenticated()">
	<sec:authentication property="principal" var="username" />
</sec:authorize>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Mini Project</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="${contextName}/assets/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="${contextName}/assets/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="${contextName}/assets/Ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet"
	href="${contextName}/assets/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="${contextName}/assets/dist/css/skins/_all-skins.min.css">
<!-- Morris chart -->
<link rel="stylesheet" href="${contextName}/assets/morris.js/morris.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="${contextName}/assets/jvectormap/jquery-jvectormap.css">
<!-- Date Picker -->
<link rel="stylesheet"
	href="${contextName}/assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- Daterange picker -->
<link rel="stylesheet"
	href="${contextName}/assets/bootstrap-daterangepicker/daterangepicker.css">
<!-- bootstrap wysihtml5 - text editor -->

<link rel="stylesheet"
	href="${contextName}/assets/datatables.net-bs/css/dataTables.bootstrap.min.css">

 <!-- Select2 -->
  <link rel="stylesheet" href="${contextName}/assets/select2/dist/css/select2.min.css">
  
<!-- jQuery 3 -->
<script src="${contextName}/assets/jquery/dist/jquery.min.js"></script>
<script src="${contextName}/assets/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="${contextName}/assets/js/default.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<script>
	var contextName = '${contextName}';
</script>
<decorator:head />
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<%@include file="_header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@include file="_sidebar.jsp"%>

		<!-- konten render tengah -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>${judul}</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<decorator:body />
					</div>
				</div>
			</section>
		</div>

		<!-- /.content-wrapper -->
		<%@include file="_footer.jsp"%>

		<!-- Control Sidebar -->
		<%@include file="_control-sidebar.jsp"%>
		<!-- /.control-sidebar -->

		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery UI 1.11.4 -->
	<script src="${contextName}/assets/jquery-ui/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.7 -->
	
	<!-- Morris.js charts -->
	<script src="${contextName}/assets/raphael/raphael.min.js"></script>
	<script src="${contextName}/assets/morris.js/morris.min.js"></script>
	<!-- Sparkline -->
	<script
		src="${contextName}/assets/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
	<!-- jQuery Knob Chart -->
	<script src="${contextName}/assets/jquery-knob/dist/jquery.knob.min.js"></script>
	<!-- daterangepicker -->
	<script src="${contextName}/assets/moment/min/moment.min.js"></script>
	<script
		src="${contextName}/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
	<!-- datepicker -->
	<script
		src="${contextName}/assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<!-- Slimscroll -->
	<script
		src="${contextName}/assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="${contextName}/assets/fastclick/lib/fastclick.js"></script>
	<script
		src="${contextName}/assets/datatables.net/js/jquery.dataTables.min.js"></script>
	<script
		src="${contextName}/assets/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<!-- bootstrap datepicker -->
	<script
		src="${contextName}/assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
		<!-- Select2 -->
<script src="${contextName}/assets/select2/dist/js/select2.full.min.js"></script>
	<!-- AdminLTE App -->
	<script src="${contextName}/assets/dist/js/adminlte.min.js"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<%-- 	<script src="${contextName}/assets/dist/js/pages/dashboard.js"></script> --%>
	<!-- AdminLTE for demo purposes -->
	<%-- <script src="${contextName}/assets/dist/js/demo.js"></script> --%>

	<script>
		$(function() {
			$('#example1').DataTable()
			$('#example2').DataTable({
				'paging' : true,
				'lengthChange' : false,
				'searching' : false,
				'ordering' : true,
				'info' : true,
				'autoWidth' : false
			})
		})
	</script>

	<style>
.example-modal .modal {
	position: relative;
	top: auto;
	bottom: auto;
	right: auto;
	left: auto;
	display: block;
	z-index: 1;
}

.example-modal .modal {
	background: transparent !important;
}
</style>
</body>
</html>
