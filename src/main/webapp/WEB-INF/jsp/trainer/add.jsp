<form role="form" id="addTrainer">
	<div class="box-body">
		<div class="form-group">
			<label>Nama</label> 
			<input class="form-control" name="name"
				placeholder="Name" id="add-name">
		</div>

		<div class="form-group">
			<label>Notes</label>
			<textarea class="form-control" name="notes" id="add-notes"
				placeholder="Notes"></textarea>
		</div>

		<div class="box-footer">
			<button type="button" class="btn btn-warning" onclick="simpanData()"
				data-dismiss="modal">Save</button>
			<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
		</div>
	</div>
</form>
