
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">TRAINER LIST</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">

			<div class="col-md-3">
				<div class="input-group">
					<input type="text" id="txt-search" placeholder="Search by name" class="form-control"> <span
						class="input-group-btn">
						<button class="btn btn-warning" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive table-bordered table-hover">
			<thead>
				<tr>
					<th>NAME</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="trainerlist">

				<c:forEach items="${list}" var="trainer">
					<tr>
						<td>${trainer.name}</td>

						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="loadDataGet(${trainer.id})">Edit</a>
									</li>
									<li><a href="#" onclick="loadDataGetDelete(${trainer.id})">Delete</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</div>
<!-- Modal-begin -->
<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>
<!-- Modal-end -->


<script>
	$("#btn-add").click(function() {

		$.ajax({
			url : '${contextName}/trainer/add.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.modal-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");
				$("#myModal").find('.box-title').html('Add Trainer');
			}
		});
	});

	function simpanData() {
		var dataformatnyajson = getFormData($('#addTrainer'));

		$.ajax({
			type : 'post',
			url : contextName + '/api/trainer/',
			data : JSON.stringify(dataformatnyajson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(data) {
				alert('Data successfully saved!');
				$('#myModal').modal('hide');
				loadData();
			},
			error : function(data) {
				alert('gagal');
			}
		});

	}
	
	function show(data){
		result = '';
		$(data).each(array);
		$('#trainerlist').html(result);
	}
	
	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/trainer/isi.html',
			success : function(d) {
				$('#trainerlist').html(d);
			}
		});
	}

	$(document).ready(function() {
		loadData();
	});

	//mengambil id untuk edit data
	function loadDataGet(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/trainer/edit.html',
			data : {
				"id" : id
			},
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.modal-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");

			}
		});

	}

	//proses edit data
	function updateData() {
		var dataformatjson = getFormData($('#trainerEdit'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/trainer/',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				alert('Data successfully updated!');
				$('#myModal').modal('hide');
				loadData();
			},
			error : function(data) {
				alert('gagal');
			}
		});
	}

	//mengambil id untuk delete data
	function loadDataGetDelete(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/trainer/delete.html',
			data : {
				"id" : id
			},
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.modal-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");

			}
		});

	}
	//proses delete data
	function deleteData() {
		var dataformatjson = getFormData($('#trainerDelete'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/trainer/delete',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				alert('Data successfully deleted!');
				$('#myModal').modal('hide');
				loadData();
			},
			error : function(data) {
				alert('gagal');
			}
		});
	}
	
	//fungsi search
	$("#btn-search").click(function(){
		$.ajax({
			url : contextName + '/trainer/search.html',
			data : {cari : $('#txt-search').val()},
			dataType : 'html',
			type : 'get',
			success : function(data){
				$('#trainerlist').html(data);
			}
		});
	});
</script>


