<form role="form" id="trainerEdit">
	<div class="box-body">
		<div class="form-group">
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="id" id="edit-id" value="${trainer.id}">
			</div>
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="createdBy"
					id="edit-createdBy" value="${trainer.createdBy}">
			</div>
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="createdOn"
					id="edit-createdOn" value="${trainer.createdOn}">
			</div>
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="isDelete"
					id="edit-isDelete" value="${trainer.isDelete}">
			</div>
		</div>
		<div class="form-group">
			<label>Nama</label> <input type="text" class="form-control"
				name="name" id="edit-name" value="${trainer.name}">
		</div>

		<div class="form-group">
			<label>Notes</label>
			<textarea class="form-control" name="notes" id="edit-notes">${trainer.notes}</textarea>
		</div>
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="button" class="btn btn-warning" onclick="updateData()" data-dismiss="modal">Save</button>
			<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
