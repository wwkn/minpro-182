<form class="form-hirizoltal" id="addRoom">
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="ra-code" name="code" class="form-control" placeholder="Code"/>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="ra-name" name="name" class="form-control" placeholder="Name"/>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="ra-capacity" name="capacity" class="form-control" placeholder="Capacity"/>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<div class="radio">
				<label><b>Any Projector  ?</b> &emsp; &emsp;</label>
				<label><input type="radio" id="ra-any"
						name="any_project" value="1" />Yes</label>
				<label><input type="radio" id="ra-any"
						name="any_project" value="0" />No</label>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<textarea id="ra-description" name="notes" class="form-control" placeholder="Description"></textarea>
		</div>
	</div>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="submit" data-dismiss="modal" onClick="">Submit</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>