<form id="deleteAjah" class="form-horizontal">
	<input type="hidden" id="od-id" name="id" class="form-control" value="${office.id}" /> 
	<input type="hidden" id="od-createdBy" name="createdBy" class="form-control" value="${office.createdBy}" /> 
	<input type="hidden" id="od-createdOn" name="createdOn" class="form-control" value="${office.createdOn}" /> 
	<input type="hidden" id="od-isDelete" name="isDelete" class="form-control" value="${office.isDelete}" /> 
	<input type="hidden" id="od-name" name="name" class="form-control" value="${office.name}" /> 
	<input type="hidden" id="od-phone" name="phone" class="form-control" value="${office.phone}" /> 
	<input type="hidden" id="od-email" name="email" class="form-control" value="${office.email}" /> 
	<input type="hidden" id="od-address" name="address" class="form-control" value="${office.address}" /> 
	<input type="hidden" id="od-notes" name="notes" class="form-control" value="${office.notes}" /> 
	<input type="hidden" id="od-modifiedOn" name="modifiedOn" class="form-control" value="${office.modifiedOn}" />
	<input type="hidden" id="od-modifiedBy" name="modifiedBy" class="form-control" value="${office.modifiedBy}" />
	<div class="box-body">
		<div class="form-group">
			<center>
				<label><h3>Hapus Data <b>${office.name}</b> ?</h3></label>
			</center>
		</div>
	</div>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="submit" onClick="deleteE()">Ok</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>