<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>

<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">OFFICE LIST</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control" id="txt-search" placeholder="Search by name" /> 
					<span class="input-group-btn">
						<button class="btn btn-warning" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>NAME</th>
					<th>CONTACT</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="officeList">
				<c:forEach items="${list}" var="office">
					<tr>
						<td>${office.name}</td>
						<td>${office.phone} / ${office.email}</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="loadEdit('${office.id}')">Edit</a></li>
									<li><a href="#" onclick="loadDelete('${office.id}')">Delete</a></li>
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modal-form">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="box-title">OFFICE</h3>
			</div>
			<div class="box-body">
			
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-room">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="box-title">ROOM</h3>
			</div>
			<div class="box-body">
			
			</div>
		</div>
	</div>
</div>

<script>
	
	$(function(){
		loadData();
	});

	function show(d) {
		result = '';
		var orangViewModel = {};
		var room = {};
		$(d).each(array);
		$('#officeList').html(result);
	}

	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/office.html',
			success : function(d) {
				show(d);
			}
		});
	}
	
	$('#btn-add').click(function(){
		$.ajax({
			url : '${contextName}/office/add.html',
			type : 'get',
			dataType : 'html',
			success : function(t){
				alert("masu buat apa sii?");
				$('#modal-form').modal('show');
				$('#modal-form').find('.box-body').html(t);
			},
			error : function(){
				alert("coba sekali lagi");
			}
		});
	});
	
	function simpan(){
		var dataformatnyajson = getFormData($('#addAjah'));
		/* officeViewModel.officeNya = dataformatnyajson;
		officeViewModel.room = room; */
		$.ajax({
			type : 'post',
			url : contextName + '/api/office/',
			data : JSON.stringify(dataformatnyajson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(t){
				$('#modal-form').modal('hide');
				loadData();
				/* room = [];  */
			},
			error : function(t){
				alert('Gagal');
			}
		});
	}
	
	/* ADD ROOM */
	
	function loadRoom(){
		$.ajax({
			url : contextName + '/office/radd.html',
			type : 'get',
			dataType : 'html',
			success : function(t){
				$('#modal-room').find('.box-body').html(t);
				$('#modal-room').modal('show');
			}
		});		
	}
	
	function submitRoom(){
		
	}
	
	
	/* CRUD */
	
	
	function updateE(){
		var json = getFormData($('#editAjah'))
		$.ajax({
			url : contextName + '/api/office/',
			type : 'put',
			data : JSON.stringify(json),
			dataType : 'json',
			contentType : 'application/json',
			success : function(t){
				$('#modal-form').modal('hide');
				alert("Berhasil");
				loadData();
			},
			error : function(){
				alert("Coba Lagi");
			}
		});
	}
	
	function loadEdit(id){
		$.ajax({
			url : contextName + '/office/edit.html',
			type : 'get',
			data : {'id':id},
			success : function(t){
				$('#modal-form').find('.box-body').html(t);
				$('#modal-form').modal('show');
			}
		});
	}
	
	function deleteE(){
		var json = getFormData($('#deleteAjah'))
		$.ajax({
			url : contextName + '/api/office/d',
			type : 'put',
			data : JSON.stringify(json),
			dataType : 'json',
			contentType : 'application/json',
			success : function(t){
				$('#modal-form').modal('hide');
				alert("Terhapus");
				loadData();
			},
			error : function(){
				alert("Coba Lagi");
			}
		});
	}
	
	function loadDelete(id){
		$.ajax({
			url : contextName + '/office/delete.html',
			type : 'get',
			data : {'id':id},
			success : function(t){
				$('#modal-form').find('.box-body').html(t);
				$('#modal-form').modal('show');
			}
		});
	}
</script>