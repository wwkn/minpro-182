
<form role="form" id="questionDelete">
	<div class="box-body">
		<div class="form-group">
			<div class="col-xs-3">
				<input type="hidden" class="form-control" name="id" id="dId"
					value="${question.id}">
			</div>
			<div class="col-xs-3">
				<input type="hidden" class="form-control" name="createdBy"
					id="dCreatedBy" value="${question.createdBy}">
			</div>
			<div class="col-xs-3">
				<input type="hidden" class="form-control" name="createdOn"
					id="dCreatedOn" value="${question.createdOn}">
			</div>
			<div class="col-xs-3">
				<input type="hidden" class="form-control" name="question" id="dName"
					value="${question.question}">
			</div>
			<div class="col-xs-3">
				<input type="hidden" class="form-control" name="questionType" id="dName"
					value="${question.questionType}">
			</div>
			<center>
				<h1>Are You Sure Want To Delete ?</h1>
			</center>
		</div>
	</div>
	<div class="box-footer">
		<center>
			<button type="button" class="btn btn-success" onclick="deleteData()"
				data-dismiss="modal">Yes</button>
			<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
		</center>
	</div>
</form>