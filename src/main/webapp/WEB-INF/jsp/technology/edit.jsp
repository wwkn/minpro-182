<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form role="form" id="editTechnology">
	<div class="form-group">
		<input type="hidden" class="form-control" name="id" id="edit-id" value="${technology.id}">
		<input type="hidden" class="form-control" name="createdBy" id="edit-createdBy" value="${technology.createdBy}">
		<input type="hidden" class="form-control" name="createdOn" id="edit-createdOn" value="${technology.createdOn}">
		<input type="hidden" class="form-control" name="isDelete" id="edit-isDelete" value="${technology.isDelete}">
	</div>
	
	<div class="box-body">
		<div class="form-group">
			<label>Nama</label> <input class="form-control" name="name"
				placeholder="Name" id="edit-name" value="${technology.name }" />
		</div>
		<div class="form-group">
			<label>Notes</label>
			<textarea class="form-control" name="notes" id="edit-notes"
				placeholder="Notes">${technology.name }</textarea>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<button type="button" class="btn btn-warning" id="btn-add-trainer">+TRAINER</button>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12"></div>
			<table class="table table-striped table-bordered table-hover"
				id="dataTables-example">
				<thead>
					<tr>
						<th>NAME</th>
						<th>CREATED BY</th>
					</tr>
				</thead>
				<tbody id="tbodyanak">

				</tbody>
			</table>
		</div>
	</div>

	<div class="box-footer">
		<button class="btn btn-warning" type="button" data-dismiss="modal"
			id="btn-pencet-edit">Submit</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>

		<button class="btn btn-danger" type="reset">Reset</button>
	</div>
</form>

<script>
	$("#btn-add-trainer").click(function() {
		$.ajax({
			url : contextName +'/technology/addTrainer.html',
			type : "get",
			dataType : "html",
			success : function(data) {
				$("#yourModal").modal('show');
				$("#yourModal").find('.box-anak').html(data);
				$("#yourModal").find('.box-title').html('Add Trainer');
				
			}
		});
	});
</script>