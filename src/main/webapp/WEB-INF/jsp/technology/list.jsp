
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-tittle">Technology List</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" id="txt-search" class="form-control" /> <span
						class="input-group-btn">
						<button class="btn btn-warning" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>NAME</th>
					<th>CREATED BY</th>

					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${list}" var="technology">
					<tr>
						<td>${technology.name}</td>
						<td>${technology.user.username}</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="loadDataGet(${technology.id})">Edit</a>
									</li>
									<li><a href="#" onclick="loadDataGetDelete(${technology.id})">Delete</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</div>

<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>

			<div class="box-body"></div>


		</div>
	</div>
</div>

<div class="modal fade" id="yourModal">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-anak"></div>
		</div>
	</div>
</div>


<script>
	var result = '';
	var technologyViewModel = {};
	var ttr = [];

	//show list
	function show(data) {
		result = '';
		$(data).each(list);
		$('#technologyList').html(result);
	}

	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/technology/isi.html/',
			success : function(data) {
				$('#technologyList').html(data);
			}
		});
	}

	$(document).ready(function() {
		loadData();
	});

	//memanggil modal add form
	$("#btn-add").click(function() {
		$.ajax({
			url : "${contextName}/technology/add.html",
			type : "get",
			dataType : "html",
			success : function(data) {
				$("#myModal").modal('show');
				$("#myModal").find('.box-body').html(data);
				$("#myModal").find('.box-title').html('Add Technology');
			}
		});
	});

	//input data 
	$('#myModal').on('click', '#btn-pencet-add', function() {
		var name = $('#myModal').find('#add-name').val();
		var notes = $('#myModal').find('#add-notes').val();
		var technology = {
				name : name,
				notes : notes,
		}
		technologyViewModel.technology = technology;
		technologyViewModel.ttr = ttr;

		$.ajax({
			type : 'post',
			url : contextName + '/api/technology/',
			data : JSON.stringify(technologyViewModel),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();
				alert('Data successfully saved!');
				$('#myModal').find('#tbodyanak').html('');
				$('#myModal').find('#add-name,#add-notes').val();				
				ttr = [];
				$('#myModal').modal('hide');
			},
			error : function(d) {
				alert('gagal');
			}
		});

	});

	function prosesanak(i, e) {
		result += '<tr>';
		result += '<td>' + e.id + '</td>';
		result += '<td>' + e.trainerId + '</td>';
		result += '</tr>';
	}

	function detailAnak(d) {
		result = '';
		$(d).each(prosesanak);
		$('#tbodyviewanak').html(result);
	}

	function loadAnak(id) {
		$.ajax({
			type : 'get',
			url : namaKonteks + '/api/technologyTrainer/' + id,
			success : function(d) {
				detailAnak(d);
			}
		});
	}

	//input trainer ke technology
	$('#yourModal').on('click', '#btn-trainer', function(){
		var dataformatnyajson = {};
		var trainId = $('#yourModal').find('#trainer-id').val();
		var trainText = $('#yourModal').find('#trainer-id option:selected').text();
		var item = {
		//sebelah kiri dari model, kanan dari field
				trainerId : trainId,
				trainerText : trainText,
				
		}
		ttr.push(item);
		
		$('#yourModal').modal('hide');
		$('#trainer-id').val('');
		showAnak();
	});

	/* function addAnak() {
		// tampilkan popup form tambah anak
		// di form tambah anak, ketika tombol submit diklik akan memanggil submitAddAnak()
		$('#yourModal').modal('show');
	} */

	//menampilkan data trainer di form technology
	function showAnak() {
		var resultRowAnak = '';
		$(ttr).each(function(i, e) {
			resultRowAnak += '<tr>';
			resultRowAnak += '<td>' + e.trainerText + '</td>';
			resultRowAnak += '<td>' + e.createdBy + '</td>';
			resultRowAnak += '</tr>';
		});
		$('#tbodyanak').html(resultRowAnak);
	}
	
	//fungsi search
	$("#btn-search").click(function(){
		$.ajax({
			url : contextName + '/technology/search.html',
			data : {cari : $('#txt-search').val()},
			dataType : 'html',
			type : 'get',
			success : function(data){
				$('#technologyList').html(data);
			}
		});
	});
	
	
	//edit
	function loadDataGet(id){
		$.ajax({
			type : 'get',
			url : contextName + '/technology/edit.html',
			data : {
				"id" : id
			},
			success : function(data){
				$('#myModal').find('.box-body').html(data);
				$('#myModal').find('.box-title').html("EDIT TECHNOLOGY");
				$('#myModal').modal('show');
			}
		});
	}
	
	//proses edit
	$('#myModal').on('click', '#btn-pencet-edit', function(){
		var id = $('#myModal').find('#edit-id').val();
		var name = $('#myModal').find('#edit-name').val();
		var notes = $('#myModal').find('#edit-notes').val();
		var cBy = $('#myModal').find('#edit-createdBy').val();
		var cOn = $('#myModal').find('#edit-createdOn').val();
		var isD = $('#myModal').find('#edit-isDelete').val();
		
		var techItem = {
				id : id,
				name : name,
				notes : notes,
				createdBy : cBy,
				createdOn : cOn,
				isDelete : isD,
				
		}
		
		technologyViewModel.technology = techItem;
		technologyViewModel.ttr = ttr;
		
		$.ajax({
			type : 'PUT',
			url : contextName + '/api/technology/',
			data : JSON.stringify(technologyViewModel),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();
				alert('Data successfully updated!');
				$('#myModal').find('#tbodyanak').html('');				
				ttr = [];
				$('#myModal').modal('hide');
			},
			error : function(d) {
				alert('gagal');
			}
		});
		
	});
	
	function loadDataGetDelete(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/technology/delete.html',
			data : {
				"id" : id
			},
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.box-body').html(data);
				//tampilkan modal
				$('#myModal').modal('show');

			}
		});

	}
	//proses delete data
	function deleteData() {
		var dataformatjson = getFormData($('#technologyDelete'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/technology/delete/',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				alert('Data successfully deleted!');
				$('#myModal').modal('hide');
				loadData();
			},
			error : function(data) {
				alert('gagal');
			}
		});
	} 
	
	//delete
	/* function deleteData(id) {
		$.ajax({
			type : 'delete',
			url : contextName + '/api/technology/' + id,
			success : function(apapun) {
				alert("Delete");
				loadData();
			}
		});
	} */
	
	
</script>