<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach items="${list}" var="technology">
	<tr>
		<td>${technology.name}</td>
		<td>${technology.user.username}</td>
		<td>
			<div class="btn-group">
				<button type="button" class="btn btn-default dropdown-toggle"
					data-toggle="dropdown">
					<i class="glyphicon glyphicon-menu-hamburger"></i>
				</button>
				<ul class="dropdown-menu">
					<li><a href="#" onclick="loadDataGet(${technology.id})">Edit</a>
					</li>
					<li><a href="#" onclick="deleteData(${technology.id})">Delete</a>
					</li>
				</ul>
			</div>
		</td>
	</tr>
</c:forEach>
