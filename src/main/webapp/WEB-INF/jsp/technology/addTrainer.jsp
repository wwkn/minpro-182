<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box-body">
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="trainerId" id="trainer-id">
				<option value="" disabled selected>Trainer</option>
				<c:forEach items="${listTtr}" var="Ttr">
					<option value="${Ttr.id}">${Ttr.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>

	<div class="box-footer">
		<button type="button" class="btn btn-warning" id="btn-trainer"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</div>
<script>
	$('.select2').select2()
</script>
