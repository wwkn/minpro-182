<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form role="form" id="addTechnology">
	<div class="box-body">
		<div class="form-group">
			<label>Nama</label> <input class="form-control" name="name"
				placeholder="Name" id="add-name" />
		</div>
		<div class="form-group">
			<label>Notes</label>
			<textarea class="form-control" name="notes" id="add-notes"
				placeholder="Notes"></textarea>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<button type="button" class="btn btn-warning" id="btn-add-trainer">+TRAINER</button>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12"></div>
			<table class="table table-striped table-bordered table-hover"
				id="dataTables-example">
				<thead>
					<tr>
						<th>NAME</th>
						<th>CREATED BY</th>
					</tr>
				</thead>
				<tbody id="tbodyanak">

				</tbody>
			</table>
		</div>
	</div>

	<div class="box-footer">
		<button class="btn btn-warning" type="button" data-dismiss="modal"
			id="btn-pencet-add">Submit</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>

<script>
	$("#btn-add-trainer").click(function() {
		$.ajax({
			url : contextName +'/technology/addTrainer.html',
			type : "get",
			dataType : "html",
			success : function(data) {
				$("#yourModal").modal('show');
				$("#yourModal").find('.box-anak').html(data);
				$("#yourModal").find('.box-title').html('Add Trainer');
				
			}
		});
	});
</script>