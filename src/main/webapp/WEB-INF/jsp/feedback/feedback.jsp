
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">FEEDBACK</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control"
						placeholder='Search By name / version' /> <span
						class="input-group-btn">
						<button class="btn btn-warning">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive text-left">
			<thead>
				<tr>
					<th>NAME</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="feedbackList">
				<c:forEach items="${testList}" var="test">
					<tr>
						<td>Feedback ${test.name}</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="loadDataGet(${test.id})">Choose Document</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
<!-- modal begin -->
<div id="feedbackModal" class="modal fade">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-body">
				<!-- modal content -->
			</div>
		</div>
	</div>
</div>
<script>
	
</script>