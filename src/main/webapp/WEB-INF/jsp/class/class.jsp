<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">CLASS</h3>
	</div>

	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control" name="search-box"
						id="txt-search" placeholder="Search by Batch" /> <span
						class="input-group-btn">
						<button class="btn btn-warning btn-flat" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
		</div>
		<div class="row"></div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>BATCH</th>
					<th>NAME</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="clazz-list">
				<c:forEach items="${classList}" var="clazz">
					<tr>
						<td>${clazz.batch.name}</td>
						<td>${clazz.biodata.name}</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="confirmDelete(${clazz.id})">Delete</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modal-form">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>

<script>
	$("#btn-search").click(function(){
		$.ajax({
			url: contextName + "/class/search.html",
			type: 'get',
			data: {search: $('#txt-search').val()},
			dataType: 'html',
			success: function(data) {
				$('#clazz-list').html(data);
			}
		});
	});
	
	function loadData(){
		$.ajax({
			type : 'get',
			url : contextName + '/class/list.html',
			success : function(data){
				$('#clazz-list').html(data);
			}
		});
	}
	
	function confirmDelete(id) {
		$.ajax({
			url: contextName + '/class/delete.html',
			type: 'get',
			data: {"id": id},
			success: function(data) {
				$('#modal-form').find('.box-body').html(data);
				$('#modal-form').modal('show');
			}
		});
	}
	
	function deleteData(id) {
		$.ajax({
			type : 'delete',
			url : contextName + '/api/class/delete/' + id,
			success : function(d) {
				alert('Data successfully deleted !');
				loadData();
			},
			error : function(d) {
				alert('Failed to delete data !');
			}
		});
	}
</script>