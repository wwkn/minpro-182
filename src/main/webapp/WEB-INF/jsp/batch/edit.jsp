<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="batch-edit" class="form-horizontal pull-center">
	<input type="hidden" class="form-control" name="id" 
		value="${batch.id}" />
	<input type="hidden" class="form-control" name="createdBy"
		value="${batch.createdBy}" /> 
	<input type="hidden" class="form-control" name="createdOn" 
		value="${batch.createdOn}" /> 
	<input type="hidden" class="form-control" name="isDelete"
		value="${batch.isDelete}" />
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="technologyId">
				<option value="${batch.technology.id}" selected>${batch.technology.name}</option>
				<c:forEach items="${techList}" var="tech">
					<option value="${tech.id}">${tech.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="trainerId">
				<option value="${batch.trainer.id}" selected>${batch.trainer.name}</option>
				<c:forEach items="${trainerList}" var="trainer">
					<option value="${trainer.id}">${trainer.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" class="form-control" name="name"
				value="${batch.name}" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<div class="input-group date">
				<input type="text" class="form-control pull-left"
					name="periodFrom" value="${batch.periodFrom}" onfocus="(this.type='date')" />
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<div class="input-group date">
				<input type="text" class="form-control pull-left"
					name="periodTo" value="${batch.periodTo}" onfocus="(this.type='date')" />
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="roomId">
				<option value="${batch.room.id}" selected>${batch.room.name}</option>
				<c:forEach items="${roomList}" var="room">
					<option value="${room.id}">${room.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="bootcampTypeId">
				<option value="${batch.bootcampType.id}" selected>${batch.bootcampType.name}</option>
				<c:forEach items="${bootList}" var="boot">
					<option value="${boot.id}">${boot.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<textarea class="form-control" rows="3" name="notes">${batch.notes}</textarea>
		</div>
	</div>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="button" onClick="updateData()"
			data-dismiss="modal">Save</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>

<script>
	$('.select2').select2()
</script>

<style>
.opt-title {
	color: grey;
}
</style>