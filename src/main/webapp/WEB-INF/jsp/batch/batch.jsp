<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">BATCH</h3>
	</div>

	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control" name="search-box"
						id="txt-search" placeholder="Search by Technology/Name" /> <span
						class="input-group-btn">
						<button class="btn btn-warning btn-flat" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>

			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<div class="row"></div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>TECHNOLOGY</th>
					<th>NAME</th>
					<th>TRAINER</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="batch-list">
				<c:forEach items="${batchList}" var="batch">
					<tr>
						<td>${batch.technology.name}</td>
						<td>${batch.name}</td>
						<td>${batch.trainer.name}</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="loadDataGet(${batch.id})">Edit</a>
									</li>
									<li><a href="#" onclick="addParticipant(${batch.id})">Add
											Participant</a></li>
									<li><a href="#" onclick="setupTest(${batch.id})">Setup Test</a></li>
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modal-form">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>

<script>
	$("#btn-add").click(function() {
		$.ajax({
			url : contextName + "/batch/add.html",
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#modal-form").find(".box-body").html(data);
				$("#modal-form").find(".box-title").html('BATCH');
				$("#modal-form").modal('show');
			}
		});
	});
	
	function loadData(){
		$.ajax({
			type : 'get',
			url : contextName + '/batch/list.html',
			success : function(data){
				$('#batch-list').html(data);
			}
		});
	}

	function saveData() {
		var json = getFormData($('#batch-add'));

		$.ajax({
			url : contextName + '/api/batch/post/',
			type : 'post',
			data : JSON.stringify(json),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				alert('Data successfully saved !');
				$('#modal-form').modal('hide');
				loadData();
			},
			error : function(d) {
				alert('Failed to save data !');
			}
		});
	}
	
	function loadDataGet(id) {
		$.ajax({
			url: contextName + '/batch/edit.html',
			type: 'get',
			data: {"id": id},
			success: function(data) {
				$('#modal-form').find('.box-body').html(data);
				$("#modal-form").find(".box-title").html('BATCH');
				$('#modal-form').modal('show');
			}
		});
	}
	
	function updateData() {
		var json = getFormData($('#batch-edit'));
		$.ajax({
			url: contextName + '/api/batch/update/',
			type: 'put',
			data: JSON.stringify(json),
			dataType: 'json',
			contentType: 'application/json',
			success: function(d) {
				alert('Data successfully updated !');
				$('#modal-form').modal('hide');
				loadData();
			},
			error: function(d) {
				alert('Failed to update data !');
			}
			
		});
	}
	
	function addParticipant(id) {
		$.ajax({
			url: contextName + '/batch/add-participant.html',
			type: 'get',
			data: {"id": id},
			success: function(data) {
				$('#modal-form').find('.box-body').html(data);
				$("#modal-form").find(".box-title").html('ADD PARTICIPANT');
				$('#modal-form').modal('show');
			}
		});
	}
	
	function saveParticipant() {
		var json = getFormData($('#participant-add'));

		$.ajax({
			url : contextName + '/api/class/post/',
			type : 'post',
			data : JSON.stringify(json),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				alert('Data successfully saved !');
				$('#modal-form').modal('hide');
				loadData();
			},
			error : function(d) {
				alert('Failed to save data !');
			}
		});
	}
	
	$("#btn-search").click(function(){
		$.ajax({
			url: contextName + "/batch/search.html",
			type: 'get',
			data: {search: $('#txt-search').val()},
			dataType: 'html',
			success: function(data) {
				$('#batch-list').html(data);
			}
		});
	})
	
	function setupTest(id) {
		$.ajax({
			url: contextName + "/batch/setup-test.html",
			type: 'get',
			data: {"id": id},
			dataType: 'html',
			success: function(data) {
				$("#modal-form").find(".box-body").html(data);
				$("#modal-form").find(".box-title").html('SETUP TEST');
				$("#modal-form").modal('show');
			}
		});
	}
	
	$('#modal-form').on('click', '.btn-choose', function(){
		var $tRow = $(this).parent().parent();
		
		var testName = $tRow.find('.test-name').val();
		var testId = $tRow.find('.test-id').val();
		var batchId = $tRow.find('.batch-id').val();
		
		item = {
			batchId : batchId,
			testId : testId,
		};
		
		if ($(this).hasClass("btn-warning")) {
			$.ajax({
				url: contextName + "/api/batch-test/post/",
				type: 'post',
				data: JSON.stringify(item),
				dataType: 'json',
				contentType: 'application/json',
				success: function(d) {
				}
			});
			
			$(this).removeClass("btn-warning").addClass("btn-success"),
			$(this).text("CANCEL")
		} else if ($(this).hasClass("btn-success")) {
			
			$.ajax({
				type : 'delete',
				url : contextName + '/api/batch-test/delete/' + testId,
				success : function(d) {
					alert('Data successfully deleted !');
				},
				error : function(d) {
					alert('Failed to delete data !');
				}
			});
			
			$(this).removeClass("btn-success").addClass("btn-warning"),
			$(this).text("CHOOSE")
		}
	});
</script>