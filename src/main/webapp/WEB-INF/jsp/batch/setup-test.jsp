<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="col-md-12">
	<h5>
		<b>TEST LIST</b>
	</h5>
	<hr>
</div>
<table class="table">
	<c:forEach items="${testList}" var="test">
		<tr>
			<td>
				<input type="hidden" class="form-control batch-id" name="batchId"
					value="${batch.id}" /> 
				<input type="hidden" class="form-control test-id" name="testId" value="${test.id}" />
				<input type="text" class="form-control test-name" value="${test.name}"
						readonly />
			</td>
			<td>
				<button type="button" class="btn btn-warning btn-flat btn-choose">CHOOSE</button>
			</td>
		</tr>
	</c:forEach>
</table>

<div class="box-footer pull-right">
	<input type="button" class="btn btn-warning" data-dismiss="modal"
		value="Close" />
</div>
