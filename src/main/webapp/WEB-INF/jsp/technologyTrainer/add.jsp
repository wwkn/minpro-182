
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="batch-add" class="form-horizontal pull-center">

	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="trainerId">
				<option value="" disabled selected>Trainer</option>
				<c:forEach items="${trainerList}" var="trainer">
					<option value="${trainer.id}">${trainer.name}</option>
				</c:forEach>
			</select>

		</div>
	</div>
	<div class="form-group">
		<div class="col-md-3">
			<button class="btn btn-warning" id="btn-add-trainer">+TRAINER</button>
		</div>
	</div>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="button" onClick="saveData()"
			data-dismiss="modal">Save</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>

<div class="modal fade" id="modal-addTrainer">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-body-trainer"></div>
		</div>
	</div>
</div>


<script>
	$("#btn-add-trainer").click(function() {
		$.ajax({
			url : contextName + "/technologyTrainer/addTrainer.html",
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$("#modal-addTrainer").find(".box-body-trainer").html(data);
				$("#modal-addTrainer").find(".box-title").html('ADD TRAINER');
				$("#modal-addTrainer").modal('show');
			}
		});
	});

	$('.select2').select2()
</script>
