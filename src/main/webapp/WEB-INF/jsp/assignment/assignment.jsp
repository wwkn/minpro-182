
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">ASSIGNMENT</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control" id="txt-search"
						placeholder="Search by name / majors" /> <span
						class="input-group-btn">
						<button class="btn btn-warning" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="tkan" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>NAME</th>
					<th>START DATE</th>
					<th>END DATE</th>
				</tr>
			</thead>
			<tbody id="assign-list">
				<c:forEach items="${assignList}" var="assig">
					<tr>
						<td>${assig.biodata.name}</td>
						<td>${assig.startDate}</td>
						<td>${assig.endDate}</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
								<!--assig adalah nama var  -->
									<li><a href="#" onclick="loadDataGet(${assig.id})">Edit</a>
									</li>
									<li><a href="#" onclick="delDataGet(${assig.id})">Delete</a>
									</li>
									<li><a href="#" onclick="holdDataGet(${assig.id})">Hold</a>
									</li>
									<li><a href="#" onclick="markDataGet(${assig.id})">Mark as Done</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modalku">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title">ASSIGNMENT</h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>

<script>
	$("#tkan").click(function() {
		$.ajax({
			url : contextName + "/assignment/add.html",
			type : "get",
			dataType : "html",
			success : function(data) {
				$("#modalku").find(".box-body").html(data);
				$("#modalku").find(".box-title").html('ASSIGNMENT');
				$("#modalku").modal("show");
			}
		});
	});
	
	function saveData() {
	var dtJson = getFormData($('#assAdd'));
	
	$.ajax({
		url : contextName + '/api/assignment/post/',
		type : 'post',
		data : JSON.stringify(dtJson),
		dataType : 'json',
		contentType : 'application/json',
		success : function(d) {
			$('#modalku').modal('hide');
			loadData();
			}
		});
	}
	
	function loadDataGet(id){
		$.ajax({
			url : contextName + "/assignment/edit.html",
			type : "get",
			data : {"id":id},
			success : function(data){
				$('#modalku').find('.box-body').html(data);
				$("#modalku").find('.box-title').html('ASSIGNMENT');
				$('#modalku').modal('show');
			}
		});
	}
	
	function updateData() {
		var json = getFormData($('#assignEdit'));
		$.ajax({
			url: contextName + '/api/assignment/e/',
			type: 'put',
			data: JSON.stringify(json),
			dataType: 'json',
			contentType: 'application/json',
			success: function(d) {
				alert("Data successfully updated!");
				$('#modalku').modal('hide');
				loadData();
			}
			
		});
	}
	
	function markDataGet(id){
		$.ajax({
			url : contextName + '/assignment/markDone.html',
			type : 'get',
			data : {"id":id},
			success : function(data){
				$('#modalku').find('.box-body').html(data);
				$('#modalku').modal('show');
			}
		});
	}
	
	function markData() {
		var json = getFormData($('#marker'));
		$.ajax({
			url: contextName + '/api/assignment/m/',
			type: 'put',
			data: JSON.stringify(json),
			dataType: 'json',
			contentType: 'application/json',
			success: function(d) {
				alert("Data successfully marked!");
				$('#modalku').modal('hide');
				loadData();
			}
			
		});
	}
	
	function holdDataGet(id){
		$.ajax({
			url: contextName + '/assignment/hold.html',
			type: 'get',
			data: {"id":id},
			success: function(o) {
				$('#modalku').find('.box-body').html(o);
				$('#modalku').modal('show');
			}
		});
	}
	
	function holdD() {
		var jos = getFormData($('#holder'));
		$.ajax({
			url:contextName + '/api/assignment/h/',
			type:'put',
			data: JSON.stringify(jos),
			dataType: 'json',
			contentType: 'application/json',
			success: function(x) {
				alert("Data sudah di hold!");
				$('#modalku').modal('hide');
				loadData();
			}
		});
	}
	
	function delDataGet(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/assignment/delete.html',
			data : {
				"id" : id
			},
			success : function(data) {
				//mengisi data html
				$('#modalku').find('.box-body').html(data);
				//tampilkan modal
				$("#modalku").modal("show");

			}
		});
		alert(id);
	}
	
	function deleteData() {
		var dataformatjson = getFormData($('#assignDelete'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/assignment/d/',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				alert('terhapus');
				loadData();
			},
			error : function(d) {
				alert('gagal');
			}
		});
	}
	
	function loadData() {
		$.ajax({
			url : contextName + '/assignment/list.html',
			type : 'get',
			dataType : 'html',
			success :  function(d) {
				$('#assign-list').html(d);
			}
		})
	}
	
	$("#btn-search").click(function(){
		$.ajax({
			url : contextName + '/assignment/search.html',
			data : {cari : $('#txt-search').val()},
			dataType : 'html',
			type : 'get',
			success : function(data){
				$('#assign-list').html(data);
			}
		});
	});
</script>