<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="marker" class="form" role="form">
	<div class="form-group">
		<div class="col-xs-4">
			<input type="hidden" class="form-control" name="id" id="edit-id" value="${assignment.id}">
			<input type="hidden" class="form-control" name="title" id="edit-title" value="${assignment.title}">
			<input type="hidden" class="form-control" name="testId" id="edit-testId" value="${assignment.testId}">
			<input type="hidden" class="form-control" name="createdBy" id="edit-createdBy"	value="${assignment.createdBy}"> 
			<input type="hidden" class="form-control" name="createdOn" id="edit-createdOn" value="${assignment.createdOn}"> 
			<input type="hidden" class="form-control" name="modifiedBy" id="edit-modifiedBy"	value="${assignment.modifiedBy}"> 
			<input type="hidden" class="form-control" name="modifiedOn" id="edit-modifiedOn" value="${assignment.modifiedOn}">
			<input type="hidden" class="form-control" name="isHold" id="edit-isHold" value="${assignment.isHold}"> 
			<input type="hidden" class="form-control" name="isDelete" id="edit-isDelete" value="${assignment.isDelete}">
			<input type="hidden" class="form-control" name="startDate" id="edit-startDate" value="${assignment.startDate}">
			<input type="hidden" class="form-control" name="endDate" id="edit-endDate" value="${assignment.endDate}">
			
		</div>
	</div>

	<div class="form-group">
		<div class="input-group date">
			<input type="date" class="form-control"	name="realizationDate" value="${assignment.realizationDate}"
				id="mDone-realizationDate" placeholder="Realization Date" />
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
		</div>
	</div>
	<div class="form-group">
		<textarea class="form-control" name="notes" id="mDone-notes" placeholder="Notes">${assignment.notes}</textarea>
	</div>
	<div class="box-footer pull-right">
		<button type="button" class="btn btn-warning" onclick="markData()"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>