	<form role="form" id="holder">
	<div class="box-body">
		<div class="form-group">
			<input type="hidden" class="form-control" name="id" id="delete-id" value="${assignment.id}"> 
			<input type="hidden" class="form-control" name="testId" id="delete-testId" value="${assignment.testId}">
			<input type="hidden" class="form-control" name="title" id="delete-title" value="${assignment.title}">
			<input type="hidden" class="form-control" name="startDate" id="delete-startDate" value="${assignment.startDate}">
			<input type="hidden" class="form-control" name="endDate" id="delete-endDate" value="${assignment.endDate}">
			<input type="hidden" class="form-control" name="createdBy" id="delete-createdBy" value="${assignment.createdBy}">
			<input type="hidden" class="form-control" name="createdOn" id="delete-createdOn" value="${assignment.createdOn}"> 
			<input type="hidden" class="form-control" name="isDelete" id="delete-isDelete" value="${assignment.isDelete}"> 

		</div>
		<div class ="form-group">
		<label>Yakin mau dihold?</label>
		</div>
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="button" class="btn btn-success" onclick="holdD()"
			data-dismiss="modal">Oke</button>
		<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
	</div>
</form>
