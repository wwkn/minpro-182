<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="assAdd">
	<div>
		<div class="form-group">
			<select class="form-control" style="width: 100%;" name="testId">
				<option value="" disabled selected>Name</option>
				<c:forEach items="${bioList}" var="bio">
					<option value="${bio.id}">${bio.name}</option>				
				</c:forEach>
			</select>
	</div>
	<div class="form-group">
		<input type="text" name="title" id="title" class="form-control" placeholder="Title"/>
	</div>
	<div class="form-group">
			<div class="input-group date">
				<input type="date" class="form-control pull-left"
					name="startDate" placeholder="Start Date" />
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
			</div>
	</div>
	<div class="form-group">
			<div class="input-group date">
				<input type="date" class="form-control pull-left"
					name="endDate" placeholder="End Date" />
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
			</div>
	</div>
	<div class="form-group">
		<textarea class="form-control" name="description" id="description"
			placeholder="Description"></textarea>
	</div>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="button" onClick="saveData()"
			data-dismiss="modal">Save</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
<script>
	$('.datepicker').datepicker({
		autoclose : true
	})

	$('.select2').select2()
</script>

<style>
.opt-title {
	color: grey;
}
</style>