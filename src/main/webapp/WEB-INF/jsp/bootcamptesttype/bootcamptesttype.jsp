<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>

<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">BOOTCAMP TEST TYPE LIST</h3>
	</div>
	<div class="box-body">
		<div class="col-xs-11">
			<form role="form">
				<div class="col-xs-3">
					<div class="form-group">
						<input type="text" class="form-control" id="searchBar"
							placeholder="Search by name">
					</div>
				</div>
				<button type="button" class="btn btn-warning">
					<i class="glyphicon glyphicon-search"></i>
				</button>
			</form>
		</div>
		<div class="col-xs-1">
			<button type="button" id="btn-add" class="btn btn-sm btn-warning">
				<i class="glyphicon glyphicon-plus"></i>
			</button>
		</div>
		<div></div>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th><center>NAME</center></th>
					<th><center>CREATED BY</center></th>
				</tr>
			</thead>
			<tbody id="bootcamptesttypelist" align="center">
			</tbody>
		</table>
	</div>
</div>

<div id="modalQue" class="modal fade">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="box-title">Data Bootcamp Test Type</h4>
			</div>
			<div class="box-body">			
			</div>
		</div>
	</div>
</div>

<script>
	$("#btn-add").click(function() {
		alert("Menambahkan...");

		$.ajax({
			url : '${contextName}/bootcamptesttype/add.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$('#modalQue').find('.box-body').html(data);
				$('#modalQue').modal("show");
			}
		});
	});

	function simpanData() {
		var dataformatnyajson = getFormData($('#formAdd'));

		$.ajax({
			type : 'post',
			url : contextName + '/api/btt/',
			data : JSON.stringify(dataformatnyajson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();

				$('#modalQue').modal('hide');
			},
			error : function(d) {
				alert('gagal');
			}
		});

	}

	var result = '';

	function array(i, e) {
		result += '<tr>';
		result += '<td>' + e.name + '</td>';
		result += '<td>' + e.user.username + '</td>';
		result += '<td><div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-menu-hamburger"></i></button><ul class="dropdown-menu"><li><a href="#" onclick="loadDataGet('
				+ e.id
				+ ')">Edit</a></li><li><a href="#" onclick="delDataGet('+ e.id +')">Delete</a></li></ul></div></td>'
		result += '</tr>';
	}

	function show(d) {
		result = '';
		$(d).each(array);
		$('#bootcamptesttypelist').html(result);
	}

	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/api/btt/',
			success : function(d) {
				show(d);
			}
		});
	}

	$(function() {
		$('#example1').DataTable()
		$('#example2').DataTable({
			'paging' : true,
			'lengthChange' : false,
			'searching' : false,
			'ordering' : true,
			'info' : true,
			'autoWidth' : false
		})
	})

	function loadDataGet(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/bootcamptesttype/edit.html',
			data : {"id":id},
			success : function(data) {
				//mengisi data html
				$('#modalQue').find('.box-body').html(data);
				//tampilkan modal
				$("#modalQue").modal("show");

			}
		});
		alert(id);
	}

	function updateData() {
		var dataformatjson = getFormData($('#bttEdit'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/btt/',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				alert('Data Diubah');
				loadData();
			},
			error : function(d){
				alert('gagal');
			}
		});
	}
	
	function delDataGet(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/bootcamptesttype/delete.html',
			data : {
				"id" : id
			},
			success : function(data) {
				//mengisi data html
				$('#modalQue').find('.box-body').html(data);
				//tampilkan modal
				$("#modalQue").modal("show");

			}
		});
		alert(id);
	}
	
	function deleteData() {
		var dataformatjson = getFormData($('#bttDelete'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/btt/d',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				alert('terhapus');
				loadData();
			},
			error : function(d){
				alert('gagal');
			}
		});
	}

	$(document).ready(function() {
		loadData();
	});
</script>