<form id="bttEdit">
	<div class="box-body">
		<div class="form-group">
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="id" id="edit-id" value="${btt.id}">
			</div>
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="createdBy"
					id="edit-createdBy" value="${btt.createdBy}">
			</div>
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="createdOn"
					id="edit-createdOn" value="${btt.createdOn}">
			</div>
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="isDelete"
					id="edit-isDelete" value="${btt.isDelete}">
			</div>
		</div>
		<div class="form-group">
			<label>Nama</label> <input type="text" class="form-control"
				name="name" id="edit-name" value="${btt.name}">
		</div>

		<div class="form-group">
			<label>Notes</label>
			<textarea class="form-control" name="notes" id="edit-notes">${btt.notes}</textarea>
		</div>
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="button" class="btn btn-warning" onclick="updateData()"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
