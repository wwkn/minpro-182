<form role="form" id="bttDelete">
	<div class="box-body">
		<div class="form-group">
			<input type="hidden" class="form-control" name="id" id="delete-id" value="${btt.id}"> 
			<input type="hidden" class="form-control" name="createdBy" id="delete-createdBy" value="${btt.createdBy}">
			<input type="hidden" class="form-control" name="createdOn" id="delete-createdOn" value="${btt.createdOn}"> 
			<input type="hidden" class="form-control" name="isDelete" id="delete-isDelete" value="${btt.isDelete}"> 
			<input type="hidden" class="form-control" name="name" id="delete-name" value="${btt.name}">
			<input type="hidden" class="form-control" name="notes" id="delete-notes" value="${btt.notes}">
		</div>
		<div class ="form-group">
		<label><h2>Apakah Anda Yakin Menghapus Data <b>${btt.name}</b>?</h2></label>
		</div>
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="button" class="btn btn-success" onclick="deleteData()"
			data-dismiss="modal">Oke</button>
		<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
	</div>
</form>
