<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach items="${searchList}" var="test">
	<tr>
		<td>${test.name}</td>
		<td>${test.user.username}</td>
		<td>
			<div class="btn-group">
				<button type="button" class="btn btn-default dropdown-toggle"
					data-toggle="dropdown">
					<i class="glyphicon glyphicon-menu-hamburger"></i>
				</button>
				<ul class="dropdown-menu">
					<li><a href="#" onclick="loadDataGet(${test.id})">Edit</a></li>
					<li><a href="#" onclick="loadDataDelete(${test.id})">Delete</a>
					</li>
				</ul>
			</div>
		</td>
	</tr>
</c:forEach>