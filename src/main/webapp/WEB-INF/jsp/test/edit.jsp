<form id="test-edit" class="form-horizontal pull-center">
	<input type="hidden" class="form-control" name="id" id="eId"
		value="${test.id}"> <input type="hidden" class="form-control"
		name="createdBy" id="eCreatedBy" value="${test.createdBy}"> <input
		type="hidden" class="form-control" name="createdOn" id="eCreatedOn"
		value="${test.createdOn}"> <input type="hidden"
		class="form-control" name="isDelete" id="eIsDelete"
		value="${test.isDelete}">

	<div class="form-group">
		<div class="col-md-12">
			<input type="text" class="form-control" name="name" id="eName"
				value="${test.name}">
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<label>Is Bootcamp Test &emsp;&emsp;</label>
			<div class="radio">
				<label> <input type="radio" name="isBootcampTest" id="eYes"
					value="1"> Yes &emsp;
				</label> <label> <input type="radio" name="isBootcampTest" id="eNo"
					value="0"> No &emsp;
				</label>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<textarea name="notes" id="eNotes" class="form-control" rows="3">${test.notes }</textarea>
		</div>
	</div>

	<div class="box-footer pull-right">
		<button type="button" class="btn btn-warning" onclick="updateData()"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>