
<form id="formrole">

	<div class="form-group">
		<label>Code</label> <input class="form-control" name="code"
			value="${newCode}" placeholder="${newCode}" id="code">
	</div>

	<div class="form-group">
		<label>Name</label> <input class="form-control" name="name" id="name"
			placeholder="Name">
	</div>

	<div class="form-group">
		<label>Description</label>
		<textarea class="form-control" name="description" id="description"
			placeholder="Description"></textarea>
	</div>
	<div class="modal-footer">
		<button class="btn btn-warning" type="button" onclick="simpanData()"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
