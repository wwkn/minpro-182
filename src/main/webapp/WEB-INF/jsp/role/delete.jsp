
<form role="form" id="roleDelete">
	<div class="box-body">
		<div class="form-group">
			<input type="hidden" class="form-control" name="id" id="del-id"
				value="${role.id}"> <input type="hidden"
				class="form-control" name="createdBy" id="del-createdBy"
				value="${role.createdBy}"> <input type="hidden"
				class="form-control" name="createdOn" id="del-createdOn"
				value="${role.createdOn}"> <input type="hidden"
				class="form-control" name="modifiedBy" id="del-modifiedBy"
				value="${role.modifiedBy}" /> <input type="hidden"
				class="form-control" name="modifiedOn" id="del-modifiedOn"
				value="${role.modifiedOn}" /><input type="hidden"
				class="form-control" name="isDelete" id="del-isDelete"
				value="${role.isDelete}"> <input type="hidden"
				class="form-control" name="name" id="del-name" value="${role.name}">
			<input type="hidden" class="form-control" name="code" id="del-code"
				value="${role.code}"> <input type="hidden"
				class="form-control" name="description" id="del-description"
				value="${role.description}">
		</div>
		<div class="form-group">
			<label><h1>Apakah Anda Yakin Menghapus Data</h1></label>
		</div>
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="button" class="btn btn-warning" onclick="deleteData()"
			data-dismiss="modal">Yes</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
	</div>
</form>
