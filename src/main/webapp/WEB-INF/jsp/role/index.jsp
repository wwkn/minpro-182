<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">ROLE</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="col-xs-11">
			<form role="form">
				<div class="col-md-3">
					<div class="input-group">
						<input type="text" class="form-control" id="txt-search"
							placeholder="Search by name"> <span
							class="input-group-btn">
							<button class="btn btn-warning" id="btn-search">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</div>
			</form>
		</div>
		<div class="col-xs-1">
			<button type="button" id="btn-add" class="btn btn-sm btn-warning">
				<i class="glyphicon glyphicon-plus"></i>
			</button>
		</div>
		<div></div>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>CODE</th>
					<th>NAME</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="roleList" align="center">
				<c:forEach items="${list}" var="role">
					<tr>
						<td>${role.code}</td>
						<td>${role.name}</td>
						<td><div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="loadDataGet(${role.id})">Edit</a>
									</li>
									<li><a href="#" onclick="delDataGet(${role.id})">Delete</a>
									</li>
								</ul>
							</div></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<!-- modal begin -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="box box-warning box-solid">
			<div class="box-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="box-title">ROLE</h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>
<!-- Modal-end -->

<script>
	$("#btn-add").click(function() {
		$.ajax({
			url : '${contextName}/role/add.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				//mengisi data html
				$("#myModal").find(".box-body").html(data);
				//tampilkan modal
				$("#myModal").modal("show");
			}
		});
	});
	
	function simpanData() {
		var dataformatnyajson = getFormData($('#formrole'));
		//membuat variable name yg diambil dari formrole ber id = name, yg diambil valuenya
		var name = $('#formrole').find('#name').val();
		$.ajax({
			type : 'get',
			url : contextName + '/api/role/checkName/'+name,
			success : function(data){
				alert('BERHASIL!!!');
			}
		});
		
		$.ajax({
			type : 'post',
			url : contextName + '/api/role/',
			data : JSON.stringify(dataformatnyajson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(data) {
				alert('Data successfully saved!');
				$('#myModal').modal('hide');
				loadData();
			},
			error : function(data) {
				alert('gagal');
			}
		});

	}

	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/role/list.html',
			success : function(data) {
				$('#roleList').html(data);
			}
		});
	}

	function loadDataGet(id) {
		$.ajax({
			type : 'get',
			url : '${contextName}/role/edit.html',
			data : {
				id : id
			},
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.box-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");
			}
		});
	}

	function updateData() {
		var dataformatjson = getFormData($('#roleEdit'));
		$.ajax({
			type : 'put',
			url : '${contextName}/api/role/update',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(data) {
				alert("Data successfully updated!");
				$('#myModal').modal('hide');
				loadData();
			}
		});
	}

	function delDataGet(id) {
		$.ajax({
			type : 'get',
			url : '${contextName}/role/delete.html',
			data : {
				id : id
			},
			success : function(data) {
				$('#myModal').find('.box-body').html(data);
				$('#myModal').modal("show");
			}
		});
	}

	function deleteData() {
		var dataformatjson = getFormData($('#roleDelete'));
		$.ajax({
			type : 'put',
			url : '${contextName}/api/role/del',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(data) {
				loadData();
				alert("Data successfully deleted!")
			}
		});
	}
	
	$("#btn-search").click(function(){
		$.ajax({
			url : contextName + '/role/search.html',
			data : {cari : $('#txt-search').val()},
			dataType : 'html',
			type : 'get',
			success : function(data){
				$('#roleList').html(data);
			}
		});
	});
</script>
