<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach items="${searchList}" var="role">
	<tr>
		<td>${role.code}</td>
		<td>${role.name}</td>
		<td><div class="btn-group pull-right">
				<button type="button" class="btn btn-default dropdown-toggle"
					data-toggle="dropdown">
					<i class="glyphicon glyphicon-menu-hamburger"></i>
				</button>
				<ul class="dropdown-menu">
					<li><a href="#" onclick="loadDataGet(${biodata.id})">Edit</a>
					</li>
					<li><a href="#" onclick="delDataGet(${biodata.id})">Delete</a>
					</li>
				</ul>
			</div></td>
	</tr>
</c:forEach>
