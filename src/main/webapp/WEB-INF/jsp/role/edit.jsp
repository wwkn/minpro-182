<form role="form" id="roleEdit">
	<div class="box-body">
		<div class="form-group">
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="id" id="edit-id" value="${role.id}">
			</div>
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="createdBy"
					id="edit-createdBy" value="${role.createdBy}">
			</div>
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="createdOn"
					id="edit-createdOn" value="${role.createdOn}">
			</div>
			<div class="col-xs-4">
				<input type="hidden" class="form-control" name="isDelete"
					id="edit-isDelete" value="${role.isDelete}">
			</div>
		</div>
		<div class="form-group">
			<label>Code</label> <input type="text" class="form-control"
				name="code" id="edit-code" value="${role.code}" readonly>
		</div>
		<div class="form-group">
			<label>Nama</label> <input type="text" class="form-control"
				name="name" id="edit-name" value="${role.name}">
		</div>
		<div class="form-group">
			<label>Description</label>
			<input class="form-control" name="description" id="edit-description" value="${role.description}">
			</div>
		</div>
	<!-- /.box-body -->

	<div class="box-footer pull-right">
		<button type="button" class="btn btn-warning" onclick="updateData()"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
