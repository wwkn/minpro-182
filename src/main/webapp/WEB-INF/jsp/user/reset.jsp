
<form id="reset">
<div class="box-body">
		<div class="form-group">
			<input type="hidden" class="form-control" name="id" id="edit-id" value="${user.id}"> 
			<input type="hidden" class="form-control" name="username" id="edit-username" value="${user.username}">
			<input type="hidden" class="form-control" name="roleId" id="edit-roleId" value="${user.roleId}">
			<input type="hidden" class="form-control" name="createdBy" id="edit-createdBy" value="${user.createdBy}">
			<input type="hidden" class="form-control" name="createdOn" id="edit-createdOn" value="${user.createdOn}"> 
			<input type="hidden" class="form-control" name="isDelete" id="edit-isDelete" value="${user.isDelete}"> 
			<input type="hidden" class="form-control" name="mobileToken" id="edit-mobileToken" value="${user.mobileToken}">
			<input type="hidden" class="form-control" name="mobileFlag" id="edit-mobileFlag" value="${user.mobileFlag}">
		</div>
	</div>

	<div class="form-group">
		<label>Password</label> <input class="form-control" type="password" name="password"
			placeholder="Password" id="pw1"></>
	</div>
	<div class="form-group">
		<label>Password</label> <input class="form-control" type="password" 
			placeholder="Re-type Password" id="pw2"></>
	</div>
	<div class="box-footer pull-right">
	<button id="btnSubmit" class="btn btn-warning" onclick="updatePassword()" type="button"
		data-dismiss="modal">Simpan</button>
	<button class="btn btn-warning" data-dismiss="modal" aria-label="Close">Cancel</button>
	</div>
	

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#btnSubmit").click(function () {
                var password = $("#pw1").val();
                var confirmPassword = $("#pw2").val();
                if (password != confirmPassword) {
                    alert("Password tidak sama!");
                    return false;
                }
                return true;
            });
        });
    </script>
</form>