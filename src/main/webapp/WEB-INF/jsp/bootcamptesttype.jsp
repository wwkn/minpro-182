<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page isELIgnored="false"%>



<div class="box-header">
	<h3 class="box-title">BOOTCAMP TEST TYPE LIST</h3>
</div>
<!-- /.box-header -->
<div class="box-body">
	<div class="col-xs-11">
		<form role="form">
			<div class="col-xs-3">
				<div class="form-group">
					<input type="text" class="form-control" id="searchBar"
						placeholder="Search by name">
				</div>
			</div>
			<button type="button" class="btn btn-primary">
				<i class="glyphicon glyphicon-search"></i>
			</button>
		</form>
	</div>
	<div class="col-xs-1">
		<button type="button" id="btn-add" class="btn btn-sm btn-success">
			<i class="glyphicon glyphicon-plus"></i>
		</button>
	</div>
	<div></div>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th><center>NAME</center></th>
				<th><center>CREATED BY</center></th>
			</tr>
		</thead>
		<tbody id="bootcamptesttypelist" align="center">
		</tbody>
	</table>
</div>


<!-- Modal-begin -->
<div id="modalQue" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Tambah Data bootcamp Test Type</h4>
			</div>
			<div class="modal-body"></div>

		</div>
	</div>
</div>
<!-- Modal-end -->

<script>
	$("btn-add").click(function() {
		alert("aduh");

		$.ajax({
			url : '${contextName}/bootcamptesttype/add.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$('#modalQue').find('.modal-body').html(data);
				$('#modalQue').modal("show");
			}
		});
	});

	function simpanData() {
		var dataformatnyajson = getFormData($('#formAdd'));

		$.ajax({
			type : 'post',
			url : contextName + '/api/btt/',
			data : JSON.stringify(dataformatnyajson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();

				$('#modalQue').modal('hide');
			},
			error : function(d) {
				alert('gagal');
			}
		});

	}

	var result = '';

	function array(i, e) {
		result += '<tr>';
		result += '<td>' + e.name + '</td>';
		result += '<td>' + e.user.username + '</td>';
		result += '<td><div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-menu-hamburger"></i></button><ul class="dropdown-menu"><li><a href="#" data-toggle="modal" data-target="#editBtt" onclick="loadDataGet('
				+ e.id
				+ ')">Edit</a></li><li><a href="#">Delete</a></li></ul></div></td>'
		result += '</tr>';
	}

	function show(d) {
		result = '';
		$(d).each(array);
		$('#bootcamptesttypelist').html(result);
	}

	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/api/btt/',
			success : function(d) {
				show(d);
			}
		});
	}

	$(function() {
		$('#example1').DataTable()
		$('#example2').DataTable({
			'paging' : true,
			'lengthChange' : false,
			'searching' : false,
			'ordering' : true,
			'info' : true,
			'autoWidth' : false
		})
	})

	function loadDataGet(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/bootcamptesttype/edit.html',
			data : {
				"id" : id
			},
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.modal-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");

			}
		});
		alert(id);
	}

	function updateData() {
		var dataformatjson = getFormData($('#bootcampTestTypeEdit'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/btt/',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();
			}
		});
	}

	$(document).ready(function() {
		loadData();
	});
</script>