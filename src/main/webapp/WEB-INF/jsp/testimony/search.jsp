<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach items="${searchList}" var="testimony">
	<tr>
		<td>${testimony.title}</td>
		<td>
			<div class="btn-group">
				<button type="button" class="btn btn-default dropdown-toggle"
					data-toggle="dropdown">
					<i class="glyphicon glyphicon-menu-hamburger"></i>
				</button>
				<ul class="dropdown-menu">
					<li><a href="#" onclick="loadDataGet(${testimony.id})">Edit</a>
					</li>
					<li><a href="#" onclick="loadDataGetDelete(${testimony.id})">Delete</a>
					</li>
				</ul>
			</div>
		</td>
	</tr>
</c:forEach>
