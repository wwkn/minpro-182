
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">TESTIMONY LIST</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">

			<div class="col-md-3">
				<div class="input-group">
					<input type="text" id="txt-search" placeholder="Search by title"
						class="form-control"> <span class="input-group-btn">
						<button class="btn btn-warning" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive table-bordered table-hover">
			<thead>
				<tr>
					<th>TITLE</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="testimonyList">

				<c:forEach items="${list}" var="testimony">
					<tr>
						<td>${testimony.title}</td>

						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="loadDataGet(${testimony.id})">Edit</a>
									</li>
									<li><a href="#"
										onclick="loadDataGetDelete(${testimony.id})">Delete</a></li>
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
<!-- Modal-begin -->
<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>
<!-- Modal-end -->


<script>

	//munculkan modal
	$("#btn-add").click(function() {

		$.ajax({
			url : '${contextName}/testimony/add.html',
			type : 'get',
			dataType : 'html',
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.modal-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");
				$("#myModal").find('.box-title').html('Add Testimony');
			}
		});
	});
	
	//proses input
	function simpanData() {
		var dataformatnyajson = getFormData($('#addTestimony'));

		$.ajax({
			type : 'post',
			url : contextName + '/api/testimony/',
			data : JSON.stringify(dataformatnyajson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(data) {
				alert('Data successfully saved!');
				$('#myModal').modal('hide');
				loadData();
			},
			error : function(data) {
				alert('gagal');
			}
		});

	}
	
	

	//mengambil id untuk edit data
	function loadDataGet(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/testimony/edit.html',
			data : {
				"id" : id
			},
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.modal-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");

			}
		});

	}

	//proses edit data
	function updateData() {
		var dataformatjson = getFormData($('#testimonyEdit'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/testimony/',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(data) {
				alert('Data successfully updated!');
				$('#myModal').modal('hide');
				loadData();
			},
			error : function(data) {
				alert('gagal');
			}
		});
	}

	//mengambil id untuk delete data
	function loadDataGetDelete(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/testimony/delete.html',
			data : {
				"id" : id
			},
			success : function(data) {
				//mengisi data html
				$('#myModal').find('.modal-body').html(data);
				//tampilkan modal
				$("#myModal").modal("show");

			}
		});
	}
	//proses delete data
	function deleteData() {
		var dataformatjson = getFormData($('#testimonyDelete'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/testimony/delete',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				alert('Data successfully deleted!');
				$('#myModal').modal('hide');
				loadData();
			},
			error : function(data) {
				alert('gagal');
			}
		});
	}
	
	//show list
	function show(data){
		result = '';
		$(data).each(array);
		$('#testimonyList').html(result);
	}
	
	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/testimony/isi.html',
			success : function(d) {
				$('#testimonyList').html(d);
			}
		});
	}

	$(document).ready(function() {
		loadData();
	});
	
	//fungsi search
	$("#btn-search").click(function(){
		$.ajax({
			url : contextName + '/testimony/search.html',
			data : {cari : $('#txt-search').val()},
			dataType : 'html',
			type : 'get',
			success : function(data){
				$('#testimonyList').html(data);
			}
		});
	});
</script>
