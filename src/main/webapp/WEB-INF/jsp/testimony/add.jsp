<form role="form" id="addTestimony">
	<div class="box-body">
		<div class="form-group">
			<label>Title</label> 
			<input class="form-control" name="title"
				placeholder="Title" id="add-title">
		</div>

		<div class="form-group">
			<label>Content</label>
			<textarea class="form-control" name="content" id="add-content"
				placeholder="Content" required></textarea>
		</div>

		<div class="box-footer">
			<button type="button" class="btn btn-warning" onclick="simpanData()"
				data-dismiss="modal">Save</button>
			<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
		</div>
	</div>
</form>
