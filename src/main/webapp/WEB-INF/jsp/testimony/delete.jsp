<form role="form" id="testimonyDelete">
	<div class="box-body">

		<input type="hidden" class="form-control" name="id" id="delete-id"
			value="${testimony.id}"> <input type="hidden"
			class="form-control" name="createdBy" id="delete-createdBy"
			value="${testimony.createdBy}"> <input type="hidden"
			class="form-control" name="createdOn" id="delete-createdOn"
			value="${testimony.createdOn}"> <input type="hidden"
			class="form-control" name="isDelete" id="delete-isDelete"
			value="${testimony.isDelete}"> <input type="hidden"
			class="form-control" name="title" id="delete-title"
			value="${testimony.title}">
			<input type="hidden" class="form-control" name="content" id="delete-content" value="${testimony.content}">
			


		<h3>Are You Sure Want To Delete Testimony ${testimony.title } ?</h3>

	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="button" class="btn btn-warning" onclick="deleteData()"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
