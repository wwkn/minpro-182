<form id="deleteAjah" class="form-horizontal">
	<input type="hidden" id="cd-id" name="id" class="form-control" value="${catt.id}"/>
	<input type="hidden" id="cd-createdBy" name="createdBy" class="form-control" value="${catt.createdBy}"/>
	<input type="hidden" id="cd-createdOn" name="createdOn" class="form-control" value="${catt.createdOn}"/>
	<input type="hidden" id="cd-isDelete" name="isDelete" class="form-control" value="${catt.isDelete}"/>
	<input type="hidden" id="cd-modifiedBy" name="modifiedBy" class="form-control" value="${catt.modifiedBy}"/>
	<input type="hidden" id="cd-modifiedON" name="modifiedOn" class="form-control" value="${catt.modifiedOn}"/>
	<input type="hidden" id="cd-code" name="code" class="form-control" placeholder="Code" value="${catt.code}"/>
	<input type="hidden" id="cd-name" name="name" class="form-control" placeholder="Name" value="${catt.name}" />
	<input type="hidden" id="cd-description" name="description" class="form-control" placeholder="Description" value="${catt.description}"/>
	<div class="box-body">
		<div class="form-group">
			<center>
				<label><h3>Yakin nih <b>${catt.name}</b> dihapus?</h3></label>
			</center>
		</div>
	</div>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="submit" onClick="deleteC()">Ok</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>