<form id="editAjah" class="form-horizontal">
	<input type="hidden" id="ce-id" name="id" class="form-control" value="${catt.id}"/>
	<input type="hidden" id="ce-createdBy" name="createdBy" class="form-control" value="${catt.createdBy}"/>
	<input type="hidden" id="ce-createdOn" name="createdOn" class="form-control" value="${catt.createdOn}"/>
	<input type="hidden" id="ce-isDelete" name="isDelete" class="form-control" value="${catt.isDelete}"/>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="ce-code" name="code" class="form-control" placeholder="Code" value="${catt.code}"/>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="ce-name" name="name" class="form-control"
				placeholder="Name" value="${catt.name}" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<textarea id="ce-description" name="description" class="form-control"
				placeholder="Description">${catt.description}</textarea>
		</div>
	</div>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="submit" onClick="update()">Submit</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>