
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">CATEGORY</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control" /> <span
						class="input-group-btn">
						<button class="btn btn-warning">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>CODE</th>
					<th>NAME</th>
					<th>STATUS</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="isiData">
				<c:forEach items="${list}" var="category">
					<tr>
						<td>${category.code}</td>
						<td>${category.name}</td>
						<td>
							<c:choose>
								<c:when test="${category.isDelete=='0'}">
								ACTIVE
								</c:when>
								<c:otherwise>
								NON ACTIVE
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#"
										onclick="loadDataGet('${category.id}')">Edit</a></li>
									<li><a href="#" onclick="loadDataDel('${category.id}')">Delete</a></li>
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modal-form">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title">CATEGORY</h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>

<script>
	$(function() {
		loadData();
	});
	
	function show(data){
		result = '';
		$(data).each(array);
		$('#isiData').html(result);
	}
	
	function loadData(){
		$.ajax({
			type : 'get',
			url : contextName + '/category.html',
			success : function(data){
				show(data);
			}
			
		});
	}

	$("#btn-add").click(function(){
		$.ajax({
			url : '${contextName}/category/add.html',
			type : 'get',
			dataType : 'html',
			success : function(t){
				$("#modal-form").modal('show');
				$("#modal-form").find('.box-body').html(t);
			}
		});
	});
	
	function simpan(){
		var dataformatnyajson = getFormData($('#addAjah'))
		$.ajax({
			type : 'post',
			url : contextName + '/api/category/',
			data : JSON.stringify(dataformatnyajson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(t){
				$('#isiData').html();
				alert('Data berhasil disimpan');
				$('#modal-form').modal('hide');
			},
			error : function(t){
				alert('Gagal');
			}
		});
	}
	
	function update(){
		var json = getFormData($('#editAjah'));
		$.ajax({
			url : contextName + '/api/category/',
			type : 'put',
			data : JSON.stringify(json),
			dataType : 'json',
			contentType : 'application/json',
			success : function(t){
				$('#isiData').html();
				$('#modal-form').modal('hide');
			},
			error : function(){
				alert("Coba Lagi");
			}
		});
	}
	
	function loadDataGet(id){
		$.ajax({
			url : contextName + '/category/edit.html',
			type : 'get',
			data : {"id": id},
			success : function(t){
				$('#modal-form').find('.box-body').html(t);
				$('#modal-form').modal('show');
			}
		});
	}
	
	function deleteC(){
		var json= getFormData($('#deleteAjah'));
		$.ajax({
			url : contextName +"/api/category/d",
			type : 'put',
			data : JSON.stringify(json),
			dataType : 'json',
			contentType : 'application/json',
			success : function(t){
				alert("Terhapus");
				$('#modal-form').modal('hide');
				loadData();
			}
		});
	}
	
	function loadDataDel(id){
		$.ajax({
			url : contextName +"/category/delete.html",
			type : 'get',
			data : {'id':id},
			success : function(t){
				$('#modal-form').find('.box-body').html(t);
				$('#modal-form').modal('show');
			},
			error : function(){
				alert("gaKehapus!");
			}
		});
	}
</script>