<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="documentTestInputQuestion" class="form-horizontal">
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="questionId" id="selectQuestion">
				<option value="" disabled selected>Question</option>
				<c:forEach items="${questionList}" var="question">
					<option id="valQues" value="${question.id}">${question.question}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="button" data-dismiss="modal" id="btn-pencet-question">Submit</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>

<script>
	$('.select2').select2()
</script>