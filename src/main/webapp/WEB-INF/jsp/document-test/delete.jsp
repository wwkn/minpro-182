<form role="form" id="docTestDelete">
	<div class="box-body">
		<div class="form-group">
				<input type="hidden" class="form-control" name="id" id="dId" value="${docTest.id}">
				<input type="hidden" class="form-control" name="testId" id="testId" value="${docTest.testId}">
				<input type="hidden" class="form-control" name="testTypeId" id="testTypeId" value="${docTest.testTypeId}">
				<input type="hidden" class="form-control" name="version" id="version" value="${docTest.version}">
				<input type="hidden" class="form-control" name="token" id="token" value="${docTest.token}">
				
				<input type="hidden" class="form-control" name="createdOn" id="createdOn" value="${docTest.createdOn}">
				<input type="hidden" class="form-control" name="createdBy" id="createdBy" value="${docTest.createdBy}">
			<center>
				<h1>Are You Sure Want To Delete ?</h1>
			</center>
		</div>
	</div>
	<div class="box-footer">
		<center>
			<button type="button" class="btn btn-success" onclick="deleteData()"
				data-dismiss="modal">Yes</button>
			<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
		</center>
	</div>
</form>