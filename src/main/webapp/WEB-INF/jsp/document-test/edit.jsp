<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="documentTestEdit" class="form-horizontal">
	<div class="form-group">
		<input type="hidden" class="form-control" name="id" id="id" value="${document.id}">
		<input type="hidden" class="form-control" name="createdBy" id="createdBy" value="${document.createdBy}">
		<input type="hidden" class="form-control" name="createdOn" id="createdOn" value="${document.createdOn}">
		<input type="hidden" class="form-control" name="isDelete" id="isDelete" value="${document.isDelete}">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="testId" id="testId">
				<option value="${document.test.id}" disabled selected>${document.test.name}</option>
				<c:forEach items="${testList}" var="test">
					<option value="${test.id}">${test.name}</option>
				</c:forEach>>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="testTypeId" id="testTypeId">
				<option value="${document.testType.id}" disabled selected>${document.testType.name}</option>
				<c:forEach items="${testTypeList}" var="testType">
					<option value="${testType.id}">${testType.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" class="form-control" name="version" id="version"
				value="${document.version}" placeholder="Version" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<div class="input-group">
				<input type="text" name="token" placeholder="Token" id="token"
					class="form-control" value="${document.token}"> <span
					class="input-group-btn">
					<button type="button" id="btn-generate"
						class="btn btn-warning btn-flat">Generate</button>
				</span>
			</div>
		</div>
	</div>
	<!-- ADD Question to document test detail -->
	<div class="form-group">
		<div class="col-md-12">
			<button type="button" id="btn-add-question" class="btn btn-warning">+QUESTION</button>
		</div>
	</div>
	<!-- TABLE QUESTION -->
	<div class="form-group">
		<div class="col-md-12">
			<table class="table table-responsive text-center">
			<thead>
				<tr>
					<th>Question</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="tBodyQuestion" align="center">
				
			
			</tbody>
		</table>
		</div>
	</div>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="submit" data-dismiss="modal" id="btn-pencet-edit">Submit</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
<script>
	$("#btn-generate").click(function() {
		$("#token").val('${iniToken}');
	});
	//add question to documentdetail
	$("#btn-add-question").click(function() {
		$.ajax({
			url : contextName + '/document-test/add-question.html',
			type : 'GET',
			dataType : 'html',
			success : function(data) {
				$('#inputQuestionModal').find('.box-body').html(data);
				$('#inputQuestionModal').find('.box-title').html("ADD QUESTION");
				$('#inputQuestionModal').modal("show");
			}
		});
	});
	$('.select2').select2()
</script>