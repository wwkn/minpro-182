<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="documentTestInput" class="form-horizontal">
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="testId">
				<option value="" disabled selected>Test</option>
				<c:forEach items="${testList}" var="test">
					<option value="${test.id}">${test.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="testTypeId">
				<option value="" disabled selected>Test Type</option>
				<c:forEach items="${testTypeList}" var="testType">
					<option value="${testType.id}">${testType.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" class="form-control" name="version"
				placeholder="Version" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<div class="input-group">
				<input type="text" name="token" placeholder="Token" id="token"
					class="form-control"> <span class="input-group-btn">
					<button type="button" id="btn-generate" class="btn btn-warning btn-flat">Generate</button>
				</span>
			</div>
		</div>
	</div>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="submit" onClick="saveData()"
			data-dismiss="modal">Submit</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
<script>
$("#btn-generate").click(function(){
	$("#token").val('${iniToken}');
});
$('.select2').select2()
</script>