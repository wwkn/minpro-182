
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">DOCUMENT TEST</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control"
						placeholder='Search By name / version' /> <span
						class="input-group-btn">
						<button class="btn btn-warning">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive text-center">
			<thead>
				<tr>
					<th>NAME</th>
					<th>VERSION</th>
					<th>TEST</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="documentTestList" align="center">


			</tbody>
		</table>
	</div>
</div>
<!-- modal begin -->
<div id="documentTestModal" class="modal fade">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-body">
				<!-- modal content -->
			</div>
		</div>
	</div>
</div>
<!-- Modal END -->
<div id="inputQuestionModal" class="modal fade">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title"></h4>
			</div>
			<div class="box-body">
				<!-- modal content -->
			</div>
		</div>
	</div>
</div>
<script>
	var documentViewModel = {};
	var documentTestDetail = [];
	$(document).ready(function() {
		loadData();
	});
	//add
	$("#btn-add").click(
			function() {
				$.ajax({
					url : "${contextName}/document-test/add.html",
					type : 'GET',
					dataType : 'html',
					success : function(data) {
						$('#documentTestModal').find('.box-body').html(data);
						$('#documentTestModal').find('.box-title').html(
								"ADD DOCUMENT TEST");
						$('#documentTestModal').modal("show");
					}
				});
			});
	function saveData() {
		var dataFormatnyajson = getFormData($('#documentTestInput'));
		$.ajax({
			type : 'POST',
			url : contextName + '/api/document-test/',
			data : JSON.stringify(dataFormatnyajson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();
			},
		});
	}
	//edit
	function loadDataGet(id) {
		$.ajax({
			type : 'get',
			url : contextName + '/document-test/edit.html',
			data : {
				"id" : id
			},
			success : function(data) {
				$('#documentTestModal').find('.box-body').html(data);
				$('#documentTestModal').find('.box-title').html(
						"EDIT DOCUMENT TEST");
				$('#documentTestModal').modal('show');
				//ambil list question di sini'
				loadDataDocumentDetail(id);
	
			}
		});
	}
	//fungsi untuk menambah question ke edit document
	$('#inputQuestionModal').on('click','#btn-pencet-question', function() {
				var dataFormatnyaJson = {};
				var anu = $('#inputQuestionModal').find('#selectQuestion')
						.val();
				var quesText = $('#inputQuestionModal').find(
						'#selectQuestion option:selected').text();
				var docTestId = $('#documentTestModal').find('#id').val();

				//sebelah kiri sesuai model : sebelah kanan sesuai field name dari view
				var item = {
					questionId : anu,
					questionText : quesText,
					documentTestId : docTestId,
				}
				documentTestDetail.push(item);
				//tampilkan modal document
				$('#inputQuestionModal').modal('hide');
				$('#selectQuestion').val('');
				showTableQuestion();
	});
	
	function loadDataDocumentDetail(id){
		$.ajax({
			type : 'GET',
			url : contextName + '/api/document-test-detail/' + id,
			success:function(d){
				detailDocumentDet(d);
			}
		});
	}
	function detailDocumentDet(x){
		resultRowQuestion = '';
		$(x).each(arrayDocumentDetail);
		$('#tBodyQuestion').html(resultRowQuestion);
	}
	function showTableQuestion() {
		$(documentTestDetail).each(function(i, e) {
			resultRowQuestion += '<tr>';
			resultRowQuestion += '<td>' + e.questionText + '</td>';
			resultRowQuestion += '</tr>';

		});
		$('#tBodyQuestion').html(resultRowQuestion);
	}

	$('#documentTestModal').on('click', '#btn-pencet-edit', function() {
		var id = $('#documentTestModal').find('#id').val();
		var cBy = $('#documentTestModal').find('#createdBy').val();
		var cOn = $('#documentTestModal').find('#createdOn').val();
		var iSD = $('#documentTestModal').find('#isDelete').val();
		var tstId = $('#documentTestModal').find('#testId option:selected').val();
		var typId = $('#documentTestModal').find('#testTypeId option:selected').val();
		var ver = $('#documentTestModal').find('#version').val();
		var tkn = $('#documentTestModal').find('#token').val();
		
		var docItem = {
			id : id,
			createdBy : cBy,
			createdOn : cOn,
			isDelete : iSD,
			testId : tstId,
			testTypeId : typId,
			version : ver,
			token : tkn
		}
		
		documentViewModel.documentTest = docItem;
		documentViewModel.documentTestDetail = documentTestDetail;
		
		$.ajax({
			type : 'PUT',
			url : contextName + '/api/document-test/',
			data : JSON.stringify(documentViewModel),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();
				$('#tBodyQuestion').html('');
				documentTestDetail = [];
				$('#documentTestModal').modal('hide');
			},
			error : function(d) {
				alert('gagal');
			}
		});
	});

	//list
	var result = '';
	function clearForm() {

	}
	function array(i, e) {
		result += '<tr>';
		result += '<td>' + e.testType.name + '</td>';
		result += '<td>' + e.version + '</td>';
		result += '<td>' + e.test.name + '</td>';
		result += '<td>'
				+ '<div class="btn-group">'
					+ '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">'
						+ '<i class="glyphicon glyphicon-menu-hamburger"></i>'
					+ '</button>' 
					+ '<ul class="dropdown-menu">'
						+ '<li><a href="#" data-toggle="modal" onclick="loadDataGet('+ e.id + ')">Edit</a></li>' 
						+ '<li><a href="#" onClick="loadDataDelete('+e.id+')">Delete</a></li>'
					+ '</ul>' 
				+ '</div>';
		result += '</tr>';
	}
	function arrayDocumentDetail(i,e){
		resultRowQuestion += '<tr>';
		resultRowQuestion += '<td>' + e.question.question + '</td>';
		resultRowQuestion += '</tr>';
	}
	function show(d) {
		result = '';
		$(d).each(array);
		$('#documentTestList').html(result);
	}
	function loadData() {
		$.ajax({
			type : 'get',
			url : contextName + '/api/document-test/',
			success : function(d) {
				show(d);
			}
		});
	}
	//delete
	function loadDataDelete(id){
		$.ajax({
			type : 'get',
			url : contextName + '/document-test/delete.html',
			data : {"id":id},
			success : function(data) {
				$('#documentTestModal').find('.box-body').html(data);
				$('#documentTestModal').modal('show');
			}
		});
	}
	function deleteData() {
		var dataformatjson = getFormData($('#docTestDelete'));
		$.ajax({
			type : 'put',
			url : contextName + '/api/document-test/del/',
			data : JSON.stringify(dataformatjson),
			dataType : 'json',
			contentType : 'application/json',
			success : function(d) {
				loadData();
			}
		});
	}
</script>