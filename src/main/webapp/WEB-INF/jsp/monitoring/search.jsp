<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach items="${searchList}" var="monitoring">
					<tr>
						<td>${monitoring.biodata.name}</td>
						<td>${monitoring.idleDate}</td>
						<td>${monitoring.placementDate}</td>
						<td><div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#" onclick="loadDataGet(${monitoring.id})">Edit</a>
									</li>
									<li><a href="#" onclick="loadDataPlacement(${monitoring.id})">Placement</a>
									<li><a href="#" onclick="delDataGet(${monitoring.id})">Delete</a>
									</li>
								</ul>
							</div></td>
					</tr>
				</c:forEach>