<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="add-monitoring" class="form-horizontal">
	<div class="form-group">
		<div class="col-md-12">
			<select class="form-control select2" style="width: 100%;"
				name="biodataId" required>
				<option value="" disabled selected>Name</option>
				<c:forEach items="${listBiodata}" var="biodata">
					<option value="${biodata.id}">${biodata.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<div class="input-group date">
				<input type="text" class="form-control"
					name="idleDate" placeholder="Idle Date" onfocus="(this.type='date')" required/>
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="lastProject" name="lastProject" class="form-control"
				placeholder="Last Project at" required/>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<textarea id="idleReason" name="idleReason" class="form-control"
				placeholder="Idle Reason" required></textarea>
		</div>
	</div>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="submit">Save</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>


<script>

	$('.select2').select2()
</script>