<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="edit-monitoring" class="form-horizontal">
	<div class="form-group">
		<div class="col-xs-4">
			<input type="hidden" class="form-control" name="id" id="edit-id"
				value="${monitoring.id}"/>
			<input type="hidden" class="form-control" name="createdBy" id="edit-createdBy"
				value="${monitoring.createdBy}">
			<input type="hidden" class="form-control" name="createdOn" id="edit-createdOn"
				value="${monitoring.createdOn}">
			<input type="hidden" class="form-control" name="isDelete" id="edit-isDelete"
				value="${monitoring.isDelete}">
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label>Name</label>
			<select class="form-control select2" style="width: 100%;"
					name="biodataId">
				<option value="${monitoring.biodata.id}" disabled selected>${monitoring.biodata.name}</option>
				<c:forEach items="${biodataList}" var="biodata">
					<option value="${biodata.id}">${biodata.name}</option>
				</c:forEach>
			</select>
		</div>	
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label>Idle Date</label>
			<input type="text" class="form-control" id="edit-idleDate" name="idleDate"
				value="${monitoring.idleDate}" onfocus="(this.type='date')"/>		
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label>Last Project</label>
			<input type="text" class="form-control" id="edit-lastProject" name="lastProject"
				value="${monitoring.lastProject}"/>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label>Idle Reason</label>
			<textarea class="form-control" id="edit-idleReason" name="idleReason">${monitoring.idleReason}</textarea>
		</div>
	</div>
	
	<div class="box-footer pull-right">
		<button type="button" class="btn btn-warning" onclick="updateMonitoring()"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>

<script>

	$('.select2').select2()
</script>