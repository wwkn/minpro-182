<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="placement" class="form-horizontal">
	<div class="form-group">
		<div class="col-xs-4">
			<input type="hidden" class="form-control" name="id" id="pm-id"
				value="${monitoring.id}"/>
			<input type="hidden" class="form-control" name="biodataId" id="pm-biodataId"
				value="${monitoring.biodata.id}"/>
			<input type="hidden" class="form-control" name="idleDate" id="pm-idleDate"
				value="${monitoring.idleDate}"/>
			<input type="hidden" class="form-control" name="lastProject" id="pm-lastProject"
				value="${monitoring.lastProject}"/>
			<input type="hidden" class="form-control" name="idleReason" id="pm-idleReason"
				value="${monitoring.idleReason}"/>
			<input type="hidden" class="form-control" name="createdBy" id="edit-createdBy"
				value="${monitoring.createdBy}">
			<input type="hidden" class="form-control" name="createdOn" id="edit-createdOn"
				value="${monitoring.createdOn}">
			<input type="hidden" class="form-control" name="isDelete" id="edit-isDelete"
				value="${monitoring.isDelete}">
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label>Placement Date</label>
			<input type="text" class="form-control" id="pm-placementDate" name="placementDate"
				placeholder="Placement Date" value="${monitoring.placementDate}" onfocus="(this.type='date')" required/>		
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label>Placement At</label>
			<input type="text" class="form-control" id="pm-placementAt" name="placementAt"
				placeholder="Placement At" value="${monitoring.placementAt}" required/>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label>Notes</label>
			<textarea class="form-control" id="pm-notes" name="notes" placeholder="Notes">${monitoring.notes}</textarea>
		</div>
	</div>
	
	<div class="box-footer pull-right">
		<button type="submit" class="btn btn-warning">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>