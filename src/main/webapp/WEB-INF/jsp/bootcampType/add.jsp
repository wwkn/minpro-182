<form id="bootcampTypeAdd" class="form-horizontal">
	<div class="box-body">
	<div class="form-group">
		<label>Name</label><input class="form-control" name="name" id="add-name"/>
	</div>
	<div class="form-group">
		<label>Notes</label><textarea class="form-control" name="notes" id="add-note"></textarea>
	</div>
	<div class="box-footer pull-right">
	<button type="submit" class="btn btn-warning" >Save</button>
	<button type="reset" class="btn btn-warning" data-dismiss="modal" >Cancel</button>
	</div>
	</div>
</form>
