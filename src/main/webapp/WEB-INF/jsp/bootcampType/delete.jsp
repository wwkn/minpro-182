<form id="bootcampTypeDelete" role="form">
	<div class="box-body">
		<input type="hidden" class="form-control" name="id" id="delete-name" value="${bootcampType.id}">
		<input type="hidden" class="form-control" name="createdBy" id="delete-createdBy" value="${bootcampType.createdBy}">
		<input type="hidden" class="form-control" name="createdOn" id="delete-createdOn" value="${bootcampType.createdOn}">
		<input type="hidden" class="form-control" name="modifiedOn" id="delete-modifiedOn" value="${bootcampType.modifiedOn}">
		<input type="hidden" class="form-control" name="modifiedBy" id="delete-modifiedBy" value="${bootcampType.modifiedBy}">
		<input type="hidden" class="form-control" name="isDelete" id="delete-isDelete" value="${bootcampType.isDelete}">	
		<input type="hidden" class="form-control" name="name" id="delete-name" value="${bootcampType.name}">
		<input type="hidden" class="form-control" name="notes" id="delete-notes" value="${bootcampType.notes}">
	</div>
	<div class="form-group">
		<label><h3>Are You Sure You Want To De-Activate? ${bootcampType.name}</h3></label>
	</div>
	
	<div class="box-footer pull-right">
		<button type="button" class="btn btn-warning" onclick="deleteData()"
			data-dismiss="modal">Delete</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
