<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="menuAccessDelete" role="form">
	<div class="box-body">
		<input type="hidden" class="form-control" name="id" id="delete-id" value="${menuAccess.id}">
		<input type="hidden" class="form-control" name="menuId" id="delete-menuId" value="${menuAccess.menuId}">
		<input type="hidden" class="form-control" name="roleId" id="delete-RoleId" value="${menuAccess.roleId}">
		<input type="hidden" class="form-control" name="createdBy" id="delete-createdBy" value="${menuAccess.createdBy}">
		<input type="hidden" class="form-control" name="createdOn" id="delete-createdOn" value="${menuAccess.createdOn}">
		
		<div class="form-group">
			<label><h3>Are You Sure You Want To Delete The Access of ${menuAccess.role.name} to menu ${menuAccess.menuId}?</h3></label>
		</div>
	
		<div class="box-footer">
			<button type="button" class="btn btn-warning" onclick="deleteAccess(${menuAccess.id})"
					data-dismiss="modal" >Delete</button>
			<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
		</div>
	</div>
</form>