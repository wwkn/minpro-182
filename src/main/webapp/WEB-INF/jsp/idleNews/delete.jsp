<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="idleNewsDelete" role="form">
	<div class="box-body">
		<input type="hidden" class="form-control" name="id" id="delete-id" value="${idleNews.id}">
		<input type="hidden" class="form-control" name="createdBy" id="delete-createdBy" value="${idleNews.createdBy}">
		<input type="hidden" class="form-control" name="createdOn" id="delete-createdOn" value="${idleNews.createdOn}">
		<input type="hidden" class="form-control" name="modifiedBy" id="delete-modifiedBy" value="${idleNews.modifiedBy}">
		<input type="hidden" class="form-control" name="modifiedOn" id="delete-modifiedOn" value="${idleNews.modifiedOn}">
		<input type="hidden" class="form-control" name="isDelete" id="delete-isDelete" value="${idleNews.isDelete}">
		<input type="hidden" class="form-control" name="categoryId" id="delete-categoryId" value="${idleNews.categoryId}">
		<input type="hidden" class="form-control" name="isPublish" id="delete-isPublish" value="${idleNews.isPublish}">
		<input type="hidden" class="form-control" name="title" id="delete-title" value="${idleNews.title}">
		<input type="hidden" class="form-control" name="content" id="delete-content" value="${idleNews.content}">
		
	
	<div class="form-group">
	<label><h3>Are You Sure You Want To Delete The Article ${idleNews.title}?</h3></label>
	</div>
	<div class="box-footer pull-right">
		<button type="button" class="btn btn-warning" onclick="deleteArticle()"
			data-dismiss="modal">Delete</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
	</div>
</form>