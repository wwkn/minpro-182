<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="idleNewsPublish" role="form">
	<div class="box-body">
		<input type="hidden" class="form-control" name="id" id="edit-id" value="${idleNews.id}">
		<input type="hidden" class="form-control" name="createdBy" id="edit-createdBy" value="${idleNews.createdBy}">
		<input type="hidden" class="form-control" name="createdOn" id="edit-createdOn" value="${idleNews.createdOn}">
		<input type="hidden" class="form-control" name="modifiedBy" id="edit-modifiedBy" value="${idleNews.modifiedBy}">
		<input type="hidden" class="form-control" name="modifiedOn" id="edit-modifiedOn" value="${idleNews.modifiedOn}">
		<input type="hidden" class="form-control" name="isDelete" id="edit-isDelete" value="${idleNews.isDelete}">
		<input type="hidden" class="form-control" name="categoryId" id="edit-categoryId" value="${idleNews.categoryId}">
		
		<input type="hidden" class="form-control" name="title" id="edit-title" value="${idleNews.title}">
		<input type="hidden" class="form-control" name="content" id="content" value="${idleNews.content}">
		
	
	<div class="form-group">
	<label><h3>Are You Sure You Want To Publish The Article ${idleNews.title}?</h3></label>
	</div>
	<div class="box-footer pull-right">
		<button type="button" class="btn btn-warning" onclick="publishArticle()"
			data-dismiss="modal">Publish</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
	</div>
</form>