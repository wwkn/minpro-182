<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach items="${list}" var="idleNews">
	<tr>
		<td>${idleNews.title}</td>
		<td>${idleNews.category.name}</td>
		<td>
			<div class="btn-group">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				<i class="glyphicon glyphicon-menu-hamburger"></i>
				</button>
				<ul class="dropdown-menu">
				<li><a class="btn btn-edit btn-sm" data="${idleNews.id}" ><i class="fa fa-edit"></i>Edit</a></li>
				<li><a class="btn btn-delete btn-sm" data="${idleNews.id}"><i class="fa fa-exclamation-circle"></i>Delete</a></li>
				<li><a class="btn btn-publish btn-sm" data="${idleNews.id}"><i class="fa fa-paper-plane"></i>Publish</a></li>
				</ul>
			</div>
		</td>
	</tr>
</c:forEach>

<!-- pastikan nama dari field(parameter sudah sama dengan modelnya!! kalau tidak jadi error [jsp] threw exception -->