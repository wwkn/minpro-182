<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="addIdleNews" class="form-horizontal">
	<div class="box-body">
		<div class="form-group">
			<label>Title</label><input class="form-control" name="title"
				id="add-title" required />
		</div>
		<div class="form-group">
			<div>
			<label>Category</label>
				<select class="form-control select2" style="width: 100%"
					name="categoryId" id="add-category" required>
					<option value="" disabled selected>Category</option>
					<c:forEach items="${cList}" var="category">
						<option value="${category.id}">${category.name}</option>
					</c:forEach>
				</select>
			</div>	
		</div>
		<div class="form-group">
			<label>Content</label>
			<textarea class='form-control' name='content' id='add-content'></textarea>
		</div>
		<div class="box-footer pull-right">
			<button type="submit" class="btn btn-warning">Save</button>
			<button type="reset" class="btn btn-warning" data-dismiss="modal">Cancel</button>
		</div>
	</div>
</form>
