<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="idleNewsEdit" role="form">
	<div class="box-body">
		<input type="hidden" class="form-control" name="id" id="edit-id" value="${idleNews.id}">
		<input type="hidden" class="form-control" name="createdBy" id="edit-createdBy" value="${idleNews.createdBy}">
		<input type="hidden" class="form-control" name="createdOn" id="edit-createdOn" value="${idleNews.createdOn}">
		<input type="hidden" class="form-control" name="modifiedBy" id="edit-modifiedBy" value="${idleNews.modifiedBy}">
		<input type="hidden" class="form-control" name="modifiedOn" id="edit-modifiedOn" value="${idleNews.modifiedOn}">
		<input type="hidden" class="form-control" name="isDelete" id="edit-isDelete" value="${idleNews.isDelete}">
		<input type="hidden" class="form-control" name="categoryId" id="edit-categoryId" value="${idleNews.categoryId}">
		<input type="hidden" class="form-control" name="isPublish" id="edit-isPublish" value="${idleNews.isPublish}">
		
		<div class="form-group">
			<div class="form-group">
				<label>Title</label><input class="form-control" name="title" id="edit-title" value="${idleNews.title}" required>
			</div>
		<div class="form-group">
			<div>
			<label>Category</label>
				<select class="form-control select2" style="width: 100%"
					name="categoryId" required>
					<option value="${idleNews.category.id }" disabled selected>${idleNews.category.name }</option>
					<c:forEach items="${cList}" var="category">
						<option value="${category.id}">${category.name}</option>
					</c:forEach>
				</select>
			</div>	
		</div>
			<div class="form-group">
				<label>Content</label><textarea class="form-control" name="content" id="edit-content">${idleNews.content}</textarea>
			</div>
		</div>
		<div class="box-footer pull-right" >
		<button type="submit" class="btn btn-warning" >Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
	</div>
</form>