
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<!-- jstl untuk memasukkan data list menggunakan foreach -->
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">Idle News</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control" id="searchTitle" placeholder="Search by Title" /> <span
						class="input-group-btn">
						<button class="btn btn-warning" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="btn-add" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>Title</th>
					<th>Category</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="list-data">
				<c:forEach items="${list}" var="idleNews">
					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-form">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
					<h4 class="box-title"></h4>
				</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
		loadData();
});

$("#btn-add").click(function(){
	$.ajax({
		url:contextName+'/idle-news/add.html',
		type:'get',
		dataType:"html",
		success: function(d){
			$('#modal-form').modal('show');
			$('#modal-form').find('.box-body').html(d);
			$('#modal-form').find('.box-title').html('Add Idle News');
		}
	});
});

//edit
$('#list-data').on('click','.btn-edit',function(){
	$.ajax({
		url:contextName+'/idle-news/edit.html',
		type:'get',
		dataType:'html',
		data:{'id':$(this).attr('data')},
		success: function(data){
			$('#modal-form').modal('show');
			$('#modal-form').find('.box-body').html(data);
			$('#modal-form').find('.box-title').html('Edit News');
		}
	});
});

//publish
$('#list-data').on('click','.btn-publish',function(){
	$.ajax({
		url:contextName+'/idle-news/publish.html',
		type:'get',
		dataType:'html',
		data:{'id':$(this).attr('data')},
		success: function(d){
			$('#modal-form').modal('show');
			$('#modal-form').find('.box-body').html(d);
			$('#modal-form').find('.box-title').html('Publish News');
		}
	});
});
//get the data of the id taken from the button
$('#list-data').on('click','.btn-delete',function(){
	$.ajax({
		url:contextName+'/idle-news/delete.html',
		type:'get',
		dataType:'html',
		data:{'id':$(this).attr('data')},
		success: function(deletedata){
			$('#modal-form').modal('show');
			$('#modal-form').find('.box-body').html(deletedata);
			$('#modal-form').find('.box-title').html('Delete Idle News');
		}
	});
});
//mengisi table
function loadData() {
	$.ajax({
		type : 'get',
		url : contextName + '/idle-news/list.html',
		success : function(data) {
			$('#list-data').html(data);
		}
	});
}
//submit add data
$("#modal-form").on('submit','#addIdleNews',function() {
			var dataFormatJson=getFormData($('#addIdleNews'))
				$.ajax({
					url: contextName+'/api/idle-news/post/',
					type:'post',
					data: JSON.stringify(dataFormatJson),
					dataType:'json',
					contentType:'application/json',
					success: function(d){
						$('#modal-form').modal('hide');
						alert('Data Succesfully Added!');
						loadData();
					},
					error : function(d){
						alert('Failed')
					}
				});
				return false;
			});
//submit idle news data
$("#modal-form").on('submit','#idleNewsEdit',function() {
	var dataForm=getFormData($('#idleNewsEdit'));
	$.ajax({
		type:'put',
		url:contextName+'/api/idle-news/edit/',
		data:JSON.stringify(dataForm),
		datatype:'json',
		contentType:'application/json',
		success:function(dataEdit){
			alert('Data Succesfully Updated!');
			loadData();
		},
		error:function(dataEdit){
			alert('FAILED')
		}
	});
	return false;
});
//publish
function publishArticle(){
	var dataForm=getFormData($('#idleNewsPublish'));
	$.ajax({
		type:'put',
		url:contextName+'/api/idle-news/publish/',
		data:JSON.stringify(dataForm),
		datatype:'json',
		contentType:'application/json',
		success:function(data){
			alert('Article Has Been Published!');
			loadData();
		},
		error:function(data){
			alert('FAILED')
		}
	});
}

function deleteArticle(){
	var dataForm=getFormData($('#idleNewsDelete'));
	$.ajax({
		type:'put',
		url:contextName+'/api/idle-news/delete/',
		data:JSON.stringify(dataForm),
		datatype:'json',
		contentType:'application/json',
		success:function(data){
			alert('Article Has Been Deleted!');
			loadData();
		},
		error:function(data){
			alert('FAILED')
		}
	});
}
$("#btn-search").click(function(){
	$.ajax({
		url : contextName + '/idle-news/search.html',
		data : {searchWord : $('#searchTitle').val()},
		dataType : 'html',
		type : 'get',
		success : function(data){
			$('#list-data').html(data);
		}
	});
});
//script select untuk modal category
$('.select2').select2()


</script>