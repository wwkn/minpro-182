<form role="form" id="delete-biodata">
	<div class="box-body">
		<div class="form-group">
			<input type="hidden" class="form-control" name="id" id="del-id"
				value="${biodata.id}" /> <input type="hidden" class="form-control"
				name="createdBy" id="del-createdBy" value="${biodata.createdBy}" />
			<input type="hidden" class="form-control" name="createdOn"
				id="del-createdOn" value="${biodata.createdOn}" /> <input
				type="hidden" class="form-control" name="modifiedBy"
				id="del-modifiedBy" value="${biodata.modifiedBy}" /> <input
				type="hidden" class="form-control" name="modifiedOn"
				id="del-modifiedOn" value="${biodata.modifiedOn}" /> <input
				type="hidden" class="form-control" name="isDelete" id="del-isDelete"
				value="${biodata.isDelete}" /> <input type="hidden"
				class="form-control" name="name" id="del-name"
				value="${biodata.name}" /> <input type="hidden"
				class="form-control" name="gender" id="del-gender"
				value="${biodata.gender}" /> <input type="hidden"
				class="form-control" name="lastEducation" id="del-lastEducation"
				value="${biodata.lastEducation}" /> <input type="hidden"
				class="form-control" name="graduationYear" id="del-graduationYear"
				value="${biodata.graduationYear}" /> <input type="hidden"
				class="form-control" name="educationalLevel"
				id="del-educationalLevel" value="${biodata.educationalLevel}" /> <input
				type="hidden" class="form-control" name="majors" id="del-majors"
				value="${biodata.majors}" /> <input type="hidden"
				class="form-control" name="gpa" id="del-gpa" value="${biodata.gpa}" />
			<input type="hidden" class="form-control" name="bootcampTestType"
				id="del-bootcampTestType" value="${biodata.bootcampTestType}" /> <input
				type="hidden" class="form-control" name="iq" id="del-iq"
				value="${biodata.iq}" /> <input type="hidden" class="form-control"
				name="du" id="del-du" value="${biodata.du}" /> <input type="hidden"
				class="form-control" name="arithmetic" id="del-arithmetic"
				value="${biodata.arithmetic}" /> <input type="hidden"
				class="form-control" name="nestedLogic" id="del-nestedLogic"
				value="${biodata.nestedLogic}" /> <input type="hidden"
				class="form-control" name="joinTable" id="del-joinTable"
				value="${biodata.joinTable}" /> <input type="hidden"
				class="form-control" name="tro" id="del-tro" value="${biodata.tro}" />
			<input type="hidden" class="form-control" name="notes" id="del-notes"
				value="${biodata.notes}" /> <input type="hidden"
				class="form-control" name="interviewer" id="del-interviewer"
				value="${biodata.interviewer}" />
		</div>

		<div class="form-group">
			<label><h1>Apakah Anda Yakin Menghapus Data</h1></label>
		</div>
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="button" class="btn btn-warning" onclick="deleteData()"
			data-dismiss="modal">Yes</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
	</div>
</form>