<form id="biodata-add" class="form-horizontal">
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="name" name="name" class="form-control"
				placeholder="Name" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="lastEducation" name="lastEducation"
				class="form-control" placeholder="Last Education" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="educationalLevel" name="educationalLevel"
				class="form-control" placeholder="Educational Level" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="graduationYear" name="graduationYear"
				class="form-control" placeholder="Graduation Year" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="majors" name="majors" class="form-control"
				placeholder="Majors" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="text" id="gpa" name="gpa" class="form-control"
				placeholder="GPA" />
		</div>
	</div>
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="submit">Save</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>