<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="biodata-edit" class="form-horizontal">
	<div class="form-group">
		<div class="col-xs-4">
			<input type="hidden" class="form-control" name="id" id="edit-id"
				value="${biodata.id}"> <input type="hidden"
				class="form-control" name="createdBy" id="edit-createdBy"
				value="${biodata.createdBy}"> <input type="hidden"
				class="form-control" name="createdOn" id="edit-createdOn"
				value="${biodata.createdOn}"> <input type="hidden"
				class="form-control" name="isDelete" id="edit-isDelete"
				value="${biodata.isDelete}">
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
			<div class="form-group">
				<label>Name</label>
				<input type="text" class="form-control" name="name" id="edit-name"
					value="${biodata.name}" />
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label>Last Education</label>
				<input type="text" class="form-control" name="lastEducation"
					id="edit-lastEducation" value="${biodata.lastEducation}" />
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label>Educational Level</label>
				<input type="text" class="form-control" name="educationalLevel"
					id="edit-educationalLevel" value="${biodata.educationalLevel}" />
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label>Graduation Year</label>
				<input type="text" class="form-control" name="graduationYear"
					id="edit-graduationYear" value="${biodata.graduationYear}" />
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label>Majors</label>
				<input type="text" class="form-control" name="majors"
					id="edit-majors" value="${biodata.majors}" />
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label>GPA</label>
				<input type="text" class="form-control" name="gpa" id="edit-gpa"
					value="${biodata.gpa}" />
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="col-md-12">
			<div class="form-group">	
				<div class="radio">
				<label><b>Gender &emsp; &emsp;</b></label>
					<label><input type="radio" id="edit-genderM"
						name="gender" value="M" />M</label> 
					<label><input type="radio" id="edit-genderF"
						name="gender" value="F" />F</label>	
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
					<label>Bootcamp Test Type</label>
					<select class="form-control select2" style="width: 100%;"
						name="bootcampTestType">
						<option value="${biodata.btt.id}" disabled selected>${biodata.btt.name}</option>
						<c:forEach items="${bttList}" var="btt">
							<option value="${btt.id}">${btt.name}</option>
						</c:forEach>
					</select>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-md-3">
					<label>IQ</label>
					<input type="text" class="form-control" id="edit-iq" name="iq"
						value="${biodata.iq}" style="font-size: 11px;"/>
				</div>
				<div class="col-md-3">
					<label>DU</label>
					<input type="text" class="form-control" id="edit-du" name="du"
						value="${biodata.du}" style="font-size: 11px;" />
				</div>
				<div class="col-md-3">
					<label>NL</label>
					<input type="text" class="form-control" id="edit-nestedLogic"
						name="nestedLogic" value="${biodata.nestedLogic}" style="font-size: 11px;" />
				</div>
				<div class="col-md-3">
					<label>JT</label>
					<input type="text" class="form-control" id="edit-joinTable"
						name="joinTable" value="${biodata.joinTable}" style="font-size: 11px;" />
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label>Arithmetic</label>
				<input type="text" class="form-control" id="edit-arithmetic"
					name="arithmetic" value="${biodata.arithmetic}" />
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label>TRO</label>
				<input type="text" class="form-control" id="edit-tro" name="tro"
					value="${biodata.tro}" />
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label>Interviewer</label>
				<input type="text" class="form-control" id="edit-interviewer"
					name="interviewer" value="${biodata.interviewer}" />
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label>Notes</label>
				<textarea class="form-control" id="edit-notes" name="notes">${biodata.notes}</textarea>
			</div>
		</div>
	</div>

	<div class="box-footer pull-right">
		<button type="button" class="btn btn-warning" onclick="updateData()"
			data-dismiss="modal">Save</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>