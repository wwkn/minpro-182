<form id=formDelete>
	<div class="box-body">
		<div class="form-group">
			<input type="hidden" class="form-control" name="id" id="delete-id" value="${testType.id }"> 
			<input type="hidden" class="form-control" name="name" id="delete-name" value="${testType.name }"> 
			<input type="hidden" class="form-control" name="notes" id="delete-notes" value="${testType.note }"> 
			<input type="hidden" class="form-control" name="createdBy" id="delete-createdBy" value="${testType.createdBy }">
			<input type="hidden" class="form-control" name="createdOn" id="delete-createdOn" value="${testType.createdOn }"> 
			<input type="hidden" class="form-control" name="modifiedOn" id="delete-modifiedOn" value="${testType.modifiedOn }"> 
			<input type="hidden" class="form-control" name="modifiedBy" id="delete-modifiedBy"value="${testType.modifiedBy }">
			<input type="hidden" class="form-control" name="typeOfAnswer" id="delete-typeOfAnswer"value="${testType.typeOfAnswer }">
		</div>
		<div class ="form-group">
		<label><h1>Are You Sure You Want To Delete?</h1></label>
		</div>
	</div>
	<!-- /.box-body -->

	<div class="box-footer pull-right">
		<button type="button" class="btn btn-warning" onclick="deleteData()"
			data-dismiss="modal">Delete</button>
		<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>