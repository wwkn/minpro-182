<script>
function searchfunct() {
	// Declare variables
	var input, filter, ul, li, a, i;
	input = document.getElementById("searchBar");
	filter = input.value.toUpperCase();
	ul = document.getElementById("testTypeList");
	li = ul.getElementsByTagName("tr");

	// Loop through all list items, and hide those who don't match the search query
	for (i = 0; i < li.length; i++) {
		a = li[i].getElementsByTagName("p")[0];
		if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
			li[i].style.display = "";
		} else {
			li[i].style.display = "none";
		}
	}
}
</script>