
<form id="formgue">
	<div class="box body">
		<div class="form-group">
			<label>Name</label> <input class="form-control" name="name"
				id="add-name">

		</div>
		<div class="form-group">
			<label>Notes</label>
			<textarea class="form-control" name="note" id="add-note"></textarea>
		</div>
		<div class="box-footer pull-right">
			<button type="submit" class="btn btn-warning btn-submit"
				 >Save</button>
			<button type="reset" class="btn btn-warning " data-dismiss="modal" onclick="loadData()">Cancel</button>
		</div>
	</div>
</form>
