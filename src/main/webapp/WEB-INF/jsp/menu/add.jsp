<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="AddMenu">
	<div class="form-group">
		<input type="text" name="code" id="menu-code" class="form-control" placeholder="Code"/>
	</div>
	<div class="form-group">
		<input type="text" name="title" id="menu-title" class="form-control" placeholder="Title"/>
	</div>
	<div class="form-group">
		<textarea class="form-control" name="description" id="menu-description"
			placeholder="Description"></textarea>
	</div>
	<div class="form-group">
		<input type="text" name="imageUrl" id="menu-imageUrl" class="form-control" placeholder="Image URL"/>
	</div>
	<div class="form-group">
		<input type="text" name="menuOrder" id="menu-menuOrder" class="form-control" value="${men.id}"placeholder="Menu Order"/>
	</div>
	<div class="form-group">
			<select class="form-control" style="width: 100%;" name="menuParent" id="menu-menuParent">
				<option value="" disabled selected>Menu Parent</option>
				<c:forEach items="${mnuList}" var="men">
					<option value="${men.id}">${men.title}</option>				
				</c:forEach>
			</select>
	</div>
	<div class="form-group">
		<input type="text" name="menuUrl" id="menu-menuUrl" class="form-control" placeholder="Menu URL"/>
	</div>
	
	<div class="box-footer pull-right">
		<button class="btn btn-warning" type="button" onClick="saveData()"
			data-dismiss="modal">Save</button>
		<button class="btn btn-warning" data-dismiss="modal">Cancel</button>
	</div>
</form>
