<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-warning box-solid">
	<div class="box-header">
		<h3 class="box-title">MENU</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="text" class="form-control" id="txt-search"
						placeholder="Search by title / majors" /> <span
						class="input-group-btn">
						<button class="btn btn-warning" id="btn-search">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
			</div>
			<div class="col-md-9">
				<button id="btnAdd" class="btn btn-warning btn-sm pull-right">
					<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>CODE</th>
					<th>TITLE</th>
					<th>MENU PARENT</th>
				</tr>
			</thead>
			<tbody id="loadMenu">
				<c:forEach items="${MnuList}" var="mn">
					<tr>
						<td>${mn.code}</td>
						<td>${mn.title}</td>
						<td>Menu ${mn.menuParent}</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<i class="glyphicon glyphicon-menu-hamburger"></i>
								</button>
								<ul class="dropdown-menu">
								<!--assig adalah nama var  -->
									<li><a href="#" onclick="loadDataGet(${mn.id})">Edit</a>
									</li>
									<li><a href="#" onclick="delDataGet(${mn.id})">Delete</a>
									</li>
									
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="modalEr">
	<div class="modal-dialog">
		<div class="box box-warning box-solid">
			<div class="box-header">
				<h4 class="box-title">MENU</h4>
			</div>
			<div class="box-body"></div>
		</div>
	</div>
</div>

<script>
//klik id=>btnAdd dan muncul modal
$("#btnAdd").click(function(){
	$.ajax({
		url : contextName + "/menu/add.html",
		type: "get",
		dataType : "html",
		success : function(data) {
			$("#modalEr").find(".box-body").html(data);
			$("#modalEr").find(".box-title").html('MENU');
			$("#modalEr").modal("show");
		}
	});
});

function saveData() {
	var dJson = getFormData($('#AddMenu'));
	$.ajax({
		url : contextName + '/api/menu/tambah/',
		type : 'post',
		data : JSON.stringify(dJson),
		dataType : 'json',
		contentType : 'application/json',
		success : function(d) {
			$('#modalEr').modal('hide');
			loadData();
			alert('sukses');
		}, error : function(d) {
			alert('sukses');
			loadData();
		}
	});
}

function loadDataGet(id) {
	$.ajax({
		type : 'get',
		url : contextName + '/menu/edit.html',
		data : {"id" : id},
		success : function(data) {
			//mengisi data html
			$('#modalEr').find('.box-body').html(data);
			$("#modalEr").find(".box-title").html('MENU');
			//tampilkan modal
			$("#modalEr").modal("show");
		}
	});
}

function updateData() {
	var dataformatjson = getFormData($('#editMenu'));
	$.ajax({
		type : 'put',
		url : contextName + '/api/menu/edit/',
		data : JSON.stringify(dataformatjson),
		dataType : 'json',
		contentType : 'application/json',
		success: function(d) {
			loadData();
			alert('sukses');
		},
		error: function(d) {
			alert('Failed to update data !');
		}
	});
}

function delDataGet(id) {
	$.ajax({
		type : 'get',
		url : contextName + '/menu/delete.html',
		data : {
			"id" : id
		},
		success : function(data) {
			//mengisi data html
			$('#modalEr').find('.box-body').html(data);
			//tampilkan modal
			$("#modalEr").modal("show");

		}
	});
}

function deleteData() {
	var dataformatjson = getFormData($('#menuDelete'));
	$.ajax({
		type : 'put',
		url : contextName + '/api/menu/d',
		data : JSON.stringify(dataformatjson),
		dataType : 'json',
		contentType : 'application/json',
		success : function(d) {
			alert('terhapus');
			loadData();
		},
		error : function(d) {
			alert('gagal');
			loadData();
		}
	});
}

function loadData() {
	$.ajax({
		url : contextName + '/menu/list.html',
		type : 'get',
		dataType : 'html',
		success : function(d) {
			$('#loadMenu').html(d);
		}
	});
}

$("#btn-search").click(function(){
	$.ajax({
		url : contextName + '/menu/search.html',
		data : {cari : $('#txt-search').val()},
		dataType : 'html',
		type : 'get',
		success : function(data){
			$('#loadMenu').html(data);
		}
	});
});
</script>