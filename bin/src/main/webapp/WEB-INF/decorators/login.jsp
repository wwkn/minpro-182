
<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%@ page isELIgnored="false"%>

<html lang="en">
<head>
<meta charset="ISO-8859-1">
<title>Login - <decorator:title /></title>
<!-- jQuery -->
<script src="${contextName}/assets/vendor/jquery/jquery.min.js"></script>
<script src="${contextName}/assets/js/default.js"></script>

<!-- Bootstrap Core CSS -->
<link
	href="${contextName}/assets/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="${contextName}/assets/vendor/metisMenu/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="${contextName}/assets/dist/css/sb-admin-2.css"
	rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="${contextName}/assets/vendor/morrisjs/morris.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="${contextName}/assets/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<script>
	var contextName = '${contextName}';
</script>
<decorator:head />

</head>

<body>
	<decorator:body />


	<!-- /#wrapper -->


	<!-- Bootstrap Core JavaScript -->
	<script
		src="${contextName}/assets/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="${contextName}/assets/vendor/metisMenu/metisMenu.min.js"></script>

	<!-- Morris Charts JavaScript -->
	<script src="${contextName}/assets/vendor/raphael/raphael.min.js"></script>
	<script src="${contextName}/assets/vendor/morrisjs/morris.min.js"></script>

	<!-- 
    <script src="${contextName}/assets/data/morris-data.js"></script>
    -->

	<!-- Custom Theme JavaScript -->
	<script src="${contextName}/assets/dist/js/sb-admin-2.js"></script>
</body>

</html>