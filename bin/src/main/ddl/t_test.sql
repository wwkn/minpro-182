--------------------------------------------------------
--  File created - Friday-December-28-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table T_TEST
--------------------------------------------------------

  CREATE TABLE "T_TEST" 
   (	"ID" NUMBER(11,0), 
	"NAME" VARCHAR2(255 BYTE), 
	"IS_BOOTCAMP_TEST" NUMBER(1,0), 
	"NOTES" VARCHAR2(255 BYTE), 
	"CREATED_BY" NUMBER(11,0), 
	"CREATED_ON" DATE, 
	"MODIFIED_BY" NUMBER(11,0), 
	"MODIFIED_ON" DATE, 
	"DELETED_BY" NUMBER(11,0), 
	"DELETED_ON" DATE, 
	"IS_DELETE" NUMBER(1,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index T_TEST_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "T_TEST_PK" ON "T_TEST" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table T_TEST
--------------------------------------------------------

  ALTER TABLE "T_TEST" ADD CONSTRAINT "T_TEST_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "T_TEST" MODIFY ("IS_DELETE" NOT NULL ENABLE);
  ALTER TABLE "T_TEST" MODIFY ("CREATED_ON" NOT NULL ENABLE);
  ALTER TABLE "T_TEST" MODIFY ("CREATED_BY" NOT NULL ENABLE);
  ALTER TABLE "T_TEST" MODIFY ("IS_BOOTCAMP_TEST" NOT NULL ENABLE);
  ALTER TABLE "T_TEST" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "T_TEST" MODIFY ("ID" NOT NULL ENABLE);
